// Homunculus Skill Tree Database
//
// Structure of Database:
// Class,SkillID,MaxLv[,JobLevel],Prerequisite SkillID1,Prerequisite SkillLv1,PrereqSkillID2,PrereqSkillLv2,PrereqSkillID3,PrereqSkillLv3,PrereqSkillID4,PrereqSkillLv4,PrereqSkillID5,PrereqSkillLv5,IntimacyLvReq //SKILLNAME#Skill Name#
//
// 01. Class                    Homunculus ID.
// 02. SkillID                  Skill ID of the homunuculus skill.
// 03. MaxLv                    Maximum level of the homunuculus skill.
// 04. JobLevel                 Job level required for the skill to become available (optional, reserved, not used by server).
// 05. Prerequisite SkillID     Homunculus skill required for the skill to become available.
// 06. Prerequisite SkillLv     Level of the required homunculus skill.
// ...
// 15. IntimacyLvReq			Minimum level of intimacy to unlock skill.
//
// NOTE: MAX_PC_SKILL_REQUIRE (typically 5) ID/Lv pairs must be specified.

// 6001 = mage
// 6002 = tanker
// 6003 = agile
// 6004 = strong
// 6005 = lucky
// 6006 = accurate
// 6007 = all rounder
// 6008 = superior

6001,8013,5,0,0,0,0,0,0,0,0,0,0,0 //HVAN_CAPRICE** - Rand-cast bolts
6002,8001,9,0,0,0,0,0,0,0,0,0,0,0 //HLIF_HEAL*- HORRIBLE healing skill.
6003,8002,5,0,0,0,0,0,0,0,0,0,0,0 //HLIF_AVOID***** Raises walk speed 
6004,8008,3,0,0,0,0,0,0,0,0,0,0,0 //HAMI_BLOODLUST** - Raise ATK and leech HP
6005,8009,5,0,0,0,0,0,0,0,0,0,0,0 //HFLI_MOON** - Like Bash, can also multihit
6006,8006,5,0,0,0,0,0,0,0,0,0,0,0 //HAMI_DEFENCE**** - Raises vit for both
6007,8007,5,0,0,0,0,0,0,0,0,0,0,0 //HAMI_SKIN* - Passive, raises HC HP/Regen/DEF
6007,8011,5,0,0,0,0,0,0,0,0,0,0,0 //HFLI_SPEED** - Raise HC Flee/Evasion
6008,8010,5,0,0,0,0,0,0,0,0,0,0,0 //HFLI_FLEET** - Raise HC ATK and ASPD
6008,8015,5,0,0,0,0,0,0,0,0,0,0,0 //HVAN_INSTRUCT* - PASSIVE SMALL HC STR/INT+.

// Second tier skills
6001,8020,2,8013,3,0,0,0,0,0,0,0,0,850 //MH_POISON_MIST** → AOE blind, nature dmg
6005,8020,2,8009,3,0,0,0,0,0,0,0,0,850 //MH_POISON_MIST** → AOE blind, nature dmg
6008,8020,2,8010,4,0,0,0,0,0,0,0,0,950 //MH_POISON_MIST** → AOE blind, nature dmg

6002,8003,5,8001,3,0,0,0,0,0,0,0,0,750 //HLIF_BRAIN*** Raises Max SP and SP Regen
6006,8003,5,8006,3,0,0,0,0,0,0,0,0,750 //HLIF_BRAIN*** Raises Max SP and SP Regen
6007,8003,5,8007,4,0,0,0,0,0,0,0,0,850 //HLIF_BRAIN*** Raises Max SP and SP Regen
6008,8003,5,8015,4,0,0,0,0,0,0,0,0,850 //HLIF_BRAIN*** Raises Max SP and SP Regen

6003,8032,2,8002,3,0,0,0,0,0,0,0,0,850 //MH_GOLDENE_FERSE** → +FLEE, ASPD ~ Holydmg
6004,8032,2,8008,3,0,0,0,0,0,0,0,0,850 //MH_GOLDENE_FERSE** → +FLEE, ASPD ~ Holydmg
6007,8032,2,8011,4,0,0,0,0,0,0,0,0,950 //MH_GOLDENE_FERSE** → +FLEE, ASPD ~ Holydmg

// Special for underpowered Homunculus
6002,8031,1,8001,9,8003,5,0,0,0,0,0,0,900 //MH_STAHL_HORN → EXTREME DAMAGE, STUN AND KNOCKBACK

