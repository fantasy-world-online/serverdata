// TMW-2 Script
// Author:
//  Jesusalva
// Description:
//  Reset LOCATION$ when entering a town

// .@q = LilitQuest_Access
// 0 - Access not granted
// 1 - Access granted
// 2 - Tree Minigame complete.

// This NPC have a 1x1 touch area.
// If you logout, then relog, and try to fool it by moving - you'll be caught
// and slided to the right position.
018-5,97,69,0	script	#LocLilit	NPC_HIDDEN,1,1,{
    end;

OnTouch:
    .@q=getq(LilitQuest_Access);
    if (!.@q)
        goto L_AccessDenied;
    else
        EnterTown("Lilit");
    end;

L_AccessDenied:
    getmapxy(.@m$, .@x, .@y, 0);
    // Cheater detected (either coming to or from Lilit), deploy countermeasures at once
    if (.@x != 97 || .@y != 69) {
        slide 97, 69;
        atcommand("@jailfor 3mn "+strcharinfo(0));
        dispbottom l("Cheater detected! You have a three minutes sentence to fulfill, now.");
        end;
    }
    mesn ("???");
    mesc l("You should not be here...");
    next;
    mesn ("???");
    mesc l("Leave this place now, defiler...");
    next;
    mesc l("How will you respond?!"), 1;
    select
        l("Step forward"),
        l("Step backward");
    mes "";

    // First option is a bad choice, but the only right choice.
    if (@menu == 1) {
        mesn ("???");
        mesc l("DIE, FILTHY @@!",  strtoupper(get_race()));
        movecam rand(-20,20), rand(-20,20);
        sleep2(60);
        movecam rand(-20,20), rand(-20,20);
        sleep2(60);
        movecam rand(-20,20), rand(-20,20);
        sleep2(60);
        restorecam;
        percentheal -30, -30;
        next;
    } else {
        closeclientdialog;
        warp "018-5-1", 77, 53;
        end;
    }

    mesn ("???");
    mesc l("Ho... I see you are a tough one...");
    next;
    mesn ("???");
    mesc l("What brings you here, @@?", get_race());
    next;
    mesc l("How will you respond?!"), 1;
    select
        l("To help fairies"),
        l("To explore these lands"),
        l("To collect snake skin"),
        l("To do quests"),
        l("To aid those in need"),
        l("Because I am awesome!"),
        l("I don't know");
    mes "";
    mesn ("???");
    mesc l("I don't care for your reasons.");
    next;
    mesn l("Yetifly the Mighty");
    mesq l("I am the Yetifly, guardian of butter and fairies.");
    next;
    mesn l("Yetifly the Mighty");
    mesc l("AND YOU ARE NOT WELCOME HERE!!"), 1;
    movecam rand(-20,20), rand(-20,20);
    sleep2(60);
    movecam rand(-20,20), rand(-20,20);
    sleep2(60);
    movecam rand(-20,20), rand(-20,20);
    sleep2(60);
    movecam rand(-20,20), rand(-20,20);
    sleep2(60);
    movecam rand(-20,20), rand(-20,20);
    sleep2(60);
    movecam rand(-20,20), rand(-20,20);
    sleep2(60);
    restorecam;
    next;
    mesn l("Yetifly the Mighty");
    mesq l("Unless, of course, if you can prove your strength, challenging me to a duel.");
    next;
    mesc l("How will you respond?!"), 1;
    select
        l("Bring it on!"),
        l("Nah, I am a chicken.");
    mes "";
    if (@menu == 2) {
        closeclientdialog;
        if (rand(1,4) == 3)
            slide 98, 83;
        else
            slide 61, 147;
        dispbottom l("The Yetifly drops you off the cliff. Good job, noob.");
        percentheal -10, 0;
        end;
    }
    mesn l("Yetifly the Mighty");
    mesq l("That wouldn't be fair to you, though. So, you don't need to defeat me.");
    next;
    mesn l("Yetifly the Mighty");
    mesq l("So, you just need to survive for one minute, and I'll consider you are good enough.");
    mesc l("Word of the wise: You can freely challenge the Yetifly later.", 2);
    if (getskilllv(NV_TRICKDEAD)) mesc l("WARNING: Fake Death skill is disabled on the fight."), 1;
    next;
    mesc l("Click \"Next\" to begin the fight."), 1;
    next;
    closeclientdialog;

    // Create instance.
    .@ID=getcharid(0);
    @MAP_NAME$="lilt@"+str(.@ID); // Max 4 chars for map name
    .@INSTID = instance_create("lilt@a"+(.@ID), getcharid(3), IOT_CHAR);
    .@instanceMapName$ = instance_attachmap("018-5-boss", .@INSTID, 0, @MAP_NAME$);

    // Instance already exists, or something went wrong
    if (.@instanceMapName$ == "") {
        mesn l("Yetifly the Mighty");
        mesc l("*put his glasses on*");
        next;
        mesn l("Yetifly the Mighty");
        mesq l("Whaaaaaat, you are that noob from earlier! Vanish! Be gone! Don't bore me!");
        next;
        warp "018-5-1", 77, 53;
        closeclientdialog;
        close;
    }

    // Everything went right, create the instance, it expires after 2 minutes
    instance_set_timeout(120, 120, .@INSTID);
    instance_init(.@INSTID);

    YETIFLY_INSTANCE=.@INSTID;
    addtimer(1000, "#YetiFlyChallengeCtrl::OnWarn1");
    warp @MAP_NAME$, 31, 41;
    end;
}

// Double check
018-5,99,64,0	script	#LilitAccCtrl	NPC_HIDDEN,1,1,{
OnTouch:
    .@q=getq(LilitQuest_Access);
    if (!.@q) {
        slide 97, 69;
        atcommand("@jailfor 3mn "+strcharinfo(0));
        dispbottom l("Cheater detected! You have a three minutes sentence to fulfill, now.");
    }
    end;
}

