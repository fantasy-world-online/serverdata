// TMW2 scripts.
// Authors:
//    Jesusalva
//    TMW Org.
// Description:
//    Workers which produces pages
//    helperM*

// NivalisQuest_BlueSagePagemaker STRUCTURE
//  FIELD 1:
//      MAIN STATE
//  FIELD 2:
//      BOOK MAKING QUEST
//      1 - Illustrations delivered (BS_PMINK)
//      2 - Bindings delivered (BS_PMBINDING)
//      4 - Pages delivered (BS_PMPAGE)
//      8 - Glue delivered (BS_PMGLUE)
//      =15: All items delivered

020-7-1,36,31,4	script	Santeri	NPC_BLUESAGEWORKER_MA,{
    function askQuestion;
    .@qt=getq2(NivalisQuest_BlueSagePagemaker);
    mesn;
    if (.@qt & BS_PMGLUE)
        mesq l("Thanks to you, our glue supply is replenished and we can repair those books.");
    else
        mesq l("Welcome. Are you an adventurer? I could use some help.");

    // Begin here
    askQuestion();
    .@qt=getq2(NivalisQuest_BlueSagePagemaker);
    mes "";
    mesn;
    mesq l("You see, we're working on repairing all of the damaged books and creating new ones for those that were lost.");
    next;
    mesn;
    mesq l("Therefore we need a lot of glue, but our supplies are nearly used up. I need @@/@@ @@ as ingredient to make new glue.", countitem(WolvernTooth), 3, getitemlink(WolvernTooth));
    next;
    mesn;
    mesq l("Do you have that? I have the other materials but I couldn't get it... @@ are too dangerous, they are level @@ monsters.", getmonsterlink(Wolvern), strmobinfo(3,Wolvern));
    if (countitem(WolvernTooth) < 3)
        close;
    next;
    if (askyesno() == ASK_NO) {
        mes "";
        mesn;
        mesq l("That's a pity.");
        close;
    }
    mes "";

    delitem WolvernTooth,     3;
    setq2 NivalisQuest_BlueSagePagemaker, .@qt|BS_PMGLUE;
    getexp 3535, 215; // 20% from reference levels 36/16. It's part of main story.
    // Exp reward will not change. It applies to Job Exp on same rate.
    mesn;
    mesq l("Great! Thank you!");
    close;

function askQuestion {
    .@qs=BSQuestion(getq(NivalisQuest_BlueSage));
    do {
        .@q=getq(NivalisQuest_BlueSage);
        .@q2=getq2(NivalisQuest_BlueSage);
        .@q3=getq3(NivalisQuest_BlueSage);
        .@qt=getq2(NivalisQuest_BlueSagePagemaker);
        next;
        select
            rif(!(.@qt & BS_PMGLUE), any(l("Can I help you?"), l("I am. What heroic action is needed?"))),
            rif(.@qs & BS_QVISITOR, l("Do you know anything about the strange visitor?")),
            rif(.@qs & BS_QHELPER, l("What's your opinion of Peetu and his work?")),
            any(l("I need to leave."), l("See you."), l("Bye."), l("I'm just a lurker."));
        mes "";
        switch (@menu) {
            case 2:
                mesn;
                mesq l("There was a visitor with a mask? I didn't notice. You see, I spend most of my time in the workshop, and concentrate on my work. There are other helpers who attend to the visitors. And hopefully keep them from disturbing my concentration.");
                break;
            case 3:
                mesn;
                mesq l("Oh, that's an interesting question. I was really surprised when I heard that he was responsible for the failure. I've worked together with him before, and I have to say, it really was a pleasure. He's very accurate and diligent, but also polite and helpful.");
                next;
                mesn;
                mesq l("I really wonder what went wrong. I can't imagine Peetu messing up something so important.");
                if (!(.@q3 & .bsId))
                    setq3 NivalisQuest_BlueSage, .@q3 | .bsId;
                break;
            case 4:
                mesn;
                mesq l("Hm. Then please don't disturb me, I'm trying to concentrate.");
                close;
        }
    } while (@menu != 1);
    return;
}

OnInit:
    .bsId=BS_NPC04;
    .sex=G_MALE;
    .distance=5;
    end;
}


