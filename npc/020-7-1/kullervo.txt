// TMW2 scripts.
// Authors:
//    Jesusalva
//    TMW Org.
// Description:
//    Workers which produces pages
//    helperM*

// NivalisQuest_BlueSagePagemaker STRUCTURE
//  FIELD 1:
//      MAIN STATE
//  FIELD 2:
//      BOOK MAKING QUEST
//      1 - Illustrations delivered (BS_PMINK)
//      2 - Bindings delivered (BS_PMBINDING)
//      4 - Pages delivered (BS_PMPAGE)
//      8 - Glue delivered (BS_PMGLUE)
//      =15: All items delivered

020-7-1,45,31,4	script	Kullervo	NPC_BLUESAGEWORKER_MA,{
    function askQuestion;
    .@qt=getq2(NivalisQuest_BlueSagePagemaker);
    mesn;
    if (.@qt & BS_PMPAGE)
        mesq l("Very well, now I can create more paper.");
    else
        mesq l("Oh no! What should we do now?");

    // Begin here
    askQuestion();
    .@qt=getq2(NivalisQuest_BlueSagePagemaker);
    mes "";
    mesn;
    mesq l("We're nearly out of paper. The new books require so much material... And it's not easy to get Reed Bundles to create new paper.");
    next;
    mesn;
    mesq l("I only need @@/@@ @@ to make enough magic paper for the books... Do you, perchance, have them with you?", countitem(ReedBundle), 15, getitemlink(ReedBundle));
    if (countitem(ReedBundle) < 15)
        close;
    next;
    if (askyesno() == ASK_NO) {
        mes "";
        mesn;
        mesq l("Well, I guess I was thinking too high of a random adventurer like yourself...");
        close;
    }
    mes "";

    delitem ReedBundle,     15;
    setq2 NivalisQuest_BlueSagePagemaker, .@qt|BS_PMPAGE;
    getexp 3535, 215; // 20% from references, rounded up. It's part of main story.
    // is present. REMEMBER THIS IS A LEVEL 36/16 QUEST, REGARDLESS IF EVERYONE DECIDES
    // TO DO IT AT LEVEL 40. Exp reward will not change. It's main story, too.
    // Besides, it gives Job Experience, which is not common.
    mesn;
    mesq l("Thank you! That's very generous of you.");
    close;

function askQuestion {
    .@qs=BSQuestion(getq(NivalisQuest_BlueSage));
    do {
        .@q=getq(NivalisQuest_BlueSage);
        .@q2=getq2(NivalisQuest_BlueSage);
        .@q3=getq3(NivalisQuest_BlueSage);
        .@qt=getq2(NivalisQuest_BlueSagePagemaker);
        next;
        select
            rif(!(.@qt & BS_PMPAGE), l("What's wrong?")),
            rif(.@qs & BS_QVISITOR, l("Do you know anything about the strange visitor?")),
            rif(.@qs & BS_QHELPER, l("What's your opinion of Peetu and his work?")),
            any(l("I need to leave."), l("See you."), l("Bye."), l("Good luck."));
        mes "";
        switch (@menu) {
            case 2:
                mesn;
                mesq l("Oh, yes. There was such a guy, shortly before the accident I think. He sneaked into the workshop several times, sticking his nose into stuff that shouldn't be his business. We had to send him back to the library area at least three times.");
                next;
                mesn;
                mesq l("Maybe he was confused, not sure. He talked very strangely.");
                if (!(.@q2 & .bsId))
                    setq2 NivalisQuest_BlueSage, .@q2 | .bsId;
                break;
            case 3:
                mesn;
                mesq l("Are you asking because people say it's his fault? Listen, I've known him for many years now. We started our service here about the same time. We share our room and are really close friends.");
                next;
                mesn;
                mesq l("And in all these years, I've never seen him messing up anything important. He's a perfectionist. He isn't satisfied with anything less than the best possible result when doing his work.");
                next;
                mesn;
                mesq l("I don't understand what went wrong with that spell, but it just has to have another cause than Peetu. I'm worried about him. He must feel very miserable. But I can't leave my duty here.");
                if (!(.@q3 & .bsId))
                    setq3 NivalisQuest_BlueSage, .@q3 | .bsId;
                break;
            case 4:
                close;
        }
    } while (@menu != 1);
    return;
}

OnInit:
    .bsId=BS_NPC03;
    .sex=G_MALE;
    .distance=5;
    npcsit;
    end;
}

