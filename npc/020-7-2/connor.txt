// TMW2 scripts.
// Authors:
//    Jesusalva
//    TMW LoF (Paxel)
// Description:
//    Makes Raw Coal to fool the Blue Sage

020-7-2,72,23,0	script	Connor	NPC_LOF_FURNACE,{
    .@l=CONNOR_LASTDAY;
    .@t=gettimeparam(GETTIME_DAYOFMONTH);
    mesn;
    mesq l("Meh, the Blue Sage is truly extravagant. We take @@ for the simplest things as keeping the house warm.", getitemlink(Coal));
    next;
    mesn;
    mesq l("But @@ is too valorous, you can only forge with it! So it sells well, about @@ GP.", getitemlink(Coal), getiteminfo(Coal, ITEMINFO_SELLPRICE));
    if (.@t == .@l)
        close;
    next;
    mesn;
    mesq l("Well, I could burn @@ @@ to make Raw Coal and the Blue Sage would be none the wiser.", 15, getitemlink(RawLog));
    next;
    mesn;
    mesq l("I can even share with you @@ @@, whaddaya say?", l("a couple of"), getitemlink(Coal)); // ie. whaddaya say -> what do you say
    if (countitem(RawLog) < 15)
        close;
    if (askyesno() == ASK_YES) {
        inventoryplace Coal, 2;
        delitem RawLog, 15;
        getitem Coal, 2;
        CONNOR_LASTDAY=.@t;
        getexp getiteminfo(Coal, ITEMINFO_SELLPRICE)+BaseLevel+JobLevel, 15;
        mes "";
        mesn;
        mesq l("Thanks, pal. See you tomorrow.");
    }
    close;

OnInit:
    .sex=G_MALE;
    .distance=5;
    end;
}

