// TMW2 Script
// Modified by Jesusalva

// Evol scripts.
// Authors:
//    4144
//    Qwerty Dragon
//    Reid
//    Vasily_Makarov
//    Jesusalva
// Description:
//    Starting script of Evol Online, modified for TMW2.
// Translation:
//      FR Translated

000-0,23,19,0	script	Sailors	NPC_SAILORS,6,6,{

OnTouch:
    .@q=getq(General_Narrator);
    if (.@q)
        end;

    //checkclientversion;
    .@lang = requestlang();
    if (.@lang >= 0 && .@lang <= 9) Lang = .@lang;

    showavatar 3;

    mesn "Narrator";
    mesc l("You open your eyes. The salt water is not helping your vision.");
    mesc l("(Click next button to advance dialogs)"), 3;
    next;
    mesc l("What in the world is happening?! Where in Jesusalva's name are you?!");
    next;
    mesc l("Actually. Who are you again? A headache which doesn't wants to pass strikes you.");
    mesc l("And on the meanwhile, ships sailors are shouting at you.");
    next;

    showavatar NPC_ORC_MAN;
    setcamnpc "Sailors", -64, -32;
    mesn l("Orc Voice");
    mesq lg("Hey kid! Can you hear me?");
    next;

    showavatar NPC_SILVIO;
    setcamnpc "Sailors", 0, -32;
    mesn l("Human Voice");
    mesq lg("Hear you? How do you even know she can understand you?!", "Hear you? How do you even know he can understand you?!");
    next;

    showavatar NPC_TRITAN_MAN_SAILOR;
    setcamnpc "Sailors", 144, -80;
    mesn l("Tritan Voice");
    mesq l("We speak various languages on this world. Let's try gesturing!");
    mesq l("Hey, you on the stranded island! Can you hear us?!");
    next;
    restorecam;

    asklanguage(LANG_ON_SEA);

    showavatar NPC_SAILORS;
    setcamnpc;
    mes "";
    mesn;
    mesq lg("Oh, she's still alive!", "Oh, he's still alive!");
    next;
    showavatar NPC_TRITAN_MAN_SAILOR;
    setcamnpc "Sailors", 144, -80;
    mesq lg("This girl needs help, we need to rescue her!", "This guy needs help, we need to rescue him!");
    next;
    mesn;
    mesc l("This is a menu. You can click on the desired option and press \"Send\", and double-clicking should work, too.");
    mes "";
    select
        l("I don't need a rescue. I'm enjoying myself here."),
        l("Thanks... I guess..."),
        l("(Don't respond)");
    mes "";

    if (@menu == 1) {
        mesn l("Sailor");
        mesq l("What do you mean? Do you at least have a rowboat with you?!");
        next;
        mesn l("Billy Bons");
        mesq l("We can't let you die from hunger! That wouldn't be cool at all! And I'm very cool!");
        next;
    } else if (@menu == 2) {
        mes ""; // You don't need to thank us!
    } else {
        mesn l("Sailor");
        mesq lg("Oh noes! She fainted! Quick, rescue her!!", "Oh noes! He fainted! Quick, rescue him!!");
        next;
    }
    restorecam;

    showavatar;
    mesn "Narrator";
    mesc l("The sailors take you aboard their ship.");
    mesc l("Click on the NPCs (Non-Player Characters) around you to continue the introduction.");
    next;

    addtimer(20000,"Elmo#sailors::OnSlow");
    warp "000-0-0", 30, 36;

    closedialog;
    close;

OnInit:
    .sex = G_MALE;
    end;
}
