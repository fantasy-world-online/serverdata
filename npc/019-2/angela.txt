// TMW2 scripts.
// Authors:
//    The Mana World Team
//    Jesusalva
// Description:
//    Elf, main quest is rescuing Cindy (she's kidnapped by Yetis every once in a while)
//    Once you complete her quest (requires some scheduling which the Yeti King provides),
//    you'll gain some needed Reputation Points to be able to cross Frostia Gates.
//    Note this is counted with other Frostia quests to reduce the toll of entering Frostia
//    (With high Frostia reputations, you get rid of the tax and high honors, but this
//    also raise/blocks Orc Village Entrance. Because if you haven't noticed, there's a
//    delicate equilibrium between Elves and Orcs.)
//
//    Globally Controled Quest. Sometimes, she's here. Other times, this NPC is
//    disabled and the one inside the house is active. That's done with a walk
//    animation.
//
//    Related Quests: Nivalis Well, Celestia Quest, Frostia Quest
//    Main Quest: Cindy Quest
//    Reward: Something awesome, probably.
//    Advised Level: Groups of level 80 players.
//
//    Quest Variable: NivalisQuest_Cindy
//      0: Quest Not Started
//      1: Angela assigned the quest
//      2: Yeti instructions were received
//      3: Yeti quest complete, received instructions
//      4: Checkpoint
//      5: Cindy was rescued!
//
//    Global Variable: $@CINDY_STATE
//      0: Status is clean (she'll be kidnapped next interation)
//      %1: Cindy still wasn't rescued
//      %2: A party is fighting currently (the division value is the difficulty level, and affects minimum level)
//      >1500000000: Cindy is safely at home (until this date +%s)

019-2,75,25,0	script	Angela#Outside	NPC_ELF_F,{
    if ($@CINDY_STATE > gettimetick(2))
        goto L_Safe;
    if (BaseLevel < 60) { hello; end; }
    if ($@CINDY_STATE % 2 == 1) goto L_Worried;
    if (getq(NivalisQuest_Cindy) == 0) goto L_Start;
    mesn;
    mesq l("Please find my lovely daughter!");
    if (getq(NivalisQuest_Cindy) == 1 && getq(NivalisQuest_Well) < 2 && @yetiagro) {
        next;
        select
            l("I will, don't worry."),
            l("She isn't on that cave.");
        mes "";
        if (@menu == 2) {
            mesn;
            mesq l("Do you have mental issues? I mean, of course there is a secret passage there!");
            next;
            mesn;
            mesq l("I think the switches may open it, but I couldn't flip them.");
            next;
            mesn;
            mesq l("I guess you'll need an Yeti for that, but those on the cave are murderous.");
            CINDY_PLAYER_STRUCK=true;
            next;
            mesn;
            mesq l("Besides, I'm not an Yeti expert. I don't understand how these creatures think, nor do I care! I only want Cindy back home safely!!");
        }
    }
    close;

L_Safe:
    mesn;
    mesq l("Ah, what a wonderful day.");
    close;

L_Worried:
    mesn;
    mesq l("The group of warriors which went into the cave to rescue my precious Cindy still haven't returned... I'm worried with them, too...");
    close;

L_Start:
    mesn;
    mesq l("My daughter! My precious Cindy, why it had to be you?!");
    next;
    mesn;
    mesq l("Please, help my precious! Save her! Oh, my precious Cindy...");
    next;
    mesn col(l("Rescuing Cindy Quest"),1);
    mesc l("You are about to accept a high-risk quest. You cannot complete it alone."), 1;
    mesc l("Help the distressed woman to save her daughter? ALL PLAYERS are penalized with failures."), 1;
    select
        l("[Decline Quest]"),
        l("[Accept Quest]");
    mes "";
    if (@menu == 1) {
        mesn strcharinfo(0);
        mesq lg("Sorry 'mam, I forgot courage on my other set of pants. This one doesn't have enough defense for it.");
        close;
    }
    mesn;
    mesq l("Awful Yetis kidnapped my daughter, and brought her to the cave. Please save her!");
    next;
    mesn;
    mesq l("Ah, but don't distress them if you can't save her! My precious daughter life is more important than anything!");
    next;
    mesn;
    mesq l("I'll reward you and your friends in an appropriate way, but if you endanger her... Oh, my poor Cindy...");
    next;
    movecam rand(-20,20), rand(-20,20);
    sleep2(80);
    movecam rand(-20,20), rand(-20,20);
    sleep2(80);
    movecam rand(-20,20), rand(-20,20);
    sleep2(80);
    movecam rand(-20,20), rand(-20,20);
    sleep2(80);
    restorecam;
    mes "";
    mesc l("[Quest Accepted]"), 1;
    mesc l("Rescue Cindy from the Ice Labyrinth and the evil Yetis. This is a global, multiplayer quest."), 1;
    mesc l("Advised party size: From 3 and above");
    channelmes("#world", strcharinfo(0) + " said to be brave enough to help Angela rescuing her daughter!");
    setq NivalisQuest_Cindy, 1;
    close;


OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, KnitHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, TneckSweater);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, CottonSkirt);
    setunitdata(.@npcId, UDT_HAIRSTYLE, any(7,8,9,10,11,12,13,19,20));
    setunitdata(.@npcId, UDT_HAIRCOLOR, rand(0,20));

    .sex = G_FEMALE;
    .distance = 5;
    end;
}



