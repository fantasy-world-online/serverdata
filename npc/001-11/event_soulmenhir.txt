// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Special Soul Menhir which only allows leaving the map.

001-11,37,31,0	script	Soul Stone#001-11	NPC_SOUL_CLEAN,{
    mesn;
    mes l("(A mystical aura surrounds this stone. It probably can return you home. What do you do?)");

    menu
        l("Touch it."), L_Warp,
        l("Leave it alone."), -;
    close;

L_Warp:
    warp "Save", 0, 0;
    close;

OnInit:
    .distance = 5;
    end;
}

