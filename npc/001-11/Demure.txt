// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Valentine Event Master

001-11,40,25,0	script	Demure#Valentine	NPC_FEMALE,{
    goto L_Main;

    // Functions
    function	ScoreValentine {
	    .@nb = query_sql("SELECT c.name, i.value FROM `acc_reg_num_db` AS i, `char` AS c WHERE i.key='#VALENTINE_POINTS' AND i.account_id=c.account_id AND c.char_num='0' ORDER BY i.value DESC LIMIT 10", .@name$, .@value);

        mes "##B"+l("Top 10 - Valentine Day")+"##b";
	    mes("1." +.@name$[0]+" ("+.@value[0]+")");
	    mes("2." +.@name$[1]+" ("+.@value[1]+")");
	    mes("3." +.@name$[2]+" ("+.@value[2]+")");
	    mes("4." +.@name$[3]+" ("+.@value[3]+")");
	    mes("5." +.@name$[4]+" ("+.@value[4]+")");
	    mes("6." +.@name$[5]+" ("+.@value[5]+")");
	    mes("7." +.@name$[6]+" ("+.@value[6]+")");
	    mes("8." +.@name$[7]+" ("+.@value[7]+")");
	    mes("9." +.@name$[8]+" ("+.@value[8]+")");
	    mes("10."+.@name$[9]+" ("+.@value[9]+")");
        next;
    }
    // $@VALENTINE_GIFTSTACKS
    // (account id, gifts to receive)
    // $@VALENTINE_LOVELETTER
    // (account id, list of names)
    function	SendGift {
        mesn;
        mesq l("How many chocolate do you want to send? You can trade: @@", @maximus);
        input @Val_Ammo;
        mes "";

        if (@Val_Ammo > @maximus || @Val_Ammo < 1) {
            mesc l("You don't have that many!");
            return;
        }

        do {
            mesn;
            mesq l("To whom you want to send @@ boxes? Cannot be yourself!", @Val_Ammo);
            mesc l("Leave blank to abort.");
            if (!#VALENTINE_SENT)
                mesc l("(If you don't have anyone special to send these, send to @@. Perhaps they'll calm down with those T.T)", b("Monster King")), 3;
            input .@ref$;
            //debugmes "Sending chocolate to: "+.@ref$;
            mes "";
            if (.@ref$ != "") {
                .@ref=gf_accid(strip(.@ref$));
                if (.@ref > 0) {
                    if (.@ref == getcharid(3)) {
                        mesn;
                        mesq l("I said it cannot be yourself... </3");
                        next;
                    } else {
                        // Now we do the magic, first of, delete the chocolate and the text input
                        delitem BoxOfChocolates, @Val_Ammo;
                        .@ref$="";

                        // Send the Chocolate
                        .@m = htget($@VALENTINE_GIFTSTACKS, str(.@ref));
                        if (!.@m)
                            htput $@VALENTINE_GIFTSTACKS, str(.@ref), @Val_Ammo;
                        else
                            htput $@VALENTINE_GIFTSTACKS, str(.@ref), .@m+@Val_Ammo;

                        // Get 1 Point per sent box
                        #VALENTINE_POINTS+=@Val_Ammo;
                        #VALENTINE_SENT+=@Val_Ammo;

                        // Handle Love Letter
                        if (countitem(LoveLetter)) {
                            mesn;
                            mesq l("Should I attach a love letter?");
                            next;
                            if (askyesno() == ASK_YES) {
                                delitem LoveLetter, 1;

                                // Register your name on love letter
                                .@s$ = htget($@VALENTINE_LOVELETTER, .@ref, "");
                                if (.@s$ == "")
                                    htput $@VALENTINE_LOVELETTER, str(.@ref), strip(strcharinfo(0));
                                else
                                    htput $@VALENTINE_LOVELETTER, str(.@ref), .@s$+", "+strip(strcharinfo(0));

                                // Get the bonus
                                // Old rule: 1 point for 2 boxes, capped at 25 and minimum 1 extra point
                                // Sending over 50 boxes at once won't give bonus, give another Love Letter.
                                //#VALENTINE_POINTS+=min(25, max(1, @Val_Ammo/2));

                                // New rule: Double points up to 10 points
                                #VALENTINE_POINTS+=min(10, @Val_Ammo);

                            } // Love Letter OK
                        mesc l("Ok, done!");
                        } // Not yourself
                    } // Valid Person
                } else {
                    mesn;
                    mesq l("Oops, there is nobody known as @@ on this game.", .@ref$);
                    mesq l("Could you try again? There could be a typo!");
                    next;
                } // Invalid Input
            } // Non void input
        } while (.@ref$ != "");
        return;
    }
    function	GetGift {
        .@m = htget($@VALENTINE_GIFTSTACKS, str(getcharid(3)));
        if (.@m > 0) {
            mesq l("Hey, look, you have @@ boxes to collect!", .@m);
            .@n$ = htget($@VALENTINE_LOVELETTER, str(getcharid(3)), l("Secret Admirer"));
            mesq l("They were given with @@ by @@.", any(l("love"), l("passion"), l("affection")), .@n$);

            inventoryplace BoxOfChocolates, .@m;
            getitem BoxOfChocolates, .@m;
            #VALENTINE_RECEIVED+=.@m;
            htput $@VALENTINE_GIFTSTACKS, str(getcharid(3)), 0;
            htput $@VALENTINE_LOVELETTER, str(getcharid(3)), "";

        } else {
            mesq l("Sorry, you don't have any chocolate to pick up.");
        }

        return;
    }

// Script begin
L_Main:
    // Safety Check
    if ($EVENT$ != "Valentine") {
        warp "Save", 0, 0; end;
    }

    // Demure Main
    mesn;
    mesq l("That annoying guy on the side of the tree is annoying. I want to ban him.");
    next;
    mesn;
    mesq l("Anyway, it is Valentine Day, a good day to trade @@ with your admired one!", getitemlink(BoxOfChocolates));
    next;
    do {
        mesc l("You currently have @@ points, @@ boxes of chocolate and @@ love letters.", #VALENTINE_POINTS, countitem(BoxOfChocolates), countitem(LoveLetter));
        @maximus=countitem(BoxOfChocolates);
        if (#VALENTINE_RECEIVED-#VALENTINE_OPENED > 0) {
            mesc l("@@ boxes of chocolate were given to you as a gift.", #VALENTINE_RECEIVED-#VALENTINE_OPENED), 1;
            // Demure can just send Soren's Chocolate to somebody else.
            // Everyone else cannot send their gift chocolates. Please don't lose the boxes.
            if (!is_gm())
                @maximus-=#VALENTINE_RECEIVED-#VALENTINE_OPENED;
        }
        mes "";
        select
            l("Information"),
            l("Scoreboards"),
            rif(@maximus >= 0, l("Send Chocolate")),
            l("Receive Chocolate"),
            l("Okay, bye");
        mes "";

        switch (@menu) {
            case 1:
            mesn;
            mes lg("Valentine Day event consist in gathering @@ and sending them to your loved one.","Valentine Day event consist in gathering @@ and sending them to your loved one.", getitemlink(BoxOfChocolates));
            next;
            mesn;
            mes l("Don't worry if they don't correspond to you. Each box sent will grant you 1 event point.");
            mes l("The prizes are only for the top 10, but Loratay on Land Of Fire Village can make a dress for you.");
            next;
            mes l("You can optionally send a @@ along the chocolate box, so the person knows it was you who sent the chocolate.", getitemlink(LoveLetter));
            mes l("Sending the letter will give you double event points, capped to 10 extra points.");
            next;
            mes l("When receiving the chocolate, you must eat it before event ends to get a point for that!");
            mes l("These boxes cannot be sold, but they'll be deleted a while after the event ends.");
            mes l("You also cannot send any boxes before eating any you've received, but Demure is an exception for this rule.");
            next;
            mes l("As with all TMW2 Major Events, the top 1 receive a special pet, and the other ranked ones get diverse rewards.");
            mes l("There might be reward for anyone who doesn't gets a rank, but I never count on that. So, let's start?");
            mesc l("Note: You must have a char on the first slot to leaderboard work. Points are shared accross all chars on your account."), 1;
            next;
            break;
            case 2:
            ScoreValentine();
            break;
            case 3:
            SendGift(); next;
            break;
            case 4:
            GetGift(); next;
            break;
        }
    } while (@menu != 5);
    closedialog;
    goodbye;
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, Cap);
    setunitdata(.@npcId, UDT_HEADMIDDLE, RedStockings);
    setunitdata(.@npcId, UDT_HEADBOTTOM, BunnyEars);
    setunitdata(.@npcId, UDT_WEAPON, GMRobe);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 14);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 18);

    .sex = G_FEMALE;
    .distance = 5;
    end;

}

