// FWO scripts.
// Authors:
//    Alige, Reid, Vasily_Makarov (Evol)
//    Jesusalva
// Description:
//    Sleeping and grumbling NPC for stat reset.

002-1,32,38,0	script	Devis	NPC_HAMMOC,{
    asleep();
    mesn;
    mesq l("Excuse me, I'm in the middle of my nap. What do you want?");
    next;
    select
        l("I'll report you to Nard."),
        l("Depend on what you can do for me.");
    mes "";
    switch (@menu) {
        case 1:
            mesn;
            mesc l("*yawn*");
            mesq l("Feel free to.");
            break;
        case 2:
            mesn;
            mesq l("I can reset your status points. If you are interested in that?");
            if (BaseLevel <= 10)
                ConfirmStatusReset(0);
            else
                ConfirmStatusReset();
            break;
    }
    asleep();
    close;

OnInit:
    .sex = G_MALE;
    .distance = 3;
    end;
}
