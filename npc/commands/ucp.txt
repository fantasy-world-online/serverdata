// TMW2 Script
// Author:
//    Jesusalva

function	script	UserCtrlPanel	{
    do
    {
        @unsaved=false;
        clear;
        setnpcdialogtitle l("User Control Panel");
        mes l("This menu gives you some options which affect your account.");
        mes l("In some cases, your pincode will be required.");
        mes "";
        mes l("What do you want to access?");
        next;
        if (@unsaved) {
            mesc l("Careful: You have unsaved changes!"), 1;
            mes "";
        }
        select
            l("Rules"),
            l("Game News"),
            l("Account Information"),
            rif(getcharid(2) > 0, l("Guild Information")),
            l("Change Language"),
            rif(getskilllv(TMW2_CRAFT), l("Change Crafting Options")),
            l("Game Settings"),
            l("Save & Exit");

        switch (@menu)
        {
            case 1: GameRules; break;
            case 2: GameNews; break;
            case 3:
                if (!validatepin())
                    break;
                if (!@lgc) {
                    query_sql("SELECT email,logincount,last_ip FROM `login` WHERE account_id="+getcharid(3)+" LIMIT 2", .@email$, .@lgc, .@ip$);
                    @email$=.@email$;
                    @lgc=.@lgc;
                    @ip$=.@ip$;
                } else {
                    .@email$=@email$;
                    .@lgc=@lgc;
                    .@ip$=@ip$;
                }
                mes l("Char Name: @@", strcharinfo(0));
                mes l("Party Name: @@", strcharinfo(1));
                mes l("Guild Name: @@", strcharinfo(2));
                mes l("Clan Name: @@", strcharinfo(4));
                mes "";
                mes l("Email: @@", .@email$[0]);
                if (Sex)
                    mes l("Male");
                else
                    mes l("Female");
                mes l("Last IP: @@", .@ip$[0]);
                mes l("Total Logins: @@", .@lgc[0]);
                next;
                if (@query)
                    break;
                @query=1;
                query_sql("SELECT name,last_login,last_map,partner_id FROM `char` WHERE account_id="+getcharid(3)+" LIMIT 9", .@name$, .@lastlogin$, .@map$, .@married);
                for (.@i = 1; .@i <= getarraysize(.@name$); .@i++) {
                mesn .@name$[.@i-1];
                mes l("Last Seen: @@", FuzzyTime(.@lastlogin$[.@i-1]));
                mes l("Last map: @@", .@map$[.@i-1]);
                if (.@married[.@i-1])
                    mes l("Married with @@", gf_charname(.@married[.@i-1]));
                mes "";
                }
                next;
                break;
            case 4:
                .@gid=getcharid(2);
                mesc (".:: "+getguildname(.@gid)+" ::."), 1;
                mesc l("Guild Master: @@", getguildmaster(.@gid)), 3;
                if (getguildnxp(.@gid) > 0)
                    mesc l("Guild Lv @@, @@/@@ EXP to level up", getguildlvl(.@gid), format_number(getguildexp(.@gid)), format_number(getguildnxp(.@gid)));
                else
                    mesc l("Guild Lv @@, @@/@@ EXP to level up", format_number(getguildlvl(.@gid)), getguildexp(.@gid), "???");

                mes "";
                mesc l("Average player level: @@", getguildavg(.@gid));
                mesc l("Your position on the guild: @@", getguildrole(.@gid, getcharid(3), true));
                next;
                break;
            case 5: asklanguage(LANG_IN_SHIP); break;
            case 6:
              // Draw the GUI and any info on it
              csysGUI_Report();
              //mesc l("Mobpt: @@", Mobpt);
              do {
                .@opt$="Do nothing";
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_BASE);

                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_ATK);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_DEF);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_ACC);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_EVD);

                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_REGEN);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_SPEED);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_DOUBLE);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_MAXPC);

                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_SCRESIST);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_SCINFLICT);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_MANAUSE);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_BOSSATK);

                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_FINAL);

                select (.@opt$);
                mes "";
                switch (@menu) {
                case 2: csysGUI_ChangeOpt(CRGROUP_BASE); break;

                case 3: csysGUI_ChangeOpt(CRGROUP_ATK); break;
                case 4: csysGUI_ChangeOpt(CRGROUP_DEF); break;
                case 5: csysGUI_ChangeOpt(CRGROUP_ACC); break;
                case 6: csysGUI_ChangeOpt(CRGROUP_EVD); break;

                case 7: csysGUI_ChangeOpt(CRGROUP_REGEN); break;
                case 8: csysGUI_ChangeOpt(CRGROUP_SPEED); break;
                case 9: csysGUI_ChangeOpt(CRGROUP_DOUBLE); break;
                case 10: csysGUI_ChangeOpt(CRGROUP_MAXPC); break;

                case 11: csysGUI_ChangeOpt(CRGROUP_SCRESIST); break;
                case 12: csysGUI_ChangeOpt(CRGROUP_SCINFLICT); break;
                case 13: csysGUI_ChangeOpt(CRGROUP_MANAUSE); break;
                case 14: csysGUI_ChangeOpt(CRGROUP_BOSSATK); break;

                case 15: csysGUI_ChangeOpt(CRGROUP_FINAL); break;
                }
              } while (@menu > 1);
                break;
            case 7:
              do
              {
                mesc ".:: " + l("GAME SETTINGS") + " ::.", 3;

                // GSET_SOULMENHIR_MANUAL
                // Enables/Disable manual position saving on Soul Menhir
                if (GSET_SOULMENHIR_MANUAL)
                    mes l("Soul Menhir automatic saving: ") + col(l("Disabled"), 1);
                else
                    mes l("Soul Menhir automatic saving: ") + col(l("Enabled"), 2);


                // GSET_DAILYREWARD_SILENT
                // Enables/Disable silent dialog for daily rewards
                // (otherwise a image will be shown)
                if (GSET_DAILYREWARD_SILENT)
                    mes l("Display daily reward screen: ") + col(l("Disabled"), 1);
                else
                    mes l("Display daily reward screen: ") + col(l("Enabled"), 2);


                if (strcharinfo(2) == "Monster King") {
                    // GSET_AUTORECEIVE_COINS
                    // Enables/Disable autoreceive strange coins
                    if (!GSET_AUTORECEIVE_COINS)
                        mes l("Autoreceive Strange Coins: ") + col(l("Disabled"), 1);
                    else
                        mes l("Autoreceive Strange Coins: ") + col(l("Enabled"), 2);
                }

                if (@unsaved) {
                    mes "";
                    mesc l("Careful: You have unsaved changes!"), 1;
                }

                mes "";
                select
                    l("Return to User Control Panel"),
                    l("Toggle Soul Menhir automatic saving"),
                    l("Toggle Daily Reward screen"),
                    rif(strcharinfo(2) == "Monster King", ("Toggle Autoreceive Event Coins"));
                mes "";

                switch (@menu) {
                    case 1:
                        // Update savepoint if needed
                        if (@unsaved) {
                            if (!GSET_SOULMENHIR_MANUAL) savepoint "000-1", 22, 22;
                            else doevent "Emergency Exit::OnSetLX";
                        }
                        break;
                    case 2:
                        GSET_SOULMENHIR_MANUAL=!GSET_SOULMENHIR_MANUAL;
                        @unsaved=true;
                        break;
                    case 3:
                        GSET_DAILYREWARD_SILENT=!GSET_DAILYREWARD_SILENT; break;
                    case 4:
                        GSET_AUTORECEIVE_COINS=!GSET_AUTORECEIVE_COINS; break;
                }
                clear;
              } while (@menu != 1);
            break;
            case 8: close; break;
        }
    } while (1);
}



-	script	@ucp	32767,{
    end;

OnCall:

    UserCtrlPanel;
    closedialog;
    end;

OnInit:
    bindatcmd "ucp", "@ucp::OnCall", 0, 99, 0;
}
