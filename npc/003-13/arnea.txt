// TMW2 Scripts
// Author:
//    4144
//    Jesusalva
// Description:
//    Arena for Duels and PVP (temporary map)

003-13,29,29,0	script	Arnea#003-13	NPC_ELF_F,{
    /*
    // FIXME
    warp "003-10", 22, 62;
    close;
    */
    if (is_staff()) {
        mes "npc name: " + .name$;
        mes "npc ext name: " + .extname$;
        mes "npc id: " + .id;
        mes "npc parent id: " + .parent;
        mes "npc src id: " + .srcId;
        mes "char id 3: " + getcharid(3);
        mes "instance id: " + instance_id();
        mes "Map ID: " + instance_mapname("003-13");
    }
    if (instance_id() >= 0) {
        goto L_Manage;
    } else {
        // Non staff and on 003-13? That's a bug!
        if (!is_staff()) {
            atcommand "@request Hey hey hey, player found in 003-13 - regular map! Report this to Jesusalva at once!";
            warp "Save", 0, 0;
            close;
        }
        mes "npc not in instance";
    }

    close;

L_Manage:
    mesn;
    select
        l("warp back"),
        rif(!'UDTf, l("begin UDT Challenge")),
        l("cancel");

    switch (@menu)
    {
        case 1:
            warp "003-10", 22, 62;
            break;
        case 2:
            mesc l("spawn challenge monster! How far can you go?"), 2;
            mesc l("The UDT Challenge will create a Gladiator Monster which should somehow emulate a PvP experience.");
            mesc l("Defeating it will advance the round. How far can you survive?");
            next;
            mesc l("Really begin the UDT Challenge?");
            if (askyesno() == ASK_NO)
                break;

            npctalk l("UDT Challenge, @@ began the fight!", strcharinfo(0));
            // Save data
            'udt_blv=BaseLevel;
            'udt_bhp=MaxHp;
            'udt_Str=readparam(bStr);
            'udt_Agi=readparam(bAgi);
            'udt_Vit=readparam(bVit);
            'udt_Int=readparam(bInt);
            'udt_Dex=readparam(bDex);
            'udt_Luk=readparam(bLuk);

            // Begin the fight
            doevent instance_npcname(.name$)+"::OnGladius";
            closeclientdialog;
            break;
        case 3:
            break;
    }
    close;

OnGladius:
    sleep(800);
    .@mg=monster(instance_mapname("003-13"), 38, 32, "Gladiator", TestingMob, 1, instance_npcname(.name$)+"::OnGladius");
    setunitdata(.@mg, UDT_LEVEL,    'udt_blv+'UDTf);
    setunitdata(.@mg, UDT_MAXHP,    'udt_bhp+('UDTf*45));
    setunitdata(.@mg, UDT_HP,       'udt_bhp+('UDTf*45));

    setunitdata(.@mg, UDT_STR, 'udt_Str+('UDTf*rand(1,3)));
    setunitdata(.@mg, UDT_AGI, 'udt_Agi+('UDTf*rand(1,3)));
    setunitdata(.@mg, UDT_VIT, 'udt_Vit+('UDTf*rand(1,3)));
    setunitdata(.@mg, UDT_INT, 'udt_Int+('UDTf*rand(1,3)));
    setunitdata(.@mg, UDT_DEX, 'udt_Dex+('UDTf*rand(1,3)));
    setunitdata(.@mg, UDT_LUK, 'udt_Luk+('UDTf*rand(1,3)));

    // Now, I don't have this info from player, so...
    // We must generate it
    .@str=getunitdata(.@mg, UDT_STR);
    .@agi=getunitdata(.@mg, UDT_AGI);
    .@vit=getunitdata(.@mg, UDT_VIT);
    .@int=getunitdata(.@mg, UDT_INT);
    .@dex=getunitdata(.@mg, UDT_DEX);
    .@luk=getunitdata(.@mg, UDT_LUK);
    .@blv=getunitdata(.@mg, UDT_LEVEL);

    // And then fill it
    setunitdata(.@mg, UDT_ATKRANGE, any(1,1,1,2));
    setunitdata(.@mg, UDT_ATKMIN,   .@blv+(.@str*2)+('UDTf*4));
    setunitdata(.@mg, UDT_ATKMAX,   .@blv+(.@str*2)+.@luk+('UDTf*6));

    setunitdata(.@mg, UDT_DEF,      .@blv+.@vit+('UDTf*2));
    setunitdata(.@mg, UDT_MDEF,     .@blv+.@int+('UDTf*2));
    setunitdata(.@mg, UDT_HIT,      (.@blv*2)+.@dex+('UDTf*4));
    setunitdata(.@mg, UDT_FLEE,     .@blv+.@agi+('UDTf*2));
    setunitdata(.@mg, UDT_PDODGE,   min(30, .@luk/10+('UDTf/3)));
    setunitdata(.@mg, UDT_CRIT,     (.@blv/2)+.@luk+('UDTf*2));
    'UDTf+=1;
    npctalk ("UDT Challenge, wave " + 'UDTf + "!");
    end;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, Darkelm);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CopperArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, JeansChaps);
    setunitdata(.@npcId, UDT_WEAPON, RockKnife);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 14);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 18);

    .sex = G_FEMALE;
    .distance = 9;
    end;

OnInstanceInit:
    .@npcId = getnpcid(instance_npcname(.name$));
    setunitdata(.@npcId, UDT_HEADTOP, Darkelm);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CopperArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, JeansChaps);
    setunitdata(.@npcId, UDT_WEAPON, RockKnife);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 14);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 18);
    end;

}

