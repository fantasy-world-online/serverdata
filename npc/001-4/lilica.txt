// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Lilica is TMW-BR Scary Scary Easter Bunny and will help your trading stuff.

001-4,139,151,0	script	Lilica#easter	NPC_EASTER,{
    mesn;
    mesq lg("Ah, traveller! I am Lilica the Scary Bunny! I exchange many @@ and @@ for neat rewards!","Ah, traveller! I am Lilica the Scary Bunny! I exchange many @@ and @@ for neat rewards!", getitemlink(SilverEasteregg), getitemlink(GoldenEasteregg));
    next;
    mesn;
    mesq l("Golden Eggs are used for the grand collector prize. Ah, I love Easter! I loooooooove it!");
    mesc l("Note: Golden and Silver Eggs are deleted after the next event end.");

    menu
        l("Scary..........."), -,
        l("Trade Silver Eggs"), L_Silver,
        l("Trade Golden Eggs"), L_Golden,
        rif(EASTER_EVENT != gettime(7),l("I want an Eggshell Hat!")), L_Quest,
        l("View LeaderBoard"), L_Leader;
    close;

L_Silver:
    openshop "#eastershop1";
    closedialog;
    close;

L_Golden:
    openshop "#eastershop2";
    closedialog;
    close;

L_Leader:
    mesc l("Leaderboard is refresh daily at 1 AM!");
	mes("1."+$@easter_name$[0]+" ("+$@easter_value[0]+")");
	mes("2."+$@easter_name$[1]+" ("+$@easter_value[1]+")");
	mes("3."+$@easter_name$[2]+" ("+$@easter_value[2]+")");
	mes("4."+$@easter_name$[3]+" ("+$@easter_value[3]+")");
	mes("5."+$@easter_name$[4]+" ("+$@easter_value[4]+")");
	mes("6."+$@easter_name$[5]+" ("+$@easter_value[5]+")");
	mes("7."+$@easter_name$[6]+" ("+$@easter_value[6]+")");
	mes("8."+$@easter_name$[7]+" ("+$@easter_value[7]+")");
	mes("9."+$@easter_name$[8]+" ("+$@easter_value[8]+")");
	mes("10."+$@easter_name$[9]+" ("+$@easter_value[9]+")");
    close;

L_Quest:
    setarray .@Seasonal, EggshellHat, EggshellHat, GreenEggshellHat, OrangeEggshellHat, BlueEggshellHat, EggshellHat;
    .@Hat=.@Seasonal[gettime(7)%6]; // Magically choose the hat from the array
    mesn;
    mesq l("Good choice! This year we're having a @@!", getitemlink(.@Hat));
    next;
    mesn;
    mesq l("As usual, you can get only one hat yearly, for the symbolic amount of 40 @@ and 10 @@!", getitemlink(GoldenEasteregg), getitemlink(SilverEasteregg));
    next;
    menu
        l("Maybe later."), -,
        rif(countitem(GoldenEasteregg) >= 40 && countitem(SilverEasteregg) >= 10, l("Deal.")), L_QuestDone;
    close;

L_QuestDone:
    inventoryplace .@Hat, 1;
    delitem GoldenEasteregg, 40;
    delitem SilverEasteregg, 10;
    //if (rand(0, 10000) < 1000) goto L_Unlucky;
    EASTER_EVENT=gettime(7);
    // The code below not allow hat to leave user account. A bad idea here so I left commented.
    //getitembound(.@Hat, 1, 1);
    getnameditem(.@Hat, strcharinfo(0));
    mesn;
    mesq l("Here you go! Happy easter! Bhop bhop!");
    close;

L_Unlucky:
    mesn;
    mes l("\"Oh... Sorry, @@.", strcharinfo(0));
    mes l("But in accordance to [@@https://tmw2.org/forums/viewtopic.php?pid=8|Community Decision@@], you were unlucky.");
    mes l("This means ##BAll items were lost##b, and you need to collect EVERYTHING, again, to get the hat.");
    mes l("Better luck next time!\"");
    close;

OnClock0100:
OnClock1300:
    if ($EVENT$ == "Easter")
    	.@nb = query_sql("SELECT c.name, i.amount FROM `inventory` AS i, `char` AS c WHERE i.nameid=834 AND i.char_id=c.char_id ORDER BY i.amount DESC LIMIT 10", $@easter_name$, $@easter_value);
    end;

OnInit:
    .sex = G_OTHER;
    .distance = 5;
    end;
}

// Silver Easter Egg exchange.
001-4,139,151,0	trader	#eastershop1	NPC_HIDDEN,{
    end;

OnInit:
	tradertype(NST_CUSTOM);

	sellitem ChocolateMouboo,100;
	sellitem GoldenEasteregg,25;
	sellitem EasterEgg,5;
    end;

OnCountFunds:
	setcurrency(countitem(SilverEasteregg));
	end;

OnPayFunds:
	if( countitem(SilverEasteregg) < @price )
		end;
	delitem SilverEasteregg,@price;
	purchaseok();
	end;
}
// Golden Easter Egg exchange.
001-4,139,151,0	trader	#eastershop2	NPC_HIDDEN,{
    end;

OnInit:
	tradertype(NST_CUSTOM);
    setarray .@Seasonal, EggshellHat, EggshellHat, GreenEggshellHat, OrangeEggshellHat, BlueEggshellHat, EggshellHat;
    .Hat=.@Seasonal[(gettime(7)+1)%6]; // Magically choose the hat from the array
    .PrevHat=.@Seasonal[(gettime(7)-1)%6]; // Magically choose the hat from the array

    // Seasonal item
	sellitem .Hat,200;
	sellitem .PrevHat,500;

    // Rare and not-so-rare Items
	sellitem MercBoxA,500;
	sellitem Boots,450;
	sellitem BronzeGift,100;
	sellitem BunnyEars,50;
	sellitem SilverEasteregg,1;
    end;

OnCountFunds:
	setcurrency(countitem(GoldenEasteregg));
	end;

OnPayFunds:
	if( countitem(GoldenEasteregg) < @price )
		end;
	delitem GoldenEasteregg,@price;
	purchaseok();
	end;
}

