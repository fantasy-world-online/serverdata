// TMW-2 Script
// Author: Jesusalva
// Desc:   Grand Hunter Quest Utils

// MobID
function	script	GHQ_GetQuestIDByMonsterID	{
    switch (getarg(0)) {
        case Maggot:
            return 1;
            break;
        case Snake:
            return 2;
            break;
        case Scorpion:
            return 3;
            break;
        case ForestMushroom:
            return 4;
            break;
        case Pinkie:
            return 5;
            break;
        case Moggun:
            return 6;
            break;
        case Fluffy:
            return 7;
            break;
        case MountainSnake:
            return 8;
            break;
        default:
            return Exception("GHQ GQID: Invalid ID: "+getarg(0), RB_DEFAULT^RB_PLEASEREPORT|RB_ISFATAL);
            break;
    }

}

// QuestID (basically reverses the previous code)
function	script	GHQ_GetMonsterIDByQuestID	{
    switch (getarg(0)) {
        case 1:
            return Maggot;
            break;
        case 2:
            return Snake;
            break;
        case 3:
            return Scorpion;
            break;
        case 4:
            return ForestMushroom;
            break;
        case 5:
            return Pinkie;
            break;
        case 6:
            return Moggun;
            break;
        case 7:
            return Fluffy;
            break;
        case 8:
            return MountainSnake;
            break;
        default:
            return Exception("GHQ GMID: Invalid ID: "+getarg(0), RB_DEFAULT^RB_PLEASEREPORT|RB_ISFATAL);
            break;
    }

}

/////////////////////////////////////////////////////////////////////////////////

// Handle milestone rewards; it does NOT update stuff
// Syntax: GHQ_GetRewardsOnMilestone ( rewardoverrideid )
function	script	GHQ_GetRewardsOnMilestone	{

    // Check if you can store a Strange Coin (you really should)
    // Another item too, which I'm sure you won't get it anywhere.
    inventoryplace NPCEyes, 1;

    // Global setup
    .@q=getq(General_Hunter);
    .@k=getq2(General_Hunter);
    .@monsterId=GHQ_GetMonsterIDByQuestID(getq(General_Hunter));
    // Setup
    .@old=GHMEMO[getq(General_Hunter)];
    .@new=getq2(General_Hunter);
    .@blv=strmobinfo(3, .@monsterId);
    .@bhp=strmobinfo(4, .@monsterId);
    .@xp=0;
    .@gp=0;
    .@jp=0;
    //if (getq2(General_Hunter) >= 10000) goto L_Finish;

    // 1st step: 1000 kills
    if (is_between(.@old, .@new, 1000)) {
        mesc l("Goal: @@/@@ reached!", .@new, 1000), 2;
        .@xp+=.@blv*10;
        .@gp+=(.@bhp/2);
        .@jp+=10;
    } else if (.@new < 1000) {
        mesc l("@@: @@/@@", l("1st step"), .@new, 1000);
    }

    // 2nd step: 2500 kills
    if (is_between(.@old, .@new, 2500)) {
        mesc l("Goal: @@/@@ reached!", .@new, 2500), 2;
        .@xp+=.@blv*25;
        .@gp+=.@bhp;
        .@jp+=25;
    } else if (.@new < 2500) {
        mesc l("@@: @@/@@", l("2nd step"), .@new, 2500);
    }

    // 3rd step: 5000 kills
    if (is_between(.@old, .@new, 5000)) {
        mesc l("Goal: @@/@@ reached!", .@new, 5000), 2;
        .@xp+=.@blv*50;
        .@gp+=.@bhp;
        .@jp+=50;
    } else if (.@new < 5000) {
        mesc l("@@: @@/@@", l("3rd step"), .@new, 5000);
    }

    // 4th step: 7500 kills
    if (is_between(.@old, .@new, 7500)) {
        mesc l("Goal: @@/@@ reached!", .@new, 7500), 2;
        .@xp+=.@blv*75;
        .@gp+=.@bhp;
        .@jp+=75;
    } else if (.@new < 7500) {
        mesc l("@@: @@/@@", l("4th step"), .@new, 7500);
    }

    // 5th step: 10000 kills
    if (is_between(.@old, .@new, 10000)) {
        mesc l("Goal: @@/@@ reached!", .@new, 10000), 2;

        // Main reward
        Zeny=Zeny+.@bhp*25; // Maggot: 10.000 gp. That's plenty.
        getexp .@bhp*BaseLevel, .@bhp; // The monster hp, once again, drastically affects

        // Grand Prize
        switch (.@monsterId) {
        case Pinkie:
            getitem PinkHelmet, 1;
            mesc l("And here's a rare for you, a @@! Good job!", getitemlink(PinkHelmet));
            break;
        default:
            mesc l("A new pet has been unlocked on the @@!", l("Pet Detective"));
        }

    }

    // Get reward (must have Job Exp)
    if (.@jp) {
        getexp .@xp, .@jp;
        Zeny=Zeny+.@gp;
    }

    // Update Grand Hunter memory
    GHMEMO[.@q]=.@k;

    return;
}

/////////////////////////////////////////////////////////////////////////////////

// MobID, Place, Prize Override
function	script	GHQ_Assign	{
    // Arguments
    .@mobId =getarg(0);
    .@loc$  =getarg(1);
    .@prize$=getarg(2, "");

    // Current Quest Status + vars
    .@id=GHQ_GetQuestIDByMonsterID(.@mobId);
    .@q=getq(General_Hunter);
    .@p=getq2(General_Hunter);
    //next;
    //mesq l("Current Quest Progress: @@/10,000 kills", .@p);

    // Handle Partial Rewards
    if (.@q)
        GHQ_GetRewardsOnMilestone();
    mes "";

    mesn;
    mes l("I represent the @@ Hunters. We hunt @@ (Lv @@).", .@loc$, getmonsterlink(.@mobId), strmobinfo(3, .@mobId));
    if (.@prize$ != "")
        mes l("The great prize is @@.", .@prize$);
    else
        mes l("The great prize is to unlock a pet!");
    if (.@q == .@id) {
        .@m=GHQ_GetMonsterIDByQuestID(.@q);
        mes l("You are currently hunting @@/10000 @@.", .@p, getmonsterlink(.@m));
        next;
        closedialog;
        goodbye;
    }
    if (.@q) {
        .@m=GHQ_GetMonsterIDByQuestID(.@q);
        mes l("You are currently hunting @@/10000 @@. Do you want to switch?", .@p, getmonsterlink(.@m));
    }
    select
        l("I'm not interested."),
        rif(GHMEMO[.@id] < 10000,l("I'll hunt them for you."));

    switch (@menu) {
    case 2:
        GHMEMO[getq(General_Hunter)]=getq2(General_Hunter);
        setq(General_Hunter, .@id, GHMEMO[.@id]);
        mesn;
        mesq l("Good luck! Don't come back until you reach 10000 kills!");
        break;
    }
    closedialog;
    goodbye;

}


function	script	mobhunter	{
    if (getq(General_Hunter) == 0)
        return;

    .@ghd=getq(General_Hunter);
    if (killedrid == GHQ_GetMonsterIDByQuestID(.@ghd)) {
        setq2 General_Hunter, getq2(General_Hunter)+1;
        .@ghd=getq2(General_Hunter);
        if (! (.@ghd % 1000))
            dispbottom l("Grand Hunter Quest: @@/10,000", format_number(.@ghd));
    }
    return;

}

