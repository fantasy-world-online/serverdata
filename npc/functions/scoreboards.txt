// TMW-2 Script
// Author:
//    Jesusalva
// Description:
//    Leaderboards + GM Command Log

-	script	scoreboardsGen	NPC_HIDDEN,{
    end;
OnHour00:
OnHour01:
OnHour02:
OnHour03:
OnHour04:
OnHour05:
OnHour06:
OnHour07:
OnHour08:
OnHour09:
OnHour10:
OnHour11:
OnHour12:
OnHour13:
OnHour14:
OnHour15:
OnHour16:
OnHour17:
OnHour18:
OnHour19:
OnHour20:
OnHour21:
OnHour22:
OnHour23:
OnInit:
    debugmes "Reloading scoreboards...";
	.@nb = query_sql("select name, zeny from `char` WHERE `guild_id`!=1 ORDER BY zeny DESC LIMIT 15", $@hofortune_name$, $@hofortune_value);
	.@nb = query_sql("select name, base_level from `char` WHERE `guild_id`!=1 ORDER BY base_level DESC LIMIT 15", $@hoblvl_name$, $@hoblvl_value);
	.@nb = query_sql("select name, job_level from `char` WHERE `guild_id`!=1 ORDER BY job_level DESC LIMIT 15", $@hojlvl_name$, $@hojlvl_value);
	.@nb = query_sql("select char_name, command from `atcommandlog` ORDER BY atcommand_id DESC LIMIT 15", $@hogm_name$, $@hogm_value$);
	.@nb = query_sql("select name, guild_lv from `guild` WHERE `guild_id`!=1 ORDER BY guild_lv DESC LIMIT 5", $@hoguild_name$, $@hoguild_value);
	.@nb = query_sql("SELECT c.name, i.value FROM `char_reg_num_db` AS i, `char` AS c WHERE i.key='CRAZYPOINTS' AND i.char_id=c.char_id ORDER BY i.value DESC LIMIT 10", $@cfefe_name$, $@cfefe_value);
    debugmes "Scoreboards reloaded";
    end;
}

function	script	HallOfGuild	{
    mes "";
	mes l("##BHall Of Guild Level: TOP5##b");
	mes("1."+$@hoguild_name$[0]+" ("+$@hoguild_value[0]+")");
	mes("2."+$@hoguild_name$[1]+" ("+$@hoguild_value[1]+")");
	mes("3."+$@hoguild_name$[2]+" ("+$@hoguild_value[2]+")");
	mes("4."+$@hoguild_name$[3]+" ("+$@hoguild_value[3]+")");
	mes("5."+$@hoguild_name$[4]+" ("+$@hoguild_value[4]+")");
    return;
}

function	script	HallOfFame	{
    // Moved to event statues
    return;
    mes "";
    /*
	.@nb = query_sql("select name, karma from `char` WHERE `guild_id`!=1 ORDER BY karma DESC LIMIT 10", .@name$, .@value);
	mes l("##BHall Of Fame: TOP10##b");
	mes("1."+.@name$[0]+" ("+.@value[0]+")");
	mes("2."+.@name$[1]+" ("+.@value[1]+")");
	mes("3."+.@name$[2]+" ("+.@value[2]+")");
	mes("4."+.@name$[3]+" ("+.@value[3]+")");
	mes("5."+.@name$[4]+" ("+.@value[4]+")");
	mes("6."+.@name$[5]+" ("+.@value[5]+")");
	mes("7."+.@name$[6]+" ("+.@value[6]+")");
	mes("8."+.@name$[7]+" ("+.@value[7]+")");
	mes("9."+.@name$[8]+" ("+.@value[8]+")");
	mes("10."+.@name$[9]+" ("+.@value[9]+")");
    */
    return;
}

function	script	HallOfFortune	{
    mes "";
	mes l("##BHall Of Fortune: TOP15##b");
	mes("1."+$@hofortune_name$[0]+" ("+format_number($@hofortune_value[0])+" GP)");
	mes("2."+$@hofortune_name$[1]+" ("+format_number($@hofortune_value[1])+" GP)");
	mes("3."+$@hofortune_name$[2]+" ("+format_number($@hofortune_value[2])+" GP)");
	mes("4."+$@hofortune_name$[3]+" ("+format_number($@hofortune_value[3])+" GP)");
	mes("5."+$@hofortune_name$[4]+" ("+format_number($@hofortune_value[4])+" GP)");
	mes("6."+$@hofortune_name$[5]+" ("+format_number($@hofortune_value[5])+" GP)");
	mes("7."+$@hofortune_name$[6]+" ("+format_number($@hofortune_value[6])+" GP)");
	mes("8."+$@hofortune_name$[7]+" ("+format_number($@hofortune_value[7])+" GP)");
	mes("9."+$@hofortune_name$[8]+" ("+format_number($@hofortune_value[8])+" GP)");
	mes("10."+$@hofortune_name$[9]+" ("+format_number($@hofortune_value[9])+" GP)");
	mes("11."+$@hofortune_name$[10]+" ("+format_number($@hofortune_value[10])+" GP)");
	mes("12."+$@hofortune_name$[11]+" ("+format_number($@hofortune_value[11])+" GP)");
	mes("13."+$@hofortune_name$[12]+" ("+format_number($@hofortune_value[12])+" GP)");
	mes("14."+$@hofortune_name$[13]+" ("+format_number($@hofortune_value[13])+" GP)");
	mes("15."+$@hofortune_name$[14]+" ("+format_number($@hofortune_value[14])+" GP)");
    return;
}

function	script	HallOfLevel	{
    mes "";
	mes l("##BHall Of Level: TOP15##b");
	mes("1."+$@hoblvl_name$[0]+" ("+$@hoblvl_value[0]+")");
	mes("2."+$@hoblvl_name$[1]+" ("+$@hoblvl_value[1]+")");
	mes("3."+$@hoblvl_name$[2]+" ("+$@hoblvl_value[2]+")");
	mes("4."+$@hoblvl_name$[3]+" ("+$@hoblvl_value[3]+")");
	mes("5."+$@hoblvl_name$[4]+" ("+$@hoblvl_value[4]+")");
	mes("6."+$@hoblvl_name$[5]+" ("+$@hoblvl_value[5]+")");
	mes("7."+$@hoblvl_name$[6]+" ("+$@hoblvl_value[6]+")");
	mes("8."+$@hoblvl_name$[7]+" ("+$@hoblvl_value[7]+")");
	mes("9."+$@hoblvl_name$[8]+" ("+$@hoblvl_value[8]+")");
	mes("10."+$@hoblvl_name$[9]+" ("+$@hoblvl_value[9]+")");
	mes("11."+$@hoblvl_name$[10]+" ("+$@hoblvl_value[10]+")");
	mes("12."+$@hoblvl_name$[11]+" ("+$@hoblvl_value[11]+")");
	mes("13."+$@hoblvl_name$[12]+" ("+$@hoblvl_value[12]+")");
	mes("14."+$@hoblvl_name$[13]+" ("+$@hoblvl_value[13]+")");
	mes("15."+$@hoblvl_name$[14]+" ("+$@hoblvl_value[14]+")");
    return;
}

function	script	HallOfJob	{
    mes "";
	mes l("##BHall Of Job Level: TOP15##b");
	mes("1."+$@hojlvl_name$[0]+" ("+$@hojlvl_value[0]+")");
	mes("2."+$@hojlvl_name$[1]+" ("+$@hojlvl_value[1]+")");
	mes("3."+$@hojlvl_name$[2]+" ("+$@hojlvl_value[2]+")");
	mes("4."+$@hojlvl_name$[3]+" ("+$@hojlvl_value[3]+")");
	mes("5."+$@hojlvl_name$[4]+" ("+$@hojlvl_value[4]+")");
	mes("6."+$@hojlvl_name$[5]+" ("+$@hojlvl_value[5]+")");
	mes("7."+$@hojlvl_name$[6]+" ("+$@hojlvl_value[6]+")");
	mes("8."+$@hojlvl_name$[7]+" ("+$@hojlvl_value[7]+")");
	mes("9."+$@hojlvl_name$[8]+" ("+$@hojlvl_value[8]+")");
	mes("10."+$@hojlvl_name$[9]+" ("+$@hojlvl_value[9]+")");
	mes("11."+$@hojlvl_name$[10]+" ("+$@hojlvl_value[10]+")");
	mes("12."+$@hojlvl_name$[11]+" ("+$@hojlvl_value[11]+")");
	mes("13."+$@hojlvl_name$[12]+" ("+$@hojlvl_value[12]+")");
	mes("14."+$@hojlvl_name$[13]+" ("+$@hojlvl_value[13]+")");
	mes("15."+$@hojlvl_name$[14]+" ("+$@hojlvl_value[14]+")");
    return;
}

function	script	HallOfGMLog	{
    mes "";
	mes l("##BLatest GM Commands##b");
	mes("1."+$@hogm_name$[0]+" ("+$@hogm_value$[0]+")");
	mes("2."+$@hogm_name$[1]+" ("+$@hogm_value$[1]+")");
	mes("3."+$@hogm_name$[2]+" ("+$@hogm_value$[2]+")");
	mes("4."+$@hogm_name$[3]+" ("+$@hogm_value$[3]+")");
	mes("5."+$@hogm_name$[4]+" ("+$@hogm_value$[4]+")");
	mes("6."+$@hogm_name$[5]+" ("+$@hogm_value$[5]+")");
	mes("7."+$@hogm_name$[6]+" ("+$@hogm_value$[6]+")");
	mes("8."+$@hogm_name$[7]+" ("+$@hogm_value$[7]+")");
	mes("9."+$@hogm_name$[8]+" ("+$@hogm_value$[8]+")");
	mes("10."+$@hogm_name$[9]+" ("+$@hogm_value$[9]+")");
	mes("11."+$@hogm_name$[10]+" ("+$@hogm_value$[10]+")");
	mes("12."+$@hogm_name$[11]+" ("+$@hogm_value$[11]+")");
	mes("13."+$@hogm_name$[12]+" ("+$@hogm_value$[12]+")");
	mes("14."+$@hogm_name$[13]+" ("+$@hogm_value$[13]+")");
	mes("15."+$@hogm_name$[14]+" ("+$@hogm_value$[14]+")");
    return;
}

function	script	HallOfCandor	{
    mes "";
	mes l("##BHall Of Crazyfefe Fight: TOP 10##b");
	mes("1."+$@cfefe_name$[0]+" ("+$@cfefe_value[0]+")");
	mes("2."+$@cfefe_name$[1]+" ("+$@cfefe_value[1]+")");
	mes("3."+$@cfefe_name$[2]+" ("+$@cfefe_value[2]+")");
	mes("4."+$@cfefe_name$[3]+" ("+$@cfefe_value[3]+")");
	mes("5."+$@cfefe_name$[4]+" ("+$@cfefe_value[4]+")");
	mes("6."+$@cfefe_name$[5]+" ("+$@cfefe_value[5]+")");
	mes("7."+$@cfefe_name$[6]+" ("+$@cfefe_value[6]+")");
	mes("8."+$@cfefe_name$[7]+" ("+$@cfefe_value[7]+")");
	mes("9."+$@cfefe_name$[8]+" ("+$@cfefe_value[8]+")");
	mes("10."+$@cfefe_name$[9]+" ("+$@cfefe_value[9]+")");
    return;
}





function	script	HallOfReferral	{
    mes "";
    .@nb = query_sql("SELECT l.userid, COUNT(a.value) FROM `mapreg` AS a, `login` AS l WHERE a.varname='$REFERRAL_IDS' AND l.account_id=a.index ORDER BY COUNT(a.value) DESC LIMIT 20", .@name$, .@value);
	mes "Referral Program Report - pg. 1";
	mes("1."+.@name$[0]+" ("+.@value[0]+")");
	mes("2."+.@name$[1]+" ("+.@value[1]+")");
	mes("3."+.@name$[2]+" ("+.@value[2]+")");
	mes("4."+.@name$[3]+" ("+.@value[3]+")");
	mes("5."+.@name$[4]+" ("+.@value[4]+")");
	mes("6."+.@name$[5]+" ("+.@value[5]+")");
	mes("7."+.@name$[6]+" ("+.@value[6]+")");
	mes("8."+.@name$[7]+" ("+.@value[7]+")");
	mes("9."+.@name$[8]+" ("+.@value[8]+")");
	mes("10."+.@name$[9]+" ("+.@value[9]+")");
    next;
	mes "Referral Program Report - pg. 2";
	mes("11."+.@name$[10]+" ("+.@value[10]+")");
	mes("12."+.@name$[11]+" ("+.@value[11]+")");
	mes("13."+.@name$[12]+" ("+.@value[12]+")");
	mes("14."+.@name$[13]+" ("+.@value[13]+")");
	mes("15."+.@name$[14]+" ("+.@value[14]+")");
	mes("16."+.@name$[15]+" ("+.@value[15]+")");
	mes("17."+.@name$[16]+" ("+.@value[16]+")");
	mes("18."+.@name$[17]+" ("+.@value[17]+")");
	mes("19."+.@name$[18]+" ("+.@value[18]+")");
	mes("20."+.@name$[19]+" ("+.@value[19]+")");
    next;
    return;
}



function	script	HallOfSponsor	{
    mes l("This is in honor of all the [@@help://about-server|Contributors@@] who helped rebuilding this world, after the Monster War outbreak.");
    mes l("And also in notable mention of those who [@@https://www.patreon.com/TMW2|sponsor@@] the Alliance and its administrative structure.");
    mes l("");
    mes l("GonzoDark, Saulc.");
    return;
}

function	script	HallOf2018	{
    mes "";
    if ($YETIKING_WINNER$ != "") {
        mes l(".:: FIRST PLAYER TO COMPLETE YETI KING QUEST ::.");
        mes $YETIKING_WINNER$;
        mes "";
    }
    mes l(".:: NOTABLE NAMES ON HURNSCALD LIBERATION DAY ::.");
    mes l("DragonStar, Aisen");
    mes "";
    mes l(".:: NOTABLE NAMES ON NIVALIS LIBERATION DAY ::.");
    mes l("Jesusalva");
    mes "";
    mes l(".:: FIND-THE-NPC 2018 MINI-EVENT WINNER ::.");
    mes "shab";
    next;
    mes l(".:: Easter 2018 ::.");
    mes l("In honor of DragonStar, winner of Easter 2018.");
    mesc l("Unfortunately, other victor's names weren't logged.");
    mes "";
    mes l(".:: Worker Day 2018 ::.");
    mes l("No victor appliable.");
    mes "";
    mes l(".:: Purple Day 2018 ::.");
    mes l("No victor appliable.");
    next;
    mes l(".:: Ched's Summer 2018 ::.");
    mes ("1. WarBlade - 5325");
    mes ("2. Aisen - 2000");
    mes ("3. msawis - 1000");
    mes ("4. vilbou - 400");
    mes ("5. Woody - 353");
    next;
    mes l(".:: Hasan Scorpion Killing Challenge 2018 ::.");
    mes ("1. Krists - 1070");
    mes ("2. Aisen - 598");
    mes ("3. AndulkaT - 212");
    mes ("4. monking - 86");
    mes ("5. Carbon14 - 78");
    next;
    mes l(".:: Christmas 2018 ::.");
    mes ("1. WarBlade - 324");
    mes ("2. Xtreem - 190");
    mes ("3. msawis - 110");
    mes ("4. Krists - 75");
    mes ("5. Mrhedx - 38");
    mes "";
    mes l(".:: Christmas 2018 ::.");
    mes ("1. WarBlade - 100,000 GP");
    mes ("2. msawis - 7,500 GP");
    mes ("3. LawnCable - 4,450 GP");
    mes ("4. Krists - 1,000 GP");
    mes ("5. demure GM - 1,000 GP");
    return;
}

function	script	HallOf2019	{
    mesc l("This schedule is subject to change without prior notice."), 1;
    mesc l("Changes include but are not limited to festive events and dates."), 1;
    next;
    if ($HEROESHOLD_WINNER$ != "") {
        mes l(".:: FIRST PLAYER TO COMPLETE HEROES HOLD MASTER DUNGEON ::.");
        mes $HEROESHOLD_WINNER$;
        mes "";
    }
    mes l(".:: TMW-2 Anniversary ::.");
    //mesc l("Scheduled: January 13rd");
    mes l("No victor appliable.");
    mesc l("Anniversary marks the project birthdate. Do not mistake with TMW2 Day.");
    mes "";
    mes l(".:: Valentine Day ::.");
    //mesc l("Scheduled: February 12th - 15th");
    mes ("1. DragonStar - 300");
    mes ("2. Jesusalva - 121");
    mes ("3. Xanthem - 35");
    mes ("4. Xtreem - 17");
    mes ("5. Yuppi - 10");
    next;
    mes l(".:: TMW2 Day ::.");
    //mesc l("Scheduled: March 2nd");
    mes l("No victor appliable.");
    mesc l("TMW2 Day marks the server birthdate. Do not mistake with TMW2 Anniversary.");
    mes "";
    mes l(".:: Easter 2019 ::.");
    //mesc l("Scheduled: April 17th - 24th");
    mes l("In honor of Woody, winner of Easter 2019.");
    mesc l("Unfortunately, other victor's names weren't logged.");
    mes "";
    mes l(".:: Worker Day ::.");
    mesc l("Scheduled: April 27th - May 3rd");
    next;
    mes l(".:: Ched's Summer 2019 ::.");
    mesc l("June 21st - September 21st");
    mes "";
    mes l(".:: Chocolate Day ::.");
    mesc l("Scheduled: July 7th");
    mes "";
    mes l(".:: Free Software Day ::.");
    mesc l("Scheduled: September 9th");
    mes "";
    mes l(".:: Hasan Scorpion Killing Challenge 2019 ::.");
    mesc l("September 22nd - December 20th");
    mes "";
    mes l(".:: Christmas 2019 ::.");
    mesc l("Scheduled: December 19th - January 2nd");
    return;
}

