// TMW2 Script.
// Authors:
//    Vasily_Makarov (original from Evol)
//    Jesusalva
// Description:
//    Status Reset NPC utils

// Return wasSP on success, 0 on failure
// ConfirmReset( {price} )
function	script	ConfirmStatusReset	{
    if (BaseLevel >= 10)
        .@plush_count=(BaseLevel*210-(10*210))/(BaseLevel/10);
    else
        .@plush_count=1;

    if (getarg(0,-1) >= 0)
        .@plush_count=getarg(0,-1);

    switch (select(lg("Yes, I am sure."),
                   lg("I need to think about it..."),
                   lg("I won't need it, thank you.")))
    {
        case 1:
            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                l("Let me just have a quick look at you. Hm... I will need @@ GP to reset your stats.", .@plush_count);

            select
                rif(Zeny >= .@plush_count, l("Here, take as much as you need, I have plenty!")),
                rif(Zeny > 0 && Zeny < .@plush_count, l("I don't have enough money...")),
                rif(Zeny == 0, l("Oh no, I don't have any money on me right now.")),
                l("I have to go, sorry.");

            if (@menu > 1) {
                return 0;
            }

            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                l("Thank you."),
                l("Now stand still... It should not take much time...");

            // Delete the GP and THEN reset the status
            Zeny-=.@plush_count;
            .@wasSP = StatusPoint;
            resetstatus();
            if (StatusPoint == .@wasSP) {
                speech S_LAST_NEXT,
                    l("It seems that you have no status points to reset!"),
                    l("But the money you brought was really awesome you know."),
                    l("Come back when you will really need me.");
            } else {
                speech S_LAST_NEXT,
                    l("Let's see... @@ of your status points have just been reset!", StatusPoint - .@wasSP),
                    l("Spend it wisely this time."),
                    l("But you are welcome to reset your stats again! I need the money.");
            }
            return .@wasSP;

        case 2:
            return 0;
        case 3:
            return 0;
    }
    Exception("Unknown Error: ConfirmStatusReset() failed");
    return 0;

}

