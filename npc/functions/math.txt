// Evol functions.
// Authors:
//    4144
//    Reid
// Description:
//    Math functions


// abs(<int>)
//    returns the absolute value of the passed integer

function	script	abs	{
    .@n = getarg(0);
    return .@n >= 0 ? .@n : -.@n;
}



// lognbaselvl({<multiplicator>{, <min value>}})
//     returns BaseLevel * logn (BaseLevel * alpha).

function	script	lognbaselvl	{
    .@alpha = getarg(0, 1);
    .@min = getarg(1, 1);
    .@ret = 0;
    .@pc_level = BaseLevel * .@alpha;

    while (.@pc_level >>= 1)
    {
        ++.@ret;
    }
    .@ret *= BaseLevel;

    if (.@ret <= .@min)
    {
        .@ret = .@min;
    }

    return .@ret;
}

// log2(<int>)
//    returns the log base 2 of the passed integer, up to 20 (2**20=1.048.576) (round down always)

function	script	log2	{
    .@v=abs(getarg(0));
    .@ok=0;
    .@i=0;
    if (.@v < 1)
        return -1;

    freeloop(true);
    while (!.@ok) {
        // exact match
        if (2**.@i == .@v) {
            .@ok=1;
        // inexact match, or limit exceeded
        } else if (2**.@i >= .@v || .@i > 20) {
            .@ok=1;
            .@i-=1; // round down
        // not yet
        } else {
            .@i+=1;
        }
    }
    freeloop(false);

    return .@i;
}


// result is: lower < target <= higher
// is_between ( lower, higher, target)
function	script	is_between	{
    .@val=getarg(2);
    return (getarg(0) < .@val && getarg(1) >= .@val);
}


// forces the equation: lower <= target <= higher.
// Note it still works if higher and target values are swapped.
// limit ( lower, target, higher)
function	script	limit	{
    return max(getarg(0), min(getarg(1), getarg(2)));
}


// result is the ponderate average.
// ponderate_avg ( arg1, sub1, arg2, sub2)
function	script	ponderate_avg	{
    .@a1=getarg(0);
    .@s1=getarg(1);
    .@a2=getarg(2);
    .@s2=getarg(3);

    .@h1=.@a1*.@s1;
    .@h2=.@a2*.@s2;
    .@dd=.@s1+.@s2;

    return (.@h1+.@h2)/.@dd;
}

