// TMW2 Script.
// Authors:
//    Jesusalva
// Description:
//    Season functions

// Function authored by Reid and edited by Jesusalva
// season({day, month})
// SQuest_Summer
//    returns the current season (approximation)
//      WINTER:      Winter, 21/12
//      SPRING:      Spring, 20/03
//      SUMMER:      Summer, 21/06
//      AUTUMN:      Autumn, 22/09

function	script	season	{
    .@current_month = getarg(0, gettime(GETTIME_MONTH));

    if (.@current_month % 3 == 0) {
        .@current_day = getarg(1, gettime(GETTIME_DAYOFMONTH));

        switch (.@current_month) {
            case MARCH:     .@season_day = 20; break;
            case JUNE:      .@season_day = 21; break;
            case SEPTEMBER: .@season_day = 22; break;
            case DECEMBER:  .@season_day = 21; break;
            default: break;
        }

        .@is_after_season_day = .@current_day >= .@season_day ? 0 : -1;
    }

    return (.@current_month / 3 + .@is_after_season_day) % 4;
}


// Event seasons
// Christmas cannot be on GlobalEventMenu because it affects seasons system
function	script	sChristmas	{
    // Determine the drop rates based on month, and Christmas proximity
    if (gettime(GETTIME_MONTH) == DECEMBER) {
        if (gettime(GETTIME_DAYOFMONTH) <= 26)
            .@m=10;
        else
            .@m=8;
    } else {
        .@m=5;
   }

    // Add Christmas drops
    addmonsterdrop(Moggun,      XmasCake,  80*.@m);
    addmonsterdrop(AlphaMouboo, XmasCake,  92*.@m);
    addmonsterdrop(BlueSlime,   XmasCake, 100*.@m);
    addmonsterdrop(SantaSlime,  XmasCake, 120*.@m);
    addmonsterdrop(IcedFluffy,  XmasCake, 150*.@m);
    addmonsterdrop(Yeti,        XmasCake, 500*.@m);

    addmonsterdrop(Bandit,      XmasCandyCane,  30*.@m);
    addmonsterdrop(Mouboo,      XmasCandyCane,  48*.@m);
    addmonsterdrop(WhiteSlime,  XmasCandyCane,  50*.@m);
    addmonsterdrop(RudolphSlime,XmasCandyCane, 100*.@m);
    addmonsterdrop(Fluffy,      XmasCandyCane, 200*.@m);
    addmonsterdrop(AzulSlime,   XmasCandyCane, 200*.@m);

    addmonsterdrop(Duck,        GingerBreadMan,  36*.@m);
    addmonsterdrop(WaterFairy,  GingerBreadMan, 100*.@m);

    // Event drop rates, multiplied by 10 during Christmas (see .@m)
    addmonsterdrop(Yeti,        ClosedChristmasBox, 350*.@m);
    addmonsterdrop(WaterFairy,  ClosedChristmasBox, 108*.@m);
    addmonsterdrop(AlphaMouboo, ClosedChristmasBox,  83*.@m);
    addmonsterdrop(IcedFluffy,  ClosedChristmasBox,  67*.@m);
    addmonsterdrop(BlueSlime,   ClosedChristmasBox,  42*.@m);
    addmonsterdrop(Moggun,      ClosedChristmasBox,  40*.@m);
    addmonsterdrop(SantaSlime,  ClosedChristmasBox,  36*.@m);
    addmonsterdrop(AzulSlime,   ClosedChristmasBox,  20*.@m);
    addmonsterdrop(Fluffy,      ClosedChristmasBox,  20*.@m);
    addmonsterdrop(RudolphSlime,ClosedChristmasBox,   8*.@m);
    addmonsterdrop(WhiteSlime,  ClosedChristmasBox,   3*.@m);
    addmonsterdrop(GiantMaggot, ClosedChristmasBox,   2*.@m);

    // This is not dropped outside December
    if (gettime(GETTIME_MONTH) == DECEMBER) {
        // Bugfix
        if (gettime(GETTIME_YEAR) == 2018)
            .@m+=10;
        addmonsterdrop(WaterFairy,  XmasGift,  6*.@m);
        addmonsterdrop(AlphaMouboo, XmasGift,  5*.@m);
        addmonsterdrop(IcedFluffy,  XmasGift,  4*.@m);
        addmonsterdrop(SantaSlime,  XmasGift,  3*.@m);
        addmonsterdrop(Fluffy,      XmasGift,  2*.@m);
        addmonsterdrop(AzulSlime,   XmasGift,  2*.@m);
    }

    // Change maps for Christmas Season (Specially LoF maps)
    addmapmask "003-1", MASK_CHRISTMAS;
    addmapmask "005-1", MASK_CHRISTMAS;
    addmapmask "009-1", MASK_CHRISTMAS;
    addmapmask "012-1", MASK_CHRISTMAS;
    addmapmask "020-2", MASK_CHRISTMAS;

    // Enable event
    set $EVENT$, "Christmas";
    //logmes "Enabled CHRISTMAS event.", LOGMES_ATCOMMAND;
    return;
}
// Valentine Day is handled by @event, this is only for @reloadmobdb
function	script	sValentine	{
    // Add Valentine drops
    addmonsterdrop(RedMushroom,         LoveLetter,  8);
    addmonsterdrop(ChocolateSlime,      LoveLetter,  4);
    return;
}

// This allows GMs to change seasons if needed
function	script	SeasonControl	{
    do
    {
        select
            "Summer Start",
            "Summer End",
            "Autumn Start",
            "Autumn End",
            "Winter Start",
            "Winter End",
            "Spring Start",
            "Spring End",
            "Abort";

        switch (@menu) {
            case 1: donpcevent("#SeasonCore::OnSummerStart"); break;
            case 2: donpcevent("#SeasonCore::OnSummerEnd"); break;
            case 3: donpcevent("#SeasonCore::OnAutumnStart"); break;
            case 4: donpcevent("#SeasonCore::OnAutumnEnd"); break;
            case 5: donpcevent("#SeasonCore::OnWinterStart"); break;
            case 6: donpcevent("#SeasonCore::OnWinterEnd"); break;
            case 7: donpcevent("#SeasonCore::OnSpringStart"); break;
            case 8: donpcevent("#SeasonCore::OnSpringEnd"); break;
        }
    } while (@menu != 9);
    return;
}

// If skip_checks is set, it'll ignore $@SEASON control.
// SeasonReload( {skip_checks} )
function	script	SeasonReload	{
    // Proccess skip_checks
    if (getarg(0,0))
        $@SEASON=99;

    // Summer extra drops
    if (season() == SUMMER && $@SEASON != SUMMER) {
        donpcevent("#SeasonCore::OnSummerStart");
    }
    // Summer end delete drops
    if (season() == AUTUMN && $@SEASON == SUMMER) {
        donpcevent("#SeasonCore::OnSummerEnd");
    }
    // Autumn extra drops
    if (season() == AUTUMN && $@SEASON != AUTUMN) {
        donpcevent("#SeasonCore::OnAutumnStart");
    }
    // Autumn end delete drops
    if (season() == WINTER && $@SEASON == AUTUMN) {
        donpcevent("#SeasonCore::OnAutumnEnd");
    }
    // Winter extra drops
    if (season() == WINTER && $@SEASON != WINTER) {
        donpcevent("#SeasonCore::OnWinterStart");
    }
    // Winter end delete drops
    if (season() == SPRING && $@SEASON == WINTER) {
        donpcevent("#SeasonCore::OnWinterEnd");
    }
    // Spring extra drops
    if (season() == SPRING && $@SEASON != SPRING) {
        donpcevent("#SeasonCore::OnSpringStart");
    }
    // Spring end delete drops
    if (season() == SUMMER && $@SEASON == SPRING) {
        donpcevent("#SeasonCore::OnSpringEnd");
    }

    // Non-season, but season-related
    // Christmas have a special feature
    if ($EVENT$ == "Christmas")
        sChristmas();
    if ($EVENT$ == "Valentine")
        sValentine();

    $@SEASON=season();
    return;
}

000-0,0,0,0	script	#SeasonCore	NPC_HIDDEN,{
    end;

OnSummerStart:
    debugmes "Summer Started";
    end;

OnSummerEnd:
    debugmes "Summer Ended";
    end;

OnAutumnStart:
    // Ched's Quest Winner
    .@nb = query_sql("SELECT c.name FROM `quest` AS i, `char` AS c WHERE i.quest_id="+SQuest_Ched+" AND i.char_id=c.char_id ORDER BY i.count2 DESC LIMIT 1", .@name$);
    $@AUTUMN_VICTOR$=.@name$[0];

    // Fancy trees
    addmapmask "012-1", MASK_AUTUMN;

    debugmes "Autumn Started";
    end;

OnAutumnEnd:
    // Ched's rewards can't be claimed anymore. Delete that from all players.
    DelQuestFromEveryPlayer(SQuest_Ched);

    removemapmask "012-1", MASK_AUTUMN;
    debugmes "Autumn Ended";
    end;

OnWinterStart:
    debugmes "Winter Started";
    end;

OnWinterEnd:
    debugmes "Winter Ended";
    end;

OnSpringStart:
    debugmes "Spring Started";
    end;

OnSpringEnd:
    debugmes "Spring Ended";
    end;

OnInit:
    SeasonReload(1);
    end;

OnHour00:
    if ($@SEASON != season()) {
        SeasonReload();
    }
    end;
}

// St. Patrick Day utils
-	script	sPatrick	NPC_HIDDEN,{
OnInit:
    setarray .maps$,
                    "005-1",
                    "013-1",
                    "014-1",
                    "014-2",
                    "014-3",
                    "014-4",
                    "014-5",
                    "017-1",
                    "018-2",
                    "018-4",
                    "018-5",
                    "soren";
    end;
OnMyMobDeath:
    end;
OnClock0000:
OnClock6000:
OnClock1200:
OnClock1500:
OnClock1800:
OnClock2100:
    .@d=gettime(GETTIME_DAYOFMONTH);
    // Patrick Day should work only in 2 luck days according to Saulc
    // If this is required use $PATRICK_DAYCTRL so the days before $@PATRICK_DAYMAX
    // act with 100% chances (determinism).
    // As it is being spawn 4 times at a day (like TMW-BR events), because it is
    // flatly and outright IMPOSSIBLE to add a permanent spawn, that is not required.
    if ($EVENT$ == "St. Patrick" && .@d <= $@PATRICK_DAYMAX) {
        for (.@i=0; .@i < getarraysize(.maps$); .@i++) {
            .@m$=.maps$[.@i];
            .@x=getmapinfo(MAPINFO_SIZE_X, .@m$)-20;
            .@y=getmapinfo(MAPINFO_SIZE_Y, .@m$)-20;

            // Remove previously spawned clovers
            killmonster(.@m$, "sPatrick::OnMyMobDeath");
            // It is one clover for each 225 tiles (about a 25x25 square)
            .@maparea=(.@x-20)*(.@y-20);
            .@mobs=max(1, .@maparea/225);
            areamonster .@m$, 20, 20, .@x, .@y, strmobinfo(1, StPatricksClover), StPatricksClover, .@mobs, "sPatrick::OnMyMobDeath";
        }
    }
    end;

// Remove previously spawned clovers for event end
// And then disables the NPC
OnCleanUp:
    for (.@i=0; .@i < getarraysize(.maps$); .@i++) {
        .@m$=.maps$[.@i];
        killmonster(.@m$, "sPatrick::OnMyMobDeath");
    }
    disablenpc "sPatrick";
    end;
}

