// TMW2 scripts.
// Authors:
//    Jesusalva
//    TMW Org.
// Description:
//    HUB functions (Login, Logout, Death)

// HUB_Login ()
function	script	HUB_Login	{
    getmapxy(.@mapa$, .@a,.@b, 0);

    // Login on Blue Sage Workshop/Library
    if (.@mapa$ == "020-7-1") {
        addtimer(1000, "#BlueSageHUB::OnCycle");
    }

    return;
}

// HUB_Logout ( {dead} )
function	script	HUB_Logout	{
    .@dead=getarg(0, false);
    getmapxy(.@mapa$, .@a,.@b, 0);

    // Vanished on Cindy Cave
    if (.@mapa$ == "021-4" && strcharinfo(0) == $@CINDY_HERO$) {
        donpcevent("Cindy#Outside::OnReckless");
        recovery(getcharid(3));
        warp any("010-1", "010-2"), 0, 0;
        percentheal -100, -100;
        sc_start2 SC_POISON, 1, 90, 10000;
        heal -1, -1;
    } else if (.@mapa$ == "021-4") {
        .@pl = getmapusers("021-4")-1;
        if (.@pl < 1)
            donpcevent("Cindy#Outside::OnCleanUp");
        recovery(getcharid(3));
        warp "Save", 0, 0;
    }
    // Logout while donating blood
    if (getq(HurnscaldQuest_BloodDonor) == 2) {
        slide 35, 28;
        setpcblock(PCBLOCK_ATTACK|PCBLOCK_SKILL|PCBLOCK_USEITEM|PCBLOCK_MOVE, false);
        setq HurnscaldQuest_BloodDonor, 0, gettimetick(2)+3600; // one hour penalty
    }
    // Logout/Death on Nard's ship hold
    if (.@mapa$ ~= "002-2" || .@mapa$ ~= "nard*") {
        setq2 ShipQuests_Peter, 0;
        setq3 ShipQuests_Peter, -1;
    }
    // Logout on botcheck area
    if (.@mapa$ ~= "botcheck" && !.@dead) {
        if (!is_staff())
            atcommand "@jail "+strcharinfo(0);
    }
    // Died on Terranite Cave where exp penalty is lower
    if (.@mapa$ == "015-6" && .@dead) {
        @deathpenalty_override=2;
        @deathpenalty_realvalue=readparam(BaseExp);
        @deathpenalty_realvaljob=readparam(JobExp);
    }
    // Died or logged out on Blue Sage House
    if (.@mapa$ == "020-7-1") {
        callfunc("BSClearNest", @nestid);
    }
    // Died or logged out during Sagratha Fight
    if (.@mapa$ ~= "sgt2*") {
        setq1 HurnscaldQuest_Sagratha, 3;
        setq3 HurnscaldQuest_Sagratha, 0;
    }
    // First death produces a warning message
    if (PC_DIE_COUNTER <= 1 && .@dead) {
        dispbottom l("Dying outside a town square will cause EXP loss.");
    }
    // If you were travelling and died/logged out, cleaning is needed
    if (@timer_navio_running) {
        @timer_navio_running=0;
        // Logged out? Correct your position to inside the ship
        // Of course, this is messy... But still better than Save Point
        if (!.@dead) {
            if (.@mapa$ ~= "002-*")
                warp "002-1", 55, 40;
            else
                warp "Save", 0, 0;
        }
    }
    // Crazyfefe hot fix
    if (.@dead) {
        // It was PK
        if (killerrid > 2000000 && killerrid < 2100000) {
            // PVP flag was off
            if (!getmapflag(.@mapa$, mf_pvp) && !getmapflag(.@mapa$, mf_pvp_noparty) && !getmapflag(.@mapa$, mf_pvpnoguild)) {
                recovery(getcharid(3));
                warp .@mapa$, .@a, .@b;
                dispbottom l("REVENGE TIME!");
                .@trueid=getcharid(3);
                //detachrid();
                attachrid(killerrid);
                setpcblock(PCBLOCK_ATTACK|PCBLOCK_SKILL|PCBLOCK_USEITEM|PCBLOCK_COMMANDS, true);
                sc_start SC_WALKSPEED,120000,50;
                //sc_end SC_CASH_PLUSEXP; // Uncomment this to annulate premium boosts
                sc_end SC_OVERLAPEXPUP;
                sc_start SC_OVERLAPEXPUP, 300000, -20;
                dispbottom l("For cowardingly killing in a \"secure\" area, you will be severely punished.");
                addtimer(15000, "#mobptsys::OnUnlock");
                percentheal -88, -100;
                detachrid();
                attachrid(.@trueid);
            }
        }
    }

    // This allows code to override death penalty, just once:
    // @deathpenalty_override
    // Valid values: 1- No penalty. 2- Halved penalty.
    // You must also set: @deathpenalty_realvalue and @deathpenalty_realvaljob
    if (@deathpenalty_override && .@dead) {
        if (is_staff())
            debugmes("Old values: %d %d Current Values: %d %d", @deathpenalty_realvalue, @deathpenalty_realvaljob, readparam(BaseExp), readparam(JobExp));
        addtimer(300, "#QuirinoHUB::OnNoPenaltyCommand");
    }

    return;
}

