// TMW-2 Script
// Author: Jesusalva
// Desc:   Grand Hunter Pet Quest Utils.
// Note:   Remember to update GHQ and pet_detective on LoF Village (017-1)

// Core logic for Pet Detective. Doesn't checks for stock, allowing exploits.
// MobID, ItemID, Base Value
function	script	PDQ_InnerSwitch	{

    .@mid=getarg(0);
    .@iid=getarg(1);
    .@val=getarg(2, 30);

    .@price=.@val*(PETMEMO[GHQ_GetQuestIDByMonsterID(.@mid)]+1)+PETMEMO[GHQ_GetQuestIDByMonsterID(.@mid)];
    mesn;
    mesq l("That'll cost you @@/@@ @@.", countitem(.@iid), .@price, getitemlink(.@iid));
    next;
    if (askyesno() == ASK_YES) {
        if (countitem(.@iid) >= .@price) {
            delitem .@iid, .@price;
            makepet .@mid;
            getexp .@price*BaseLevel, 0;
            PETMEMO[GHQ_GetQuestIDByMonsterID(.@mid)]+=1;
            mesn;
            mesq l("There you go!");
            return 1;
        } else {
            mesn;
            mesq l("You don't have that, lying is bad for health...");
        }
    }
    return 0;
}
