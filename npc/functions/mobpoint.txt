// TMW2 Scripts
// Author: Crazyfefe
//         Jesusalva
// Desc:   Mob Points for Aidan & Ishi. You will gain MONSTER-LEVEL mob points.

// fix_mobkill(mobID) → Manual fix for scripted mobs
function	script	fix_mobkill	{
    killedrid=getarg(0);
    doevent "#mobptsys::OnNPCKillEvent";
    return;
}

function	script	mobpoint	{
    if (!MPQUEST)
        return;
    //if (killedrid < 1002) goto L_Return;

    // You get MobLv + 10% as MobPoints.
    // So a level 100 monster gives you 110 MobPt.
    .@addval=strmobinfo(3,killedrid)*11/10;
    Mobpt = Mobpt + .@addval;
    return;

}

-	script	#mobptsys	NPC_HIDDEN,{
    end;

OnUnlock:
    if (checkpcblock() & PCBLOCK_ATTACK)
        setpcblock(PCBLOCK_ATTACK|PCBLOCK_SKILL|PCBLOCK_USEITEM|PCBLOCK_MOVE|PCBLOCK_COMMANDS, false);
    end;

OnNPCKillEvent:
    $MONSTERS_KILLED+=1;
    if (killedrid == MonsterKing) {
        announce "An illusionary monster king was killed.", bc_all | bc_pc;
        getexp min(641500, BaseLevel**3), 0;
    }

    // Remove undue Job exp
    // The check is probably correct, but setparam is not working =/
    /*
    if (strmobinfo(7, killedrid) == 0 && readparam(JobExp) > 0) {
        setparam(JobExp, readparam(JobExp)-1);
    }
    */

    // call functions
    callfunc "mobpoint";
    callfunc "mobhunter";
    callfunc "SQuest_Hasan";
    callfunc "dausen_mobtutorial";
    callfunc "Guardhouse_RandQuestCheck";

    killedrid=0;
    end;

// When you kill a player, some special care is needed
// Only a few maps will give you experience for PK: Tulimshar's Guards Arena,
// Frostia Imperial PVP Arena, Call Of Dusty, Arena Quirino Voraz.
OnPCKillEvent:
    $PLAYERS_KILLED+=1;
    // killedrid
    .@m$=getmap();
    .@bxp=readparam(BaseLevel, killedrid);
    .@jxp=readparam(JobLevel, killedrid);
    if (.@m$ ~= "001-8") {
        // Quirino Voraz PVP Arena
        // You get 5 times killed player level, and 1 time job level
        getexp .@bxp*5, .@jxp;
    } else if (.@m$ ~= "ARENA" || .@m$ ~= "003-13") {
        // Tulimshar Duel Arena
        // You get 3 times killed player level, and 2 times job level
        getexp .@bxp*3, .@jxp*2;
    } else if (.@m$ ~= "001-10") {
        // Call Of Dusty
        // You get 3 times killed player level, and 3 times job level
        getexp .@bxp*3, .@jxp*3;
    } else if (.@m$ ~= "001-10-1") {
        // Call Of Dusty Boss Room
        // You _may_ get a Bottled Dusty at random, but dead player status affect
        .@bagistr=(readparam(bAgi, killedrid)*2)+readparam(bDex, killedrid);
        //.@b2=readparam(MaxHp, killedrid);
        if (.@bagistr > 20)
            if (rand(0,250) < readparam(bLuk)+readparam(bLuk, killedrid))
                getitem BottledDust, 1;
    } else {
        // Anywhere else
        // You get 0.5 times killed player level, and 0 times job level
        getexp (.@bxp/2), 0;
    }
    end;

}
