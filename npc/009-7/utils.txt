// TMW2: Moubootaur Legends scripts.
// Author:
//    Jesusalva
// Description:
//    Real Estate System
//    Utils take care of NPCs - Their code, and enable/disable using check_cell
//    This file is custom to every room

// ID: 5
// $ESTATE_OWNER[.id] → Account ID owner of the Real Estate
// $ESTATE_OWNERNAME$[.id] → Human readable name of Real Estate owner
// $ESTATE_RENTTIME[.id] → When the rent will expire
// $ESTATE_MOBILIA_2[.id] → Bitmask of mobilia currently purchased on Monster Collision (6) (Use on walls only)
// $ESTATE_MOBILIA_4[.id] → Bitmask of mobilia currently purchased on Air Collision (2)
// $ESTATE_MOBILIA_8[.id] → Bitmask of mobilia currently purchased on Water Collision (3)
// $ESTATE_MOBILIA_32[.id] → Bitmask of mobilia currently purchased on Yellow Collision (4)
// $ESTATE_MOBILIA_64[.id] → Bitmask of mobilia currently purchased on Player Collision (5)
// $ESTATE_MOBILIA_128[.id] → Bitmask of mobilia currently purchased on Normal Collision (1)
// $ESTATE_PASSWORD$[.id] → Password to enter the estate. If it is "", then no password required
// Note: GMs and Administrators can always use super password "mouboo" to enter a locked estate
// $ESTATE_DOORBELL[.id] → If doorbell is disabled (enabled by default)

// REAL_ESTATE_CREDITS → Credits equivalent to GP the player have. Will be used first.

// The sign is the main controller for rent system
// Doorbell is the main controller for indoor
// This is the NPC script controller
009-7,0,0,0	script	NPCs#RES_0097	NPC_HIDDEN,{
    // load_npc ( name , map, x , y{, cell} )
    function load_npc {
        if (checknpccell(getarg(1), getarg(2), getarg(3), getarg(4, cell_chknopass))) {
            enablenpc getarg(0);
            //debugmes "ENABLING NPC %s", getarg(0);
        } else {
            disablenpc getarg(0);
            //debugmes "Disabling NPC %s", getarg(0);
        }

        /*
        debugmes "----- %s (%d,%d) cell report", getarg(1), getarg(2), getarg(3);
        debugmes "cell_chknopass: %d", checknpccell(getarg(1), getarg(2), getarg(3), cell_chknopass);
        debugmes "cell_chknoreach: %d", checknpccell(getarg(1), getarg(2), getarg(3), cell_chknoreach);
        debugmes "cell_chkbasilica: %d", checknpccell(getarg(1), getarg(2), getarg(3), cell_chkbasilica);
        debugmes "";
        debugmes "cell_chkwater: %d", checknpccell(getarg(1), getarg(2), getarg(3), cell_chkwater);
        debugmes "cell_chkwall: %d", checknpccell(getarg(1), getarg(2), getarg(3), cell_chkwall);
        debugmes "cell_chkcliff: %d", checknpccell(getarg(1), getarg(2), getarg(3), cell_chkcliff);
        debugmes "----- Npc Id: %s", getarg(0);
        */
        return;
    }
    end;

OnInit:
    // Estate Settings
    .id=5; // Estate ID
    .mapa$="009-7"; // Map name

    // NPC Settings
    .sex = G_OTHER;
    .distance = 3;
    end;

// Load or unload accordingly
OnReload:
    //debugmes "[REAL ESTATE] NPC ONRELOAD";
    // load_npc ( name , map, x , y{, cell} )
    load_npc("Wardrobe#RES_0097", .mapa$, 21, 23);
    load_npc("Piano#RES_0097"   , .mapa$, 34, 25);
    end;

}

009-7,21,23,0	script	Wardrobe#RES_0097	NPC_NO_SPRITE,{
    openstorage;
    end;

OnInit:
    .distance=3;
    end;
}


009-7,34,25,0	script	Piano#RES_0097	NPC_NO_SPRITE,{
    mesc l("Do you want to play a song?");
    mesc l("This is not saved.");
    select
        l("Nothing"),
        l("Default"),
        l("Indoors 1 (Peace)"),
        l("Indoors 2 (Dimonds)"),
        l("TMW Adventure"),
        l("Sailing Away!"),
        l("Magick Real"),
        l("The Forest"),
        l("Dragons and Toast"),
        l("Unforgiving Lands"),
        l("Arabesque (Action)"),
        l("No Chains (Tulimshar)"),
        l("School of Quirks (Candor)"),
        l("Cake Town (Hurnscald)"),
        l("Steam (LoF Village)"),
        l("Woodland Fantasy"),
        l("Birds in the Sunrise");

    mes "";
    .@m$="";
    switch (@menu) {
        case 1:
            close;
        case 2:
            .@m$="8bit_the_hero.ogg"; break;
        case 3:
            .@m$="peace.ogg"; break;
        case 4:
            .@m$="peace2.ogg"; break;
        case 5:
            .@m$="tmw_adventure.ogg"; break;
        case 6:
            .@m$="sail_away.ogg"; break;
        case 7:
            .@m$="magick_real.ogg"; break;
        case 8:
            .@m$="dariunas_forest.ogg"; break;
        case 9:
            .@m$="dragon_and_toast.ogg"; break;
        case 10:
            .@m$="Unforgiving_Lands.ogg"; break;
        case 11:
            .@m$="Arabesque.ogg"; break;
        case 12:
            .@m$="mvrasseli_nochains.ogg"; break;
        case 13:
            .@m$="school_of_quirks.ogg"; break;
        case 14:
            .@m$="caketown.ogg"; break;
        case 15:
            .@m$="steam.ogg"; break;
        case 16:
            .@m$="woodland_fantasy.ogg"; break;
        case 17:
            .@m$="tws_birds_in_the_sunrise.ogg"; break;

    }
    changemusic "009-7", .@m$;
    close;

OnInit:
    .distance=3;
    end;
}

