// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Heroes Hold Exchanger - Level 4

018-2-4,54,28,0	script	Ultimate Exchanger#0	NPC_M_COINKEEPER,{
    .@q=getq2(LoFQuest_HH);
    if (.@q & HH_EXPERT)
        openshop;
    goodbye;
    closedialog;
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, TopHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SaviorArmor);
    setunitdata(.@npcId, UDT_WEAPON, JeansChaps);
    setunitdata(.@npcId, UDT_HEADBOTTOM, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 25);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex = G_OTHER;
    .distance = 5;
    npcsit;

	tradertype(NST_CUSTOM);

	sellitem BlacksmithAxe,30000;
	sellitem Judgement,16200;

	sellitem MercBoxB,2200;
	sellitem GoldenGift,1920;

	sellitem PlatinumOre,1600;
	sellitem IridiumOre,800;

	sellitem PoisonAmmoBox,2160;
	sellitem PoisonArrow,12;
	end;

OnCountFunds:
	setcurrency(countitem(HeroCoin));
	end;

OnPayFunds:
	if( countitem(HeroCoin) < @price )
		end;
	delitem HeroCoin,@price;
	purchaseok();
	end;

}

018-2-4,54,49,0	duplicate(Ultimate Exchanger#0)	Ultimate Exchanger#1	NPC_M_COINKEEPER

