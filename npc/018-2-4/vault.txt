// TMW2/LoF Script.
// Author:
//    Jesusalva
// Notes:
//    Based on BenB idea.

018-2-4,23,24,0	script	Vault#01824a	NPC_NO_SPRITE,{
    if (MERC_RANK) goto L_Debug;
    mesn;
    mesq l("There's a shiny safe here. How much money is inside? Nobody is looking at you, great!");
    // 3*3 = 9 possibilities, 6~8 attempts
    if (LockPicking(3, 3)) {
        Zeny=Zeny+$VAULT_01824;
        $VAULT_01824=0;
        mesn;
        mesq l("Booty!");
    } else {
        mesn;
        mesq l("Arrested!");
        .@inch=(Zeny/100);
        Zeny-=.@inch;
        $VAULT_01824+=.@inch;
        atcommand("@jailfor 5mn "+strcharinfo(0));
    }
    close;

// TODO: And remove from here.
L_Debug:
    mesn;
    mesq l("Thiefs frequently attack this vault, and locking it again is a pain. If you break the lock you'll need to pay 100 GP.");
    if (Zeny < 100)
        close;
    // 2*3 = 6 possibilities, 5 attempts
    if (ToDoMerc(3, 3)) {
        getexp 20, 90;
        $VAULT_01824+=2;
        mesn;
        mesq l("Safe again! You've gained some experience for your hard work!");
    } else {
        mesn;
        mesq l("Dargh, you broke the lock!!");
        Zeny-=100;
    }
    close;

OnInit:
    .distance=3;
    end;

OnClock0201:
OnClock1216:
    $VAULT_01824+=rand(15,35);
    end;
}


018-2-4,23,45,0	duplicate(Vault#01824a)	Vault#01824b	NPC_NO_SPRITE

