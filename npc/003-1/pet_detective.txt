// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Sell pets (LoFQuest_Pets) (mesn Ace Ventura)

003-1,62,38,0	script	Pet Detective	NPC_PET_TAMER,{
    .@q=getq(LoFQuest_Pets);
    if (.@q) goto L_Menu;
    mesn l("Ace Ventura");
    mesq l("I investigate missing pets. Many flee from their owners because they die pathetically or forgot to feed them.");
    next;
    mesn l("Ace Ventura");
    mesq l("If they at least read the @@, they wouldn't have such problems.", getitemlink(PetcaringGuide));
    mesq l("The Pet Caring Guild will collect them after a while, and capture many others.");
    next;
    mesn l("Ace Ventura");
    mesq l("So, what do you say about a deal? I'll affiliate you on the Pet Caring Guild for only 5.000 GP!");
    if (Zeny < 5000)
        close;
    next;
    if (askyesno() == ASK_NO)
        close;
    mes "";
    if (Zeny < 5000) {
        mesn l("Ace Ventura");
        mesq l("Oh, but we are affiliated with Jesusalva, whom hates cheaters. So, meet your dismissal!");
        percentheal -100, 0;
        /*
        .@val=(Zeny/10)*2;
        Zeny=(Zeny/10)*8; // You pay 20% of your GP as tax-money. Hasan gladly takes the money. Jesusalva's happy.
        $HASAN_GP+=.@val;
        */
        close;
    }
    Zeny=Zeny-5000;
    getexp 0, 500;
    setq LoFQuest_Pets, 1;
    setarray PETMEMO, 0, 0;
    mesn l("Ace Ventura");
    mesq l("Congrats! You are now part from the Pet Caring Guild. You can now buy pets! %%G");
    close;

L_Menu:
    if (!.stock)
        goto L_OutOfStock;
    mesn l("Ace Ventura");
    mesq l("I currently have some pets with me. For most, you need to finish their Grand Hunter Quest, of course.");
    mes "";
    mesc l("Note: some pets still weren't implemented!");
    mesc l("If you notice your pet is missing, this means nobody cleared them yet."), 1;
    mesc l("So, you are the first and must contact Jesusalva %%g"), 1;
    mes "";
    select
        rif(GHMEMO[GHQ_GetQuestIDByMonsterID(Maggot)] >= 10000 && !countitem(MaggotCocoon), l("Cute Maggot")),
        rif(GHMEMO[GHQ_GetQuestIDByMonsterID(ForestMushroom)] >= 10000 && !countitem(ForestShroomEgg), l("Forest Mushroom")),
        rif(#LOGIN_ALLTIME >= 12 && !countitem(PiouEgg), l("Piou")),
        l("Nothing at the moment.");

    mes "";
    switch (@menu) {
        case 1:
            if (PDQ_InnerSwitch(Maggot, MaggotSlime, 35))
                .stock-=1;
            break;
        case 2:
            if (PDQ_InnerSwitch(ForestMushroom, MushroomSpores, 30))
                .stock-=1;
            break;
        case 3:
            if (PDQ_InnerSwitch(Piou, PiouFeathers, 80))
                .stock-=1;
            break;
        default:
            closedialog;
            goodbye;
            close;
    }

    next;
    goto L_Menu;

L_OutOfStock:
    mesn l("Ace Ventura");
    mesq l("Sorry, I don't have any pets at the moment.");
    close;

OnInit:
    .sex=G_MALE;
    .distance=5;
    .stock=1;
    end;

OnSun0000:
OnWed1200:
    .stock+=2;
    end;
}

