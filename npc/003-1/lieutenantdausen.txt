// TMW2 scripts.
// Authors:
//    Saulc
//    Jesusalva
//    acsvln
//    gnulinux
// Description:
//    Help Tulimshar guards
// Quest variable:
//    TulimsharQuests_Guards
// Quest stages:
//    0 - not started
//    1 - Lieutenant Dausen asked for help Tulimshar guards
//    2 - completed
//    3 - Reward given

003-1,108,30,0	script	Lieutenant Dausen	NPC_PLAYER,{
    function DausenMobTutorial;

    .@q = getq(TulimsharQuest_WaterForGuard);
    .@t = getq(TulimsharQuest_MobTutorial);

    switch (.@q) {
    case 0:
        mesn;
        mesq l("Greetings, wanderer. I am @@, chief of the Tulimshar guards. My wards are dying from dehydration in the sun. Bring them water and you will earn a reward.", .name$);
        break;
    case 1:
        mesn;
        mesq l("Please help my wards!");
        break;
    case 2:
        goto L_Reward;
        break;
    case 3:
        mesn;
        mesq l("Thank you for your help.");
        break;
    default:
        end;
    }
    next;
    select
        rif(!.@q, l("Yes sir. I will help them.")),
        rif(getq(TulimsharQuest_Hasan) == 1, l("A guy named Hasan stole me!")),
        rif (strcharinfo(2) == "Monster King", l("I'm with the Monster King.")),
        l("What can you say about the monsters here?"),
        l("Good bye, sir.");
    mes "";
    switch (@menu) {
        // Thristy Guards Quest
        case 1:
            setq TulimsharQuest_WaterForGuard, 1;
            mes "";
            mesn;
            mesq l("Good luck! Come for remuneration when you finish!");
            next;
            mesc l("Protip: You need an @@ full of water to get a reply from guards.", getitemlink(EmptyBottle));
            break;
        // Hasan Quest
        case 2:
            setq TulimsharQuest_Hasan, 2;
            speech S_FIRST_BLANK_LINE, lg("Ah, Hasan... Sorry pal, afraid I can't do anything for you. Try talking to his mother Sorfina, she is on Mahoud's house, near the Inn.");
            break;
        // The Monster King guild have a special menu
        case 3:
            if (strcharinfo(2) == "Monster King") goto L_MKControl;
            break;
        // Monster info
        case 4:
            DausenMobTutorial();
            break;
        default:
            closedialog;
            goodbye;
            break;
    }
    close;

// Reward for quest completion
L_Reward:
        mesn;
        mesq l("Thank you, here is your reward.");

        inventoryplace TulimsharGuardBoots, 1, TulimsharGuardCard, 1;
        getitem TulimsharGuardBoots, 1;
        getitem TulimsharGuardCard, 1;
        setq TulimsharQuest_WaterForGuard, 3;

        next;

        speech 0x0,
            l("Wait a minute..."),
            l("The Tulimshar guards needs an freelance employee who would help us in our work. We are searching for people as you."),
            l("Take this badge, so you can get access to the guard house. You will find more work there. Bye, and good luck!");
    return;

// Mob Tutorial Quest
function DausenMobTutorial {
    if (BaseLevel < 9) {
        mesn;
        mesq l("They're strong, so keep fighting Maggots and Scorpions which you're used to, until you get stronger.");
        close;
    }
    .@t = getq(TulimsharQuest_MobTutorial);
    .@k = getq2(TulimsharQuest_MobTutorial);
    // You need a Guard Card, Dausen have a bad memory :p
    if (!countitem(TulimsharGuardCard)) {
        mesn;
        mesq l("Well, I do not trust you yet. You literally were just brought by the shore, and even if Nard and Lua seems to trust you...");
        next;
        mesn;
        mesq l("...I still need to cover up if they're mistaken. Give me a reason to tell you secrets, show me you're concerned with Tulimshar safety and don't want just to be strong.");
        next;
        mesn;
        mesq l("Power in the wrong hands is nothing but a burden and a reason for others to cry. That's exactly what I don't want in this town.");
        close;
    }
    switch (.@t) {
    case 0:
    case 1:
        // Reward
        if (.@k >= 10) {
            mesn;
            mesq l("Welp, you killed 10 Crocs. They're not dangerous, as you see. That's why we don't bother in cleaning them up.");
            setq TulimsharQuest_MobTutorial, 2, 0;
            // 30% of exp values, and 6 GP/Level
            getexp 75, 0;
            Zeny+=54;
            close;
        }
        // Quest Body
        mesn;
        mesq l("So. Uhm. The monsters here have varying levels of strength... I think the best way is to witness that yourself.");
        next;
        mesn;
        mesq l("Do you see the crocs, with their claws and such? They have high defense, this means your attacks deal less damage.");
        next;
        mesn;
        mesq l("They're not too dangerous, but takes a lot to kill. So, if you kill 10 of them, I'll know you're dedicated in learning which monsters are out there.");
        next;
        mesn;
        mesq l("Can you do that? I'll be waiting!");
        compareandsetq TulimsharQuest_MobTutorial, 0, 1;
    case 2:
    case 3:
        // TODO: Blubs and Ducks
        // Level Requeriment (same as blubs)
        if (BaseLevel < 14) {
            mesn;
            mesq l("There's some diversity, but you should keep aiming at helping people and killing small-fry. If you ever want a challenge, there's a Giant Maggot inside the town which will one-shot you.");
            next;
            mesn;
            mesq l("Have you found Tulimshar's Secret Beach yet? Tulimshar is full of secrets. Some NPCs which only say hi may say something else depending on your level or insistence.");
            close;
        }
        // Reward
        if (.@k >= 37) {
            mesn;
            mesq l("Hmm, that's some progress. Nobody goes to the beach because these slimes, but it looks like this might change sometime soon.");
            next;
            mesn;
            mesq l("Also, the Inn folks said the Ducks keep bothering them, but they've noticed a small decrease lately.");
            next;
            mesn;
            mesq l("I don't know what you did, but both the Ducks and Blubs did got scared. I thank you in the name of the city guard. Please come back later.");
            setq TulimsharQuest_MobTutorial, 4, 0;
            // 30% of exp values, and 6 GP/Level
            getexp 210, 0;
            Zeny+=84;
            close;
        }
        // Quest Body
        mesn;
        mesq l("West of here is a beach. There's a secret passage to it, underground. In there you'll find slime-like creatures called Blubs.");
        next;
        mesn;
        mesq l("Some are small, others are bigger. But they all fight togheter, so be mindful when they're in groups.");
        next;
        mesn;
        mesq b(l("The big one without hat"))+" "+l("is your target. They spawn smaller versions of itself! Kill @@ of them and make the beach safe for tourists.", 37);
        next;
        mesn;
        mesq l("Or make yourself useful killing a Duck. They steal all Cherry Cake from the Inn and the staff is getting angry at me. It'll serve, too.");
        compareandsetq TulimsharQuest_MobTutorial, 2, 3;
    case 4:
    case 5:
        // TODO: Desert Log Head, Desert Bandits, Sarracenus
        if (BaseLevel < 22) {
            mesn;
            mesq l("Have you visited the mines already? Tycoon is in charge of the security operations in there. He might need your help more than I do.");
            close;
        }
        // Reward
        if (.@k >= 100) {
            mesn;
            mesq l("Not bad. These are the main threat we have to fend off, along snakes and black scorpions.");
            setq TulimsharQuest_MobTutorial, 6, 0;
            // 30% of exp values, and 6 GP/Level
            getexp 600, 0;
            Zeny+=132;
            close;
        }
        // Quest Body
        mesn;
        mesq l("So... Have you tried visiting the Canyons? I know, it is a crazy idea.");
        next;
        mesn;
        mesq l("But if you want to be guard, you must be brave! Go fight your fears, and go kill some bandits like a decent guard.");
        next;
        mesn;
        mesq l("Well, if you're scared, you can kill Desert Log Heads, but they'll have a smaller worth.");
        next;
        mesn;
        mesq l("Bring me @@ Bandits or Sarracenus heads, or the double of that in Desert Log Heads, and I'll consider you brave enough.", 50);
        compareandsetq TulimsharQuest_MobTutorial, 4, 5;
    case 6:
    case 7:
        if (BaseLevel < 29) {
            mesn;
            mesq l("You're brave but weak. Go grind some levels, go make a wooden sword, I don't know.");
            close;
        }
        // Reward
        if (.@k >= 300) {
            mesn;
            mesq l("Alright, you've not only proven your worth, but you've went through most monsters in the desert close to the town.");
            next;
            mesn;
            mesq l("Knowledge is power... And now you have both. Use them wisely.");
            next;
            inventoryplace Coal, 6;
            getitem Coal, 6;
            setq TulimsharQuest_MobTutorial, 8, 0;
            // ~30% of exp values, and 6 GP/Level (ofc you cannot complete yet)
            getexp 2100, 0;
            Zeny+=200;
            mesn;
            mesq l("This coal will aid you to craft better weapons later. It's a token of appreciation. Good job.");
            close;
        }
        // Quest Body
        mesn;
        mesq l("You've passed the test of courage. Bravure you have, but are you a real adventurer?");
        next;
        mesn;
        mesq l("You have good intentions, be brave, but in a world where power is measured in levels, numbers have more meaning than they should.");
        next;
        mesn;
        mesq l("I'll present you three kind of strong monsters. Snakes are fast and dangerous. Giant Maggots are slow and dangerous. Black Scorpions have average speed and are... deadly.");
        next;
        mesn;
        mesq l("Giant Maggots are worth 1 point, Snakes are worth 5 points and Black Scorpions are worth 10 points. The ones in caves doesn't count. Bring me 300 points. Good luck!");
        compareandsetq TulimsharQuest_MobTutorial, 6, 7;
    default:
        mesn;
        mesq l("Eh? Well, you're in a desert. You can see Maggots and Scorpions, they're very common on these parts. Giant Maggots are very dangerous, but also very slow. If you know how to fight, they will yield you lots of experience.");
        next;
        mesn;
        mesq l("West of here is a beach. In said beach there are blubs, they don't attack but they walk in packs. Be careful if you provoke too many of them.");
        next;
        mesn;
        mesq l("Also, south of here are mines. Talk to Tycoon for information about it.");
        next;
        mesn;
        mesq l("By last, east of here are the Canyons. Do not go there before level 20, and even then, do not engage snakes in combat. They are fast and very dangerous.");
        break;
    }
    return;
}

// The Monster King guild have a special menu
L_MKControl:
    mesn;
    mes l("Oh noes! You've found the Tulimshar control panel!");
    next;
    select
        l("Abort"),
        l("Initiate a siege");
    mes "";
    if (@menu == 2) {
        doevent "Lieutenant Dausen::OnStartSiege";
        closedialog;
    }
    close;

OnGuardDeath:
    end;

OnMKSiege:
OnStartSiege:
    kamibroadcast(col("WARNING! WARNING! Monster Army moving towards Tulimshar!!",1));
    do_siege("003-1", "004-1", "TULIM", TP_TULIM, .name$, .siegetime);
    initnpctimer;
    end;

// Timers
OnTimer5000:
    .siegetime+=5;
    do_siege("003-1", "004-1", "TULIM", TP_TULIM, .name$, .siegetime);
    switch (.siegetime) {
    // Monster Army arrives in town
    case 60:
        disablenpc "Ched";
        disablenpc "Aahna";
        disablenpc "Constable Perry";
        disablenpc "Cyndala";
        disablenpc "Eomie";
        disablenpc "Eugene";
        disablenpc "Gladys";
        disablenpc "Inac";
        disablenpc "Ishi";
        disablenpc "Itka";
        disablenpc "Jakod";
        disablenpc "Jerican";
        disablenpc "Mahoud";
        disablenpc "Marius The Bard";
        disablenpc "Michel";
        disablenpc "Neko";
        disablenpc "Nina The Traveler";
        disablenpc "Sarah";
        disablenpc "Silvia";
        disablenpc "Tamiloc";
        disablenpc "Tinris";
        disablenpc "#water_animation0";
        disablenpc "Sailors#003-1";
        disablenpc "Guard Philip";
        disablenpc "Guard Avou";
        disablenpc "Guard Benji";

        // Create guards
        /*
        monster("003-1", 98, 100, ("Guard Philip"), FallenGuard3, 1, "Lieutenant Dausen::OnGuardDeath", Size_Medium, 2);
        monster("003-1",102, 121, ("Guard Avou"), FallenGuard3, 1, "Lieutenant Dausen::OnGuardDeath", Size_Medium, 2);
        monster("003-1", 58, 158, ("Guard Benji"), FallenGuard3, 1, "Lieutenant Dausen::OnGuardDeath", Size_Medium, 2);
        */
        break;
    // Monster Army deployed in town
    case 90:
        disablenpc "Aidan";
        disablenpc "Inar";
        disablenpc "Malivox";
        disablenpc "Estard";
        disablenpc "Malindou";
        disablenpc "Jhedia";
        disablenpc "Swezanne";
        disablenpc "Luca";
        disablenpc "Colin";
        break;
    // Monster army have withdrawn completly
    case MK_SIEGE_DURATION:
        .siegetime=0;
    announce(("Tulimshar siege is over!"), bc_all);
        enablenpc "Ched";
        enablenpc "Aahna";
        enablenpc "Constable Perry";
        enablenpc "Cyndala";
        enablenpc "Eomie";
        enablenpc "Eugene";
        enablenpc "Gladys";
        enablenpc "Inac";
        enablenpc "Ishi";
        enablenpc "Itka";
        enablenpc "Jakod";
        enablenpc "Jerican";
        enablenpc "Mahoud";
        enablenpc "Marius The Bard";
        enablenpc "Michel";
        enablenpc "Neko";
        enablenpc "Nina The Traveler";
        enablenpc "Sarah";
        enablenpc "Silvia";
        enablenpc "Swezanne";
        enablenpc "Tamiloc";
        enablenpc "Tinris";
        enablenpc "#water_animation0";
        enablenpc "Aidan";
        enablenpc "Inar";
        enablenpc "Malivox";
        enablenpc "Luca";
        enablenpc "Colin";
        enablenpc "Estard";
        enablenpc "Malindou";
        enablenpc "Jhedia";
        enablenpc "Sailors#003-1";
        enablenpc "Guard Philip";
        enablenpc "Guard Avou";
        enablenpc "Guard Benji";
        stopnpctimer;
        end;
        break;
    }

    // Loop again
    initnpctimer;
    end;

OnInit:
    // Check items.xml for info about this.
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, Bull);
    setunitdata(.@npcId, UDT_HEADMIDDLE, LieutenantArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, RaidTrousers);
    setunitdata(.@npcId, UDT_SHIELD, LousyMoccasins);    // TODO FIXME: Display Boots
    setunitdata(.@npcId, UDT_WEAPON, Backsword);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 7);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 17);

    .siegetime=0;
    .sex = G_MALE;
    .distance = 4;
    end;
}

// dausen_mobtutorial (killedrid)
// updates dausen quest
function	script	dausen_mobtutorial	{
    .@mobId=getarg(0, killedrid);
    .@t = getq(TulimsharQuest_MobTutorial);
    .@k = getq2(TulimsharQuest_MobTutorial);
    .@v = 1;
    .@upd=false;

    switch (.@mobId) {
    case Croc:
        if (.@t == 1) {
            setq2 TulimsharQuest_MobTutorial, .@k+.@v;
            .@upd=true;
        }
        break;
    case Blub:
    case Duck:
        if (.@t == 3) {
            setq2 TulimsharQuest_MobTutorial, .@k+.@v;
            .@upd=true;
        }
        break;
    case DesertBandit:
    case Sarracenus:
        .@v=2;
    case DesertLogHead:
        if (.@t == 5) {
            setq2 TulimsharQuest_MobTutorial, .@k+.@v;
            .@upd=true;
        }
        break;
    case BlackScorpion:
        .@v=10;
    case Snake:
        if (.@mobId != BlackScorpion)
            .@v=5;
    case GiantMaggot:
        if (.@t == 7) {
            setq2 TulimsharQuest_MobTutorial, .@k+.@v;
            .@upd=true;
        }
        break;
    }
    if (.@upd) {
        if (.@k+.@v % 10 == 0)
            dispbottom l("Dausen Quest - @@ @@ killed", .@k+.@v, strmobinfo(1, .@mobId));
    }
    return;
}

// Render random guard answer after bringing him water
function	script	GuardsGratitude	{

    switch (rand2(6))
    {
        case 0:
            .@message$ = l("God bless you! You have saved me from sweltering!");
            break;
        case 1:
            .@message$ = l("I am happy that such responsible citizens live in Tulimshar. Thank you for your help. It's really hot nowdays!");
            break;
        case 2:
            .@message$ = l("Thanks, this is very handy.");
            break;
        case 3:
            .@message$ = l("Our service is dangerous and difficult. But I would not want any other. Thanks for the help.");
            break;
        case 4:
            .@message$ = l("My mother told me, do not go work like a guard. You will die from overheating in the sun during the summer time.");
            break;
        case 5:
            .@message$ = l("Who are you? Thanks for the help.");
            break;
        default:
            .@message$=l("Thank you!");
            break;
    }

    if (Sex != getvariableofnpc(.sex, strnpcinfo(0))) {
        .@narrator_message$ = l("The Guard sends an air kiss to you.");
    } else {
        .@narrator_message$ = l("The Guard patted you on the back.");
    }

    speech S_LAST_BLANK_LINE, .@message$;
    narrator S_LAST_BLANK_LINE, .@narrator_message$;

    return;
}

// Do TulimsharQuest_WaterForGuard quest
function	script	CheckGuard	{
    .@guard_id = getarg(0);
    .@guard_count = 0;

    if ($@GM_OVERRIDE || debug)
        npctalk3 l("Hello, I am G-@@, of the @@ order. (G-0~4, order totals 31)", .@guard_id, $@GuardBits[.@guard_id]);

    if (GUARDS_DONE_BITARRAY & $@GuardBits[.@guard_id])
    {
        mesn;
        mesq l("Thanks for help! Other guards may need help too!");
        close;
    } else {
        for (.@i=0; .@i < getarraysize($@GuardBits); .@i++)
        {
            if (GUARDS_DONE_BITARRAY & $@GuardBits[.@i])
               .@guard_count+=1;
        }

        if ( !countitem(BottleOfTonoriWater) ) {
            legiontalk;
        } else {
            delitem BottleOfTonoriWater, 1;
            getitem EmptyBottle, 1;

            getexp 32, 2;
            Zeny = (Zeny + 30);

            GUARDS_DONE_BITARRAY = GUARDS_DONE_BITARRAY | $@GuardBits[.@guard_id];
            .@guard_count+=1; // Mark the guard as complete
            setq2 TulimsharQuest_WaterForGuard, .@guard_count; // Update questlog

            if (.@guard_count >= 5) {
                message strcharinfo(0), "That must have been the last guard.";
                setq TulimsharQuest_WaterForGuard, 2;
            }

            GuardsGratitude();
            narrator(l("You receive 32 exp and 30 GP."));
            close;
        }
    }

    return;
}

// Handle Guard's logic
function	script	GuardHandler	{
    if (getq(TulimsharQuest_WaterForGuard) == 1) {
        CheckGuard(getarg(0));
    } else {
        legiontalk;
    }

    return;
}

003-1,130,23,0	script	Guard Philip	NPC_GUARD1,{
    GuardHandler(0);
    end;

OnInit:
    .distance = 5;
    setarray $@GuardBits, 1, 2, 4, 8, 16;
    end;
}

003-1,132,23,0	script	Guard Avou	NPC_GUARD1,{
    GuardHandler(1);
    end;
OnInit:
    .sex = G_OTHER;
    .distance = 3;
    end;
}
003-1,133,23,0	script	Guard Benji	NPC_GUARD1,{
    GuardHandler(2);
    end;
OnInit:
    .sex = G_OTHER;
    .distance = 3;
    end;
}
003-1,140,23,0	script	Guard Yuna	NPC_GUARD2,{
    if (getq(TulimsharQuest_WaterForGuard) == 1)
    {
        CheckGuard(3);
    } else {
        npctalkonce l("I like to sing.");
    }
    end;
OnInit:
    .sex = G_FEMALE;
    .distance = 3;
    end;
}
003-3,39,37,0	script	Guard Malindax	NPC_GUARD1,{
    GuardHandler(4);
    end;
OnInit:
    .sex = G_OTHER;
    .distance = 3;
    end;
}


003-1,107,29,0	script	#tulim-guardhouse	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    if (countitem(TulimsharGuardCard) >= 1) goto L_Warp;
    dispbottom l("Only Tulimshar Guards are allowed in this building.");
    end;

L_Warp:
    warp "003-10", 42, 79;
    end;
}
