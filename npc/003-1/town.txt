// TMW-2 Script
// Author:
//  Jesusalva
// Description:
//  Reset LOCATION$ when entering a town

003-1,146,21,0	script	#LocTulim	NPC_HIDDEN,4,1,{
OnTouch:
    EnterTown("Tulim"); end;
}

// Crocotree spawn experiment
003-1,0,0,0	script	#CrocotreeTulim01	NPC_HIDDEN,{
    end;
// Spawn the crocotrees
OnInit:
    areamonster "003-1", 20, 20, 150, 100, strmobinfo(1, CrocoTree), CrocoTree, 1, .name$+"::OnRespawn";
    end;

// Script Generated
OnRespawn:
    initnpctimer;
    end;
OnTimer61000:
    stopnpctimer;
    areamonster "003-1", 20, 20, 150, 100, strmobinfo(1, CrocoTree), CrocoTree, 1, .name$+"::OnRespawn";
    end;
}

003-1,0,0,0	duplicate(#CrocotreeTulim01)	#CrocotreeTulim02	NPC_NO_SPRITE
003-1,0,0,0	duplicate(#CrocotreeTulim01)	#CrocotreeTulim03	NPC_NO_SPRITE
003-1,0,0,0	duplicate(#CrocotreeTulim01)	#CrocotreeTulim04	NPC_NO_SPRITE
003-1,0,0,0	duplicate(#CrocotreeTulim01)	#CrocotreeTulim05	NPC_NO_SPRITE
