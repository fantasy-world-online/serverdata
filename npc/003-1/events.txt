// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    The NPCs on this file are for when events end.

// Easter
003-1,47,36,0	script	Lilica#final	NPC_EASTER,{
    if (EASTER_YEAR != gettime(GETTIME_YEAR)) {
        EASTER_YEAR=gettime(GETTIME_YEAR);
        if (strcharinfo(0) == $@easter_name$[0]) {
            makepet BhopFluffy;
            mesn;
            mesc l("For the first place in Easter, you gained a Bhopper Fluffy."), 3;
            mesc l("Remember to give it a balanced diet of Aquadas to make it happy."), 3;
            next;
        }
        else
        {
            .@pos=array_find($@easter_name$, strcharinfo(0));
            // 0 (aka top 1) is not an appliable winner
            if (.@pos > 0) {
                // Reverse it so top 10 value is 2, and top 2 value is 10.
                .@pos=11-.@pos;
                EVENT_POINTS=.@pos*10;
                mesc l("You got @@ Event Points for leaderboard position.", .@pos*10), 2;
            }
        }
    }
    mesn;
    mesq l("Easter is over! I am the last chance to get rid of eggs!!");
    mesc l("Note: Golden and Silver Eggs are deleted after the next event end."), 1;

    menu
        l("Trade Silver Eggs"), L_Silver,
        l("Trade Golden Eggs"), L_Golden,
        l("View LeaderBoard"), L_Leader,
        l("Thanks Lilica."), -;
    close;

L_Silver:
    openshop "#eastershop1";
    closedialog;
    close;

L_Golden:
    openshop "#eastershop2";
    closedialog;
    close;

L_Leader:
    mesn l("Easter @@", gettime(GETTIME_YEAR));
	mes("1."+$@easter_name$[0]+" ("+$@easter_value[0]+")");
	mes("2."+$@easter_name$[1]+" ("+$@easter_value[1]+")");
	mes("3."+$@easter_name$[2]+" ("+$@easter_value[2]+")");
	mes("4."+$@easter_name$[3]+" ("+$@easter_value[3]+")");
	mes("5."+$@easter_name$[4]+" ("+$@easter_value[4]+")");
	mes("6."+$@easter_name$[5]+" ("+$@easter_value[5]+")");
	mes("7."+$@easter_name$[6]+" ("+$@easter_value[6]+")");
	mes("8."+$@easter_name$[7]+" ("+$@easter_value[7]+")");
	mes("9."+$@easter_name$[8]+" ("+$@easter_value[8]+")");
	mes("10."+$@easter_name$[9]+" ("+$@easter_value[9]+")");
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 5;

    disablenpc(.name$);
    end;
}























// Valentine Day
003-1,47,36,0	script	Demure#ValentineFinal	NPC_FEMALE,{
    mesn;
    mesq l("Valentine Day is over!");
    if (#VALENTINE_SENT+#VALENTINE_OPENED <= 0)
        close;
    .@nb = query_sql("SELECT c.name, i.value FROM `acc_reg_num_db` AS i, `char` AS c WHERE i.key='#VALENTINE_POINTS' AND i.account_id=c.account_id AND c.char_num='0' ORDER BY i.value DESC LIMIT 10", .@name$, .@value);

    mes "##B"+l("Top 10 - Valentine Day")+"##b";
    mes("1." +.@name$[0]+" ("+.@value[0]+")");
    mes("2." +.@name$[1]+" ("+.@value[1]+")");
    mes("3." +.@name$[2]+" ("+.@value[2]+")");
    mes("4." +.@name$[3]+" ("+.@value[3]+")");
    mes("5." +.@name$[4]+" ("+.@value[4]+")");
    mes("6." +.@name$[5]+" ("+.@value[5]+")");
    mes("7." +.@name$[6]+" ("+.@value[6]+")");
    mes("8." +.@name$[7]+" ("+.@value[7]+")");
    mes("9." +.@name$[8]+" ("+.@value[8]+")");
    mes("10."+.@name$[9]+" ("+.@value[9]+")");
    next;
    #VALENTINE_SENT=0;
    #VALENTINE_OPENED=0;
    #VALENTINE_RECEIVED=0;
    if (strcharinfo(0) == .@name$[0])
        makepet DoggyDog;
    else if (strcharinfo(0) == .@name$[1] || strcharinfo(0) == .@name$[2])
        getitem PrismGift, 1;
    else if (strcharinfo(0) == .@name$[3] || strcharinfo(0) == .@name$[4])
        getitem GoldenGift, 1;
    else if (strcharinfo(0) == .@name$[5] || strcharinfo(0) == .@name$[6])
        getitem SilverGift, 1;
    else if (strcharinfo(0) == .@name$[7] || strcharinfo(0) == .@name$[8] || strcharinfo(0) == .@name$[9])
        getitem BronzeGift, 1;
    else
        getitem ChocolateBar, 1;

    getexp #VALENTINE_SENT+#VALENTINE_RECEIVED, #VALENTINE_SENT;
    Zeny=Zeny+#VALENTINE_OPENED;

    if (strcharinfo(0) == .@name$[0])
        mesc l("You gained a @@ for the #1 place on the event. Remember to feed it @@, or it may run away from you.", getitemlink(DoggyDog), getitemlink(AnimalBones));

    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, Cap);
    setunitdata(.@npcId, UDT_HEADMIDDLE, RedStockings);
    setunitdata(.@npcId, UDT_HEADBOTTOM, BunnyEars);
    setunitdata(.@npcId, UDT_WEAPON, GMRobe);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 14);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 18);

    .sex = G_FEMALE;
    .distance = 5;

    disablenpc(.name$);
    end;
}























// St. Patrick Day
003-1,47,36,0	script	Patrick Saulc	NPC_WEIRDGREEN,{
    mesn;
    mesc l("It's St. Patrick Event!"), 3;
    mes l("At 00:00, 06:00, 12:00, 15:00, 18:00 and 21:00 server time");
    mes l("Several special clovers will show up at forests.");
    next;
    mes l("They have 10x more chance to drop a @@, so it is a great deal!", getitemlink(FourLeafClover));
    mes l("Also, hidden in a forest which is not either hot or cold, is the Gold Pot Cauldron...");
    mes l("You can get daily something from it, but unless you're green like me, you will have no luck...");
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 5;

    if ($EVENT$ != "St. Patrick")
        disablenpc(.name$);
    end;
}

























