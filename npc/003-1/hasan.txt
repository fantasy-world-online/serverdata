// TMW-2 Script
// Author:
//  Jesusalva
// Description:
//  Hasan is the city bully and steals the player. He lost his father to a Murderer
//  Scorpion. Will respect player if they kill one. His mother is Sorfina, whom
//  explains this and cries that he never touched a Soul Menhir. Allowing player
//  to kill scorpion on Mahoud's Basement.
//
//  Player must report theft first to Dausen, who will tell the player that's
//  normal and nobody messes with Hasan, and giving player pointer that he should
//  tell his mother, Sorfina.
//
//  Reward: Cotton Short, Hasan won't steal you anymore.
//
//  GETQ1 Variable Value;
//  0 - Never stolen by Hasan
//  1 - Already stolen by Hasan
//  2 - Dausen pointed player to Sorfina
//  3 - Sorfina unlocked Mahoud's Basement
//  4 - Player killed Murderer Scorpion
//  5 - Quest is complete
//
//  $HASAN_ST
//      Number of Scorpions killed during Autumn
//  $HASAN_GP
//      Total money Hasan collected for Autumn event
//  $HASAN_LT
//      Previous collected money, for rewards calc
//
//  PS. $@GM_OVERRIDE will disable Season restrictions, but values from 20/09 and 21/12
//  must be set manually.

// Temporary sprite
003-1,47,63,0	script	Hasan	NPC_ELVEN_MAN_RED,2,2,{
    if (getq(TulimsharQuest_Hasan) == 0) goto L_Safe;
    if (getq(TulimsharQuest_Hasan) == 4) goto L_Finish;
    if (getq(TulimsharQuest_Hasan) >= 5) goto L_Complete;
    mesn strcharinfo(0);
    mesq l("Give me back what you've stole, thief!");
    next;
    mesn;
    mesq l("I stole nothing from you. Do you have any proof?");
    next;
    menu
        l("I have a print screen!"), L_Print,
        l("No..."), L_Close;
    close;

L_Away:
    mesn;
    mesq l("Hey... Go bother someone else.");
    close;

L_Print:
    mes "";
    mesn;
    mesq l("Let me analyze that.");
    mesc l("Hasan takes your print screen and analyzes it.");
    next;
    mesn;
    mesq l("Photoshopped. Definitely. Good luck convincing somebody with that! %%a");
    next;
    mesn;
    mesq l("If you go to Lieutenant Dausen, he'll say that you used Gimp. Just give up.");
    close;

// TODO
L_Finish:
    mesn;
    mesq l("Is that... Wait... Wow. You... Killed a murderer scorpion.");
    next;
    mesn;
    mesq l("I promise I'll never steal from you again. Here, take this shorts.");
    mesq l("Good job, man!");
    setq TulimsharQuest_Hasan, 5;
    getitem CottonShorts, 1;
    getexp 99, 55;
    close;

L_Complete:
    mesn;
    mesq l("Thanks for avenging my father.");
    next;
    mesn;
    mesq l("I am collecting money for the Yearly Autumn Scorpion Hunter quest.");
    mesq l("You're, of course, invited. Thus far, the total prize money I've collected is @@ GP.", $HASAN_GP);
    if (season() == AUTUMN || $@GM_OVERRIDE) {
        mesq l("You've killed @@ scorpions, and a total of @@ were killed this season.", getq2(SQuest_Autumn), $HASAN_ST);
        mesq l("If the event ended now, you would get @@ GP.", getq2(SQuest_Autumn)*$HASAN_GP/$HASAN_ST);
        menuint
            l("Thanks."),0,
            rif(Zeny >= 5, l("Donate 5 GP for prize")),5,
            rif(Zeny >= 15, l("Donate 15 GP for prize")),15,
            rif(Zeny >= 50, l("Donate 50 GP for prize")),50,
            rif(Zeny >= 100, l("Donate 100 GP for prize")),100;
        if (@menuret > 0) {
            Zeny=Zeny-@menuret;
            $HASAN_GP=$HASAN_GP+@menuret;
        }
    }

    .@q=getq(SQuest_Autumn);
    if (season() == WINTER && (.@q == gettime(GETTIME_YEAR) || (.@q == gettime(GETTIME_YEAR)-1 && gettime(GETTIME_MONTH) <= MARCH))) {
        .@p=getq2(SQuest_Autumn)*$HASAN_LT/$HASAN_ST;
        mesn l("Summary");
        mes l("Scorpions killed: @@/@@ scorpions", getq2(SQuest_Autumn), $HASAN_ST);
        mes l("Total money collected: @@ GP", $HASAN_LT);
        mes l("##2Your prize: @@ GP", .@p);
        if (getq2(SQuest_Autumn) > 20)
            getexp (getq2(SQuest_Autumn)/20), 0;
        if (.@p >= 1)
            Zeny=Zeny+.@p;

        // Top 3 reward
	    .@nb = query_sql("SELECT c.name FROM `quest` AS i, `char` AS c WHERE i.quest_id="+SQuest_Autumn+" AND i.char_id=c.char_id ORDER BY i.count2 DESC LIMIT 5", .@name$);
        if (strcharinfo(0) == .@name$[0])
            getitem GoldenGift, 1;
        if (strcharinfo(0) == .@name$[1])
            getitem SilverGift, 1;
        if (strcharinfo(0) == .@name$[2])
            getitem BronzeGift, 1;

        // Quest is closed
        setq1 SQuest_Autumn, 1970;
    }
    close;

L_TouchComplete:
    if (@hasan_antiflood < gettimetick(2)) {
        npctalkonce l("Hey there, @@! Good luck killing monsters!", strcharinfo(0)), 3;
        @hasan_antiflood=gettimetick(2)+600;
    }
    end;

L_Close:
    close;

OnTouch:
    if (getq(TulimsharQuest_Hasan) >= 5) goto L_TouchComplete;
    if (Zeny > 15) goto L_Steal;
    end;

L_Steal:
    npctalkonce(l("*whistle*"));
    if (getq(TulimsharQuest_Hasan) == 0) goto L_DoSteal;
    if (getq(TulimsharQuest_Hasan) <= 4 && rand(0,100) < 20) goto L_DoSteal;
    end;

L_DoSteal:
    .@s=rand(3,12);
    $HASAN_GP=$HASAN_GP+.@s;
    Zeny=Zeny-.@s;
    if (getq(TulimsharQuest_Hasan) == 0)
        setq TulimsharQuest_Hasan, 1;
    dispbottom l("##BYou were robbed##b by an evil NPC.");
    end;

OnInit:
    .sex = G_MALE;
    .distance = 5;

    //npcspeed(150);
    //npcwalkto(<x>, <y>);
    end;

// Restart hasan status on 20/09 (double-sure)
OnDay0920:
    $HASAN_ST=0;
    end;

// Once autumn is over (21/12) restart GP poll and send value to backup
OnDay1221:
    $HASAN_LT=0+$HASAN_GP;
    $HASAN_GP=2000;
    end;
}

function	script	SQuest_Hasan	{
    if (season() != AUTUMN && !$@GM_OVERRIDE)
        return;
    if (getq(SQuest_Autumn) != gettime(GETTIME_YEAR))
        setq SQuest_Autumn, gettime(GETTIME_YEAR), 0;

    // All scorpions are counting for Hasan Autumn Quest
    if (
        killedrid == Scorpion ||
        killedrid == RedScorpion ||
        killedrid == BlackScorpion ||
        killedrid == CandorScorpion ||
        killedrid == AngryScorpion ||
        killedrid == AngryRedScorpion ||
        killedrid == NightScorpion ||
        killedrid == GoldenScorpion ||
        killedrid == MurdererScorpion) {
            setq2 SQuest_Autumn, getq2(SQuest_Autumn)+1;
            $HASAN_ST=$HASAN_ST+1;
    }
    return;
}

