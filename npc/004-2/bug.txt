// TMW2 Script.
// Author:
//    Jesusalva
// Description:
//    Error Handler

004-2,54,63,0	script	Sign#ErrorHandler0041	NPC_SWORDS_SIGN,{
    mesc l("WARNING: Desert Canyon locked down due major bugs"), 1;
    mesc l("Please use the alternative route provided by the Alliance High Council"), 1;
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 3;
    end;
}


004-2,84,77,0	script	Sign#ErrorHandler0042	NPC_SWORDS_SIGN,{
    mesc l("WARNING: This path has been locked down due to major bugs"), 1;
    mesc l("You may be trapped forever inside the desert maze beyond help"), 1;
    mesc l("shall you decide to disregard this warning."), 1;
    mes "";
    mesc l("Do not attempt to use the normal route with a @@ with you.", getitemlink(TimeFlask)), 1;
    next;
    mesc l("Use the normal route?");
    if (askyesno() == ASK_YES)
        warp "004-2-1", 26, 87;
    closeclientdialog;
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 2;
    end;
}
