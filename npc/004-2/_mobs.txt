// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 004-2: Desert Mountains mobs
004-2,36,40,15,21	monster	Maggot	1030,8,35000,150000
004-2,73,77,13,4	monster	Maggot	1030,7,35000,150000
004-2,100,55,14,14	monster	Maggot	1030,7,35000,150000
004-2,108,39,17,4	monster	Scorpion	1071,3,35000,150000
004-2,51,40,10,22	monster	Scorpion	1071,6,35000,150000
004-2,102,55,4,4	monster	Black Scorpion	1074,1,35000,150000
004-2,108,97,4,4	monster	Snake	1122,3,35000,150000
004-2,77,51,9,4	monster	Snake	1122,1,35000,150000
004-2,67,63,14,11	monster	Scorpion	1071,8,35000,150000
004-2,82,50,27,29	monster	Desert Log Head	1127,20,20000,20000
