// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Essential scripts any city must have

// Description:
//    Banker.
020-2,30,25,0	script	Ben#NivBanker	NPC_LLOYD,{
    Banker(.name$, "Nivalis", 10000);
    close;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}

// Description:
//    Barber.
020-1,88,76,0	script	Camilot	NPC_ELVEN_FEMALE_ARMOR_SHOP,{
    function setRace {
        clear;
        setnpcdialogtitle l("Debug - Modify Race");
        mes l("Race") + ": " + $@allraces$[Class];
        next;
        mes l("Please select the desired race.");
        select("Human:Ukar:Redy:Elf:Orc:Raijin:Tritan");
        jobchange max(0, @menu-1);
        return;
    }


    mesn;
    mesq l("Hi! Do you want a hair cut?");

    do
    {
        select
            l("What is my current hairstyle and hair color?"),
            l("I'd like to get a different style."),
            l("Can you do something with my color?"),
            rif(is_staff(), l("I am a GM, and I want to change my Race!")),
            l("I'm fine for now, thank you.");

        switch (@menu)
        {
            case 1:
                BarberSayStyle 3;
                break;
            case 2:
                BarberChangeStyle;
                speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("Enjoy your new style.");
                    l("Anything else?");
                break;
            case 3:
                BarberChangeColor;
                speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("I hope you like this color.");
                    l("Anything else?");
                break;
            case 4:
                setRace;
                break;
            case 5:
                speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("Feel free to come visit me another time.");

                goodbye;
        }
    } while (1);
    close;


OnInit:
    .sex = G_FEMALE;
    .distance = 5;
    end;
}

// Description:
//    Soul Menhir
020-1,57,63,0	script	Soul Menhir#niv	NPC_SOUL_SNOW,{
    @map$ = "020-1";
    setarray @Xs, 56, 57, 58, 56, 58, 56, 57, 58;
    setarray @Ys, 62, 62, 62, 63, 63, 64, 64, 64;
    @x = 0;
    @y = 0;
    callfunc "SoulMenhir";
    @map$ = "";
    cleararray @Xs[0], 0, getarraysize(@Xs);
    cleararray @Ys[0], 0, getarraysize(@Ys);
    @x = 0;
    @y = 0;
    close;
}
