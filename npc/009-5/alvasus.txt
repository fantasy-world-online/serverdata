// TMW2 Script
// Author:
//      Pookie
//      Saulc
//      Jesusalva
// Description:
//    Alvasus is a believer, he is from Halinarzo and he collects Roasted Maggot
//    for Church Party.
//    Quest for level 19 - 5 roasted maggot for Alvasus Pendant
// Notes:
//    There is no church on Tulimshar. If he wanted to spread faith or whatever
//    he would fit, but as he wants a party, I had to move him to Halinarzo.
//    I turned him on a weekly quest. Sorry, I got excited u.u
//
//    First time: Alvasus Pendant
//    Later times: GP and EXP
//
//    Q1: Current State
//    Q2: Current Reward
//    Q3: Current Week Number (for repeat)

009-5,29,39,0	script	Alvasus	NPC_PLAYER,{
    .@q3 = getq3(HalinarzoQuest_Alvasus);
    .@q2 = getq2(HalinarzoQuest_Alvasus);
    if (.@q3 != atoi(gettimestr("%U", 2)))
        setq HalinarzoQuest_Alvasus, 0, .@q2, atoi(gettimestr("%U", 2));

	if (gettime(GETTIME_WEEKDAY) == SUNDAY) goto L_Sunday;
    .@q = getq(HalinarzoQuest_Alvasus);
    if (BaseLevel < 19) goto L_TooWeak;
    if (.@q == 0) goto L_GiveTask;
    if (.@q == 1) goto L_Check;
    if (.@q == 2) goto L_Complete;

    closedialog;
    goodbye;
    close;

L_TooWeak:
    mesn;
    mesq l("Go away, kid! Let me pray in peace!");
    close;

L_Sunday:
    mesn;
	mesq l("Ah, Sunday. What better time to go to church?");
    next;
    if (!@alvasus && .@q == 2) goto L_Party;
    mesn;
	mesq l("We will have a party today, but only believers are allowed in!");
    close;

L_Quit:
    mes "";
    mesn;
    mesq l("Alright.");
    close;

L_Party:
    mesn;
    mesq l("Please pray a bit with us!");
    if (askyesno() != ASK_YES)
        close;
    mes "";
    mesc "...";
    next;
    mesc "...";
    next;
    mesc "...";
    next;
    mesc "...";
    next;
    mesc "...";
    next;
    mesn;
    mesq l("Alright, PARTY TIME!");
    @alvasus=any(1,1,1,1,2,3,4,5);
	@min = 65*@alvasus;
	@max = 175*@alvasus;
	@delay = 3*@alvasus;
	@type = 1;
	doevent "rand_sc_heal::OnUse";
    close;


L_GiveTask:
    mesn;
    mesq lg("Hello Adventurer!");
    next;
    mesn;
    mesq l("Would you like to help me with organizing a Church Party?");
    next;

    menu
        l("Sure, why not?"), L_Quest,
        l("I don't have time for that."), L_Quit;


L_Quest:
    mes "";
    mesn;
    mesq l("Sunday I am having a Church Party but I forgot to prepare food!");
    next;
    mesn;
    mesq l("Can you help me find some @@?", getitemlink(RoastedMaggot));
    next;

    menu
        l("Okay!"), L_Start,
        l("I don't have the time!"), L_Quit;


L_Start:
    setq1 HalinarzoQuest_Alvasus, 1;
    mes "";
    mesn;
    mesq l("Ok, let's see how many you have and how many I need:");
    goto L_List;

L_List:
    mes "";
    mesn;
    mes l("I will need:");
    mes l("@@/5 @@", countitem(RoastedMaggot), getitemlink(RoastedMaggot));
    next;
    goto L_Check;

L_Check:
    mesn;
    mesq l("Do you have my @@?", getitemlink(RoastedMaggot));
    next;
    menu
        l("Yes!"), L_Give,
        l("How many did you need again?"), L_List,
        l("No!"), L_Quit;

L_Give:
    if (
        countitem(RoastedMaggot) < 5
        ) goto L_NoItem;


    // First time you get the Pendant
    .@q2 = getq2(HalinarzoQuest_Alvasus);
    if (.@q2 == 0) {
        inventoryplace AlvasusPendant, 1;
        getitem(AlvasusPendant, 1);
    }

    delitem(RoastedMaggot, 5);

    getexp(700+.@q2, 1+.@q2);
    Zeny=Zeny+(.@q2*5);

    setq1(HalinarzoQuest_Alvasus, 2);
    setq2 HalinarzoQuest_Alvasus, .@q2+1;
    setq3 HalinarzoQuest_Alvasus, atoi(gettimestr("%U", 2));

    mes "";
    mesn;
    mesq l("Thanks for all of your help!");
    close;

L_Complete:
    mesn;
    mesq l("Now, to wait for Sunday is the most boring part... Alas, I wonder if I'll remember next week, too.");
    //mesq l("Do you know where the party is? It's at Halin, to get there you need to pass through the Desert Canyon.");
    close;

L_NoItem:
    mesn;
    mesq l("Thanks for the kindness, but you don't have enough. I need 5!");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, DesertHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SilkRobe);
    setunitdata(.@npcId, UDT_WEAPON, CreasedBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 6);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 7);

    .sex = G_MALE;
    .distance = 4;

    end;
}

