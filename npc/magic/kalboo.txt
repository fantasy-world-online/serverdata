// TMW2 script
// Author: Jesusalva <admin@tmw2.org>
//
// Magic Script: TMW2_KALBOO
//
// Summons mouboo

-	script	sk#kalboo	32767,{
    end;

OnCall:
    // Other requeriments: 1x MoubooFigurine
    if (countitem(MoubooFigurine) < 1) {
        dispbottom l("You need 1x @@ to cast this skill.", getitemlink(MoubooFigurine));
        end;
    }

    // Check cooldown
    if (@kalboo_at > gettimetick(2)) {
        dispbottom l("Skill is in cooldown for @@.", FuzzyTime(@kalboo_at));
        end;
    }

    // Setup
    @sk=TMW2_KALBOO;
    @mp=250;
    @amp=55;

    // Check if you have mana to cast
    // MagicCheck(SkillID, Mana{, MP per level})
    if (!MagicCheck(@sk, @mp, @amp))
        end;

    // Destroy reagents
    delitem MoubooFigurine, 1;

    // set cooldown
    @kalboo_at=gettimetick(2);
    @kalboo_at=@kalboo_at+44;

    // As usual, magic profeciency affects success ratio
    if (rand(1,6) < abizit()+1) {
        .@mobId=Mouboo;
        // Summon Magic
        // SummonMagic(SkillID, MobID{, SkillLevelPerMob=2{, Level Override}})
        SummonMagic(@sk, .@mobId, 4, MAGIC_LVL+getskilllv(@sk)-1);
    } else {
        dispbottom l("The spell fails!");
    }

    // Get 4~5 mana experience point (this is NOT used by Mana Stone)
    GetManaExp(@sk, rand(4,5));

    end;

OnInit:
    bindatcmd "sk-kalboo", "sk#kalboo::OnCall", 0, 100, 0;
    end;
}
