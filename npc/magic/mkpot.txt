// TMW2 script
// Author: Jesusalva <admin@tmw2.org>
//
// Magic Script: TMW2_MKPOT
//
// Attempts to make a haste potion from Mouboo Milk and Plushroom.
// May make sewer water on failure.
// TODO: Using too much transmutation magic may have dire consequences! Like, uh, transmutating your head!
// Nah, it is probably just propaganda... I hope. ¬.¬

-	script	sk#mkpot	32767,{
    end;

/*
OnFriendlyDeath:
    emote 4;
    end;
*/

OnCall:
    // Other requeriments
    if (countitem(Plushroom) < 10 || !countitem(Milk)) {
        dispbottom l("You need @@ @@ and @@ to cast this skill.", 10, getitemlink(Plushroom), getitemlink(Milk));
        end;
    }

    // Check cooldown
    if (@mkpot_at > gettimetick(2)) {
        dispbottom l("Skill is in cooldown for @@.", FuzzyTime(@mkpot_at));
        end;
    }

    // Check requisites
    if (!MagicCheck(TMW2_MKPOT, 185, -5))
        end;

    // Consume items
    delitem Plushroom, 10;
    delitem Milk, 1;

    // Create the stuff based on MAGIC_EXP
    // The closer to zero, best;
    // Each 3 mexp reduces chance to get a fail
    // Each skill level reduces chances to get a fail
    .@r=rand(141,181)-(MAGIC_EXP/3)-getskilllv(TMW2_MKPOT);
    if (.@r < 21) {
            // Get up to 6 Haste Potions depending on your skill.
            getitem HastePotion, abizit()+1;
    } else if (.@r < 82) {
        getitem any(BottleOfTonoriWater, BottleOfWoodlandWater, BottleOfDivineWater, Milk, CelestiaTea, PileOfAsh), 1;
    } else if (.@r < 122) {
        getitem any(BottleOfSewerWater, Milk, CelestiaTea, PileOfAsh), 1;
    } else if (.@r < 162) {
        getitem any(BottleOfSewerWater, BottleOfSewerWater, PileOfAsh), 1;
    } else {
        dispbottom l("Your items vanishes into thin air. What?!");
    }

    // set cooldown
    @mkpot_at=gettimetick(2);
    @mkpot_at=@mkpot_at+6;

    // Get a few mana experience points (this is NOT used by Mana Stone)
    GetManaExp(TMW2_MKPOT, rand(4,11));
    end;

OnInit:
    bindatcmd "sk-mkpot", "sk#mkpot::OnCall", 0, 100, 0;
    end;
}
