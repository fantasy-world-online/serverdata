// TMW2 script
// Author: Jesusalva <admin@tmw2.org>
//
// Magic Script: TMW2_PLANTKINGDOM
//
// Summons plants

-	script	sk#plantkingdom	32767,{
    end;

OnCall:
    // Other requeriments: 2x Root
    if (countitem(Root) < 2) {
        dispbottom l("You need 2x @@ to cast this skill.", getitemlink(Root));
        end;
    }

    // Check cooldown
    if (@plantkingdom_at > gettimetick(2)) {
        dispbottom l("Skill is in cooldown for @@.", FuzzyTime(@plantkingdom_at));
        end;
    }

    // Setup
    @sk=TMW2_PLANTKINGDOM;
    @mp=300;
    @amp=30;

    // Check if you have mana to cast
    // MagicCheck(SkillID, Mana{, MP per level})
    if (!MagicCheck(@sk, @mp, @amp))
        end;

    // Destroy reagents
    delitem Root, 2;

    // set cooldown
    @plantkingdom_at=gettimetick(2);
    @plantkingdom_at=@plantkingdom_at+54;

    // As usual, magic profeciency affects
    if (rand(1,6) < abizit()+1) {
        // Summon Magic
        // SummonMagic(SkillID, MobID{, SkillLevelPerMob=2{, Level Override}})
        SummonMagic(@sk, any(ShadowPlant, AlizarinPlant, CobaltPlant, MauvePlant, GambogePlant, PlushroomField, ChagashroomField, MananaTree, CrocoTree, AlizarinPlant, CobaltPlant, MauvePlant, GambogePlant), 2, MAGIC_LVL+getskilllv(@sk)-1);
    } else {
        dispbottom l("The spell fails!");
    }

    // Get 3~4 mana experience point (this is NOT used by Mana Stone)
    GetManaExp(@sk, rand(3,4));

    end;

OnInit:
    bindatcmd "sk-plantkingdom", "sk#plantkingdom::OnCall", 0, 100, 0;
    end;
}
