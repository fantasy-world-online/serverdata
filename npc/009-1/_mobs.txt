// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 009-1: Halinarzo mobs
009-1,76,35,49,10	monster	Giant Maggot	1031,6,30000,90000
009-1,27,29,7,8	monster	Piou	1002,1,60000,60000
009-1,73,51,49,14	monster	Fire Goblin	1067,7,45000,45000,Fisherman::OnKillFireGoblin
009-1,70,31,49,21	monster	Maggot	1030,25,30000,60000
009-1,70,86,47,6	monster	Scorpion	1071,3,30000,20000
009-1,70,102,47,6	monster	Red Scorpion	1072,1,30000,20000
009-1,27,98,7,8	monster	Duck	1029,1,60000,60000
009-1,113,88,0,0	monster	Chagashroom Field 	1128,1,60000,60000
009-1,76,105,1,0	monster	Chagashroom Field 	1128,1,70000,60000
