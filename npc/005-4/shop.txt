// TMW2 scripts.
// Author:
//    Saulc
// Description:
//    Rosen sells basic armor on Candor. Logic by Reid.

005-4,30,37,0	trader	Shop#Candor	NPC_NO_SPRITE,{

OnInit:
    tradertype(NST_MARKET);

    sellitem RoundLeatherShield, -1, 2;
    sellitem Knife, -1, 5;
    sellitem TolchiArrow, -1, 30000;
    sellitem TolchiAmmoBox, -1, 4;
    sellitem LousyMoccasins, -1, 4;
    sellitem WarlordBoots, 15000, 3;

    .sex = G_OTHER;
    .distance = 3;
    end;

OnClock0602:
OnClock1206:
OnClock1811:
OnClock0004:
    restoreshopitem RoundLeatherShield, -1, 2;
    restoreshopitem Knife, -1, 5;
    restoreshopitem TolchiArrow, -1, 30000;
    restoreshopitem TolchiAmmoBox, -1, 4;
    restoreshopitem LousyMoccasins, -1, 4;
    restoreshopitem TrainingBow, -1, 3;
}

