// TMW2 scripts.
// Author:
//    Saulc
//    Jesusalva
// Variables:
//    CandorQuest_Tolchi
// Description:
//   Tolchi crafts weapons at Rosen & Tolchi shop
// Values:
//    0    BlackSmith quest ask for 1 iron oore
//    1    BlackSmith quest ask for 3 coal
//    2    BlackSmith quest ask for 1 iron ingot
//    3    Quest is Complete


005-4,42,37,0	script	Tolchi	NPC_RAIJIN_FEMALE_LEGION_ARTIS,{

    function quest_close {
        if (@q < 3) {
            mesn;
            mesq l("Maybe next time, then.");
            next;
        }
        close2;
        goodbye;
    }

    function quest_giveitem {
    @q = getq(CandorQuest_Tolchi);
        if (@q == 2)
        {
            if (countitem("Iron Ingot") == 0)
            {
                speech S_FIRST_BLANK_LINE,
                    l("You don't have the Iron Ingot.");
                close2;
                goodbye;
            }
            delitem .Item3, 1;
            Zeny = Zeny + 8000; // Real worth: 2820 GP + 2400 (ship fee) = 5220 gp poll (the plus is net profit)
            getexp 3575,0;
            setq CandorQuest_Tolchi, 3;
            speech S_FIRST_BLANK_LINE,
            l("Thanks mate, that is everything I need! Have a good day!");
        }
        if (@q == 1)
        {
            if (countitem("Coal") <= 2)
            {
                speech S_FIRST_BLANK_LINE,
                    l("You don't have the three Coal lumps.");
                close2;
                goodbye;
            }
            delitem .Item2, 3;
            Zeny = Zeny + 825;
            getexp 305,0;
            setq CandorQuest_Tolchi, 2;
        }
        if (@q == 0)
        {
            if (countitem("Iron Ore") == 0)
            {
                speech S_FIRST_BLANK_LINE,
                    l("You don't have the Iron Ore.");
                close2;
                goodbye;
            }
            delitem .Item1, 1;
            Zeny = Zeny + 225;
            getexp 150,0;
            setq CandorQuest_Tolchi, 1;
        }
        close;
    }

    function quest_first {
        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("Well, it is terrible! How can I make armours if I ran out of Iron? Shall the citizens of Candor Island perish in a monster attack?!"), // NOTE: I really prefer EN_US (eg. armor vs armour)
            l("They shouldn't. Could you perhaps, kindly bring me 1 @@?", getitemlink(.Item1));
        do
        {
            //l("Do not worry, I'll seek and bring it to you."),
            select
                l("Do not worry, I have them right here."),
                l("I am not a citizen of Candor.");

            switch (@menu)
            {
                case 1:
                    quest_giveitem;
                    break;
                case 2:
                    quest_close;
                    break;
            }
        } while (@menu != 2);
    }

    function quest_second {
        speech  S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("Thanks for helping my shop earlier. I forge weapons, but unfortunately, I need more than just iron to forge them."),
            l("Can you bring me 3 @@? Of course, you'll be rewarded.", getitemlink(.Item2));
        do
        {
            // l("Hey, I like rewards. Wait me, I'll be back!"),
            select
                l("You better have it ready, because I have the Coal with me!"),
                l("Eh, that seems too problematic. Sorry.");

            switch (@menu)
            {
                case 1:
                    quest_giveitem;
                    break;
                case 2:
                    quest_close;
                    break;
            }
        } while (@menu != 2);
    }

    function quest_third {
        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("Ok, this will be my last request. The Trainer asked me for a fine weapon, to protect our village."),
            l("Problem is, I do not have the knowledge to make it without @@. If you bring me one, I'll reward you with one quarter of my commission.", getitemlink(.Item3));
        do
        {
            // l("Do not worry, I'll be back in a jiffy."),
            select
                l("No problem is too big for me. I have them right here!"),
                l("Sorry, I am busy...");

            switch (@menu)
            {
                case 1:
                    quest_giveitem;
                    break;
                case 2:
                    quest_close;
                    break;
            }
        } while (@menu != 2);
    }

    function tolchi_arrows {
        // Price: 120~200, with 25 GP discount per task done (max. 3 tasks atm)
        .@price=max(120, 200-(@q*25));
        mesn;
        mesq l("Well, I can sell you a random box with almost 100 @@ for just @@ GP.", getitemlink(TolchiArrow), .@price);
        next;
        if (Zeny < .@price || askyesno() == ASK_NO) {
            close2;
            goodbye;
        } else {
            mes "";
            inventoryplace TolchiArrow, 110;
            Zeny=Zeny-.@price;
            getitem TolchiArrow, rand(96,102);
            mesn;
            mesq l("Here you go. Uhm, I really prefer if you buy with Rosen, though."); // I just don't want to add loops or inputs
            close;
        }
        close;
    }

    /////////////////////////////////////////////////////////////////////////////
    @q = getq(CandorQuest_Tolchi);

    if (@q == 3) {
        mesn;
        mesq l("Hey! How are you today? Thanks again for your help.");
    } else if (BaseLevel < 5 || BaseLevel < 10 && @q == 1 || BaseLevel < 15 && @q == 2) {
        mesn;
        mesq l("I need help, but you aren't strong enough. Please come back later.");
        next;
    } else {
        mesn;
        mesq l("Hello! You seem strong enough, could take a request from me? Of course, not for free.");
        next;
    }

    do
    {
        select
            l("I'm interested in your arrows, they're too expensive with Rosen."),
            rif(@q == 0 && BaseLevel >= 5, l("Yes. What do you need help with?")),
            rif(@q == 1 && BaseLevel >= 10, l("Yes. What do you need help with?")),
            rif(@q == 2 && BaseLevel >= 15, l("Yes. What do you need help with?")),
            l("I have other things to do at the moment.");
        mes "";
        switch (@menu)
        {
            case 1:
                tolchi_arrows;
                break;
            case 2:
                quest_first;
                break;
            case 3:
                quest_second;
                break;
            case 4:
                quest_third;
                break;
            default:
                quest_close;
                goodbye;
                break;
        }
    } while (@menu != 4);

    closedialog;
    goodbye;
    close;

OnTimer1000:
    domovestep;

OnInit:
    initpath "move", 44, 35,
             "dir", UP, 0,
             "wait", 30, 0,
             "move", 43, 40,
             "dir", UP, 0,
             "wait", 30, 0,
             "move", 40, 35,
             "dir", UP, 0,
             "wait", 30, 0,
             "dir", LEFT, 0,
             "wait", 30, 0,
             "move", 42, 37,
             "dir", DOWN, 0,
             "dir", UP, 0,
             "wait", 30, 0,
             "move", 45, 37,
             "dir", DOWN, 0,
             "wait", 30, 0,
             "move", 33, 37,
             "dir", LEFT, 0,
             "wait", 30, 0,
             "move", 33, 37,
             "dir", RIGHT, 0,
             "wait", 30, 0;

    initialmove;
    initnpctimer;
    .distance = 5;

    // I don't really like this system but oh well
    .Item1 = IronOre;
    .Item2 = Coal;
    .Item3 = IronIngot;
}
