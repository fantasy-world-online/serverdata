// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Controls the four first levels from Heroes Hold

018-2-2,0,0,0	script	#HH_CONTROLLER01	NPC_HIDDEN,{
    end;

// Boss-Slaying related
    function DungeonClear {
        getmapxy(.@m$, .@x, .@y, 0);
        areatimer(.@m$, .@x-15, .@y-15, .@x+15, .@y+15, 100, "#HH_CONTROLLER01::OnFinish");
        return;
    }

OnFinish:
    if (ispcdead())
        end;
    .@g=getq2(LoFQuest_HH);
    setq2 LoFQuest_HH, .@g|@HH_LEVEL;
    dispbottom l("Dungeon cleared!");
    getitem HeroCoin, @HH_LEVEL*3;
    getexp 4800-@HH_TIMER, @HH_LEVEL*20;
    deltimer("#HH_CONTROLLER01::OnPlayerCycle");
    setpcblock(PCBLOCK_ATTACK|PCBLOCK_SKILL, true);
    addtimer(4000, "#HH_CONTROLLER01::OnCompulsoryWarp");
    recovery(getcharid(3));
    end;

OnCompulsoryWarp:
    setpcblock(PCBLOCK_ATTACK|PCBLOCK_SKILL, false);
    warp "018-2-1", 0, 0;
    end;

OnNoviceBossKilled:
    DungeonClear();
    monster "018-2-2@No", 188, 29, "Novice Dungeon Boss", AlphaMouboo, 1, "#HH_CONTROLLER01::OnNoviceBossKilled";
    end;

OnIntermBossKilled:
    DungeonClear();
    monster "018-2-3@In", 52, 196, "Intermiary Dungeon Boss", FafiDragon, 1, "#HH_CONTROLLER01::OnIntermBossKilled";
    end;

OnAdvancedBossKilled:
    DungeonClear();
    monster "018-2-2@Ad", 52, 29, "Advanced Dungeon Boss", GiantMutatedBat, 1, "#HH_CONTROLLER01::OnAdvancedBossKilled";
    end;

OnExpertBossKilled:
    DungeonClear();
    monster "018-2-3@Ex", 188, 196, "Expert Dungeon Boss", FallenKing1, 1, "#HH_CONTROLLER01::OnExpertBossKilled";
    end;

OnMasterBossKilled:
    if ($HEROESHOLD_WINNER$ == "") {
        $HEROESHOLD_WINNER$=strcharinfo(0);
        channelmes("#world", $HEROESHOLD_WINNER$+" is the first player to finish HEROES HOLD Master Dungeon!! GG, dude! %%N");
        announce "All hail ##B"+$HEROESHOLD_WINNER$+"##b, first to complete the ##3HEROES HOLD Master Dungeon!", bc_all|bc_npc;
        getexp 0, 2000;
        getitem PrismGift, 1;
        mesc l("CONGRATULATIONS! You are the first player to finish Heroes Hold Master Dungeon!!"), 2;
        mesc l("You just gained a Prism Gift, and 2000 Job Exp for your bravery!"), 2;
    }
    DungeonClear();
    monster "018-2-5@Ma", any(52,188), any(29,196), "Master Dungeon Boss", MonsterKing, 1, "#HH_CONTROLLER01::OnMasterBossKilled";
    end;


// Everytime loop
OnPlayerCycle:
    @HH_TIMER+=1;
    // 25 minutes have passed and your time is over
    if (@HH_TIMER >= 3000) {
        warp "018-2-1", 0, 0;
        dispbottom l("You were rescued by DUSTMAN.");
        end;
    }
    // TODO: Handle traps (We'll use isin() command because you can give 3~4 steps each counter)
    if (rand(0,1000) <= @HH_LEVEL) {
        dispbottom l("You set off a trap!");
        heal -(@HH_LEVEL*rand(2,5)), 0;
    }

    // Continue the execution
    if (getmap() ~= "018-2-*")
        addtimer(500, "#HH_CONTROLLER01::OnPlayerCycle");
    end;

// Initialize Variables. Remember this causes a search for On<Difficulty><SeqNumber>. And keep same number or CRASH
OnInit:
    // Novice
    setarray .Novice_Mobs, AngryScorpion,CaveMaggot,MagicGoblin,ViciousSquirrel,AngryBat,RedSlime,AngryRedScorpion,Bandit,Skeleton,GreenSlime,
                           BlueSlime,LavaSlime,RedMushroom,RobinBandit,AngryYellowSlime,OldSnake,GrassSnake,BlackSlime,SmallMagicBif,BronzeChest,
                           Bif;
    setarray .Novice_Ammo,            35,        35,         40,             30,      40,      80,              20,    20,      10,        90,
                                  30,       30,        25,          10,              35,      10,        15,        45,            7,          2,
                             2;

    // Intermiary
    setarray .Interm_Mobs, RedSlime,AngryRedScorpion,Bandit,Skeleton,GreenSlime, BlueSlime,LavaSlime,RedMushroom,RobinBandit,Bif,
                           AngryYellowSlime,OldSnake,GrassSnake,BlackSlime,Wolvern,DarkLizard,BlackScorpion,DustRevolver,MagicBif,SilverChest;
    setarray .Interm_Ammo,       80,              50,    50,      10,        90,        30,       60,        35,          20,  5,
                                          50,      20,        25,        50,    30,        20,           25,           5,      7,          2;

    // Advanced
    setarray .Advanc_Mobs, AngryYellowSlime,Snake,GrassSnake,BlackSlime,Wolvern,DarkLizard,BlackScorpion,DustRevolver,MagicBif,SilverChest,
                           MountainSnake, Yeti, WickedMushroom,Forain,GoldenChest,BigMagicBif,DustGatling,Archant,Bif;
    setarray .Advanc_Ammo,               50,   30,        35,        90,     30,        30,           45,          15,      7,           2,
                                      15,   10,             30,     5,          2,          1,         10,     18,  9;

    // Expert
    setarray .Expert_Mobs, MountainSnake, Yeti, WickedMushroom,Forain,GoldenChest,BigMagicBif,DustGatling,BlackMamba,Terranite, Wolvern,
                           PrismChest, BlueSlime, GreenDragon,Bif,GoboBear;
    setarray .Expert_Ammo,            45,   30,             90,    35,          3,          7,         30,        20,       15,      10,
                                    1,       140,          15, 13,      15;

    // Master
    setarray .Master_Mobs, PrismChest,BlueSlime,MurdererScorpion,Tipiou,AlphaMouboo,BanditLord,Tipiu,GreenDragon,GiantMutatedBat,FallenKing1,
                           FallenKing2, EvilScythe, YetiKing, Tipiu, Yetifly;
    setarray .Master_Ammo,            2,      140,              20,    20,         20,        20,   20,         20,             20,         10,
                                    10,         1,         7,     8,       3;

    end;

OnHHInit:
    initnpctimer;
    end;


// Controls logic for each instance (TODO)
// Initialize each instance (Currently waits 3 seconds, could work with just 4, but better safe than sorry)
OnTimer3000:
    stopnpctimer;
    debugmes "Heroes Hold Monsters: Initializing";

    // HH_Novice
    freeloop(true);
    for (.@i=0;.@i<getarraysize(.Novice_Mobs);.@i++) {
        areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[.@i]), .Novice_Mobs[.@i], .Novice_Ammo[.@i], "#HH_CONTROLLER01::OnNovice"+.@i;
    }
    freeloop(false);

    // HH_INTERMEDIARY
    freeloop(true);
    for (.@i=0;.@i<getarraysize(.Interm_Mobs);.@i++) {
        areamonster "018-2-3@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[.@i]), .Interm_Mobs[.@i], .Interm_Ammo[.@i], "#HH_CONTROLLER01::OnInterm"+.@i;
    }
    freeloop(false);

    // HH_ADVANCED
    freeloop(true);
    for (.@i=0;.@i<getarraysize(.Advanc_Mobs);.@i++) {
        areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[.@i]), .Advanc_Mobs[.@i], .Advanc_Ammo[.@i], "#HH_CONTROLLER01::OnAdvanc"+.@i;
    }
    freeloop(false);

    // HH_EXPERT
    freeloop(true);
    for (.@i=0;.@i<getarraysize(.Expert_Mobs);.@i++) {
        areamonster "018-2-3@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[.@i]), .Expert_Mobs[.@i], .Expert_Ammo[.@i], "#HH_CONTROLLER01::OnExpert"+.@i;
    }
    freeloop(false);

    // HH_MASTER
    freeloop(true);
    for (.@i=0;.@i<getarraysize(.Master_Mobs);.@i++) {
        areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[.@i]), .Master_Mobs[.@i], .Master_Ammo[.@i], "#HH_CONTROLLER01::OnMaster"+.@i;
    }
    freeloop(false);

    // Boss for each dungeon
    monster "018-2-2@No", 188, 29, "Novice Dungeon Boss", AlphaMouboo, 1, "#HH_CONTROLLER01::OnNoviceBossKilled";
    monster "018-2-3@In", 52, 196, "Intermiary Dungeon Boss", FafiDragon, 1, "#HH_CONTROLLER01::OnIntermBossKilled";
    monster "018-2-2@Ad", 52, 29, "Advanced Dungeon Boss", GiantMutatedBat, 1, "#HH_CONTROLLER01::OnAdvancedBossKilled";
    monster "018-2-3@Ex", 188, 196, "Expert Dungeon Boss", FallenKing1, 1, "#HH_CONTROLLER01::OnExpertBossKilled";
    monster "018-2-5@Ma", any(52,188), any(29,196), "Master Dungeon Boss", MonsterKing, 1, "#HH_CONTROLLER01::OnMasterBossKilled";

    // TODO: We still need the main logic for this. I mean, what is the objective on each floor of Master Dungeon?
    debugmes "Heroes Hold Monsters: Success";
    end;


/////////////////////////////////////////////////////////////////////////////////
// Respawn Arrays (Autogenerated, Python Script at bottom. 14 pages worth.)
// Drop Chances are: 0.05% each luck point, 0.20% each mob level, 0.05% each job experience given
// So killing a Mouboo with 20 luck will give: 1% (luck) + 7% (level) + 0.3% (jexp) => 8.3% drop chance

OnNovice0:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[0])*4)+strmobinfo(7,.Novice_Mobs[0])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[0]), .Novice_Mobs[0], 1, "#HH_CONTROLLER01::OnNovice0"; end;
OnNovice1:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[1])*4)+strmobinfo(7,.Novice_Mobs[1])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[1]), .Novice_Mobs[1], 1, "#HH_CONTROLLER01::OnNovice1"; end;
OnNovice2:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[2])*4)+strmobinfo(7,.Novice_Mobs[2])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[2]), .Novice_Mobs[2], 1, "#HH_CONTROLLER01::OnNovice2"; end;
OnNovice3:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[3])*4)+strmobinfo(7,.Novice_Mobs[3])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[3]), .Novice_Mobs[3], 1, "#HH_CONTROLLER01::OnNovice3"; end;
OnNovice4:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[4])*4)+strmobinfo(7,.Novice_Mobs[4])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[4]), .Novice_Mobs[4], 1, "#HH_CONTROLLER01::OnNovice4"; end;
OnNovice5:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[5])*4)+strmobinfo(7,.Novice_Mobs[5])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[5]), .Novice_Mobs[5], 1, "#HH_CONTROLLER01::OnNovice5"; end;
OnNovice6:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[6])*4)+strmobinfo(7,.Novice_Mobs[6])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[6]), .Novice_Mobs[6], 1, "#HH_CONTROLLER01::OnNovice6"; end;
OnNovice7:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[7])*4)+strmobinfo(7,.Novice_Mobs[7])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[7]), .Novice_Mobs[7], 1, "#HH_CONTROLLER01::OnNovice7"; end;
OnNovice8:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[8])*4)+strmobinfo(7,.Novice_Mobs[8])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[8]), .Novice_Mobs[8], 1, "#HH_CONTROLLER01::OnNovice8"; end;
OnNovice9:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[9])*4)+strmobinfo(7,.Novice_Mobs[9])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[9]), .Novice_Mobs[9], 1, "#HH_CONTROLLER01::OnNovice9"; end;
OnNovice10:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[10])*4)+strmobinfo(7,.Novice_Mobs[10])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[10]), .Novice_Mobs[10], 1, "#HH_CONTROLLER01::OnNovice10"; end;
OnNovice11:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[11])*4)+strmobinfo(7,.Novice_Mobs[11])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[11]), .Novice_Mobs[11], 1, "#HH_CONTROLLER01::OnNovice11"; end;
OnNovice12:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[12])*4)+strmobinfo(7,.Novice_Mobs[12])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[12]), .Novice_Mobs[12], 1, "#HH_CONTROLLER01::OnNovice12"; end;
OnNovice13:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[13])*4)+strmobinfo(7,.Novice_Mobs[13])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[13]), .Novice_Mobs[13], 1, "#HH_CONTROLLER01::OnNovice13"; end;
OnNovice14:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[14])*4)+strmobinfo(7,.Novice_Mobs[14])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[14]), .Novice_Mobs[14], 1, "#HH_CONTROLLER01::OnNovice14"; end;
OnNovice15:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[15])*4)+strmobinfo(7,.Novice_Mobs[15])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[15]), .Novice_Mobs[15], 1, "#HH_CONTROLLER01::OnNovice15"; end;
OnNovice16:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[16])*4)+strmobinfo(7,.Novice_Mobs[16])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[16]), .Novice_Mobs[16], 1, "#HH_CONTROLLER01::OnNovice16"; end;
OnNovice17:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[17])*4)+strmobinfo(7,.Novice_Mobs[17])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[17]), .Novice_Mobs[17], 1, "#HH_CONTROLLER01::OnNovice17"; end;
OnNovice18:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[18])*4)+strmobinfo(7,.Novice_Mobs[18])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[18]), .Novice_Mobs[18], 1, "#HH_CONTROLLER01::OnNovice18"; end;
OnNovice19:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[19])*4)+strmobinfo(7,.Novice_Mobs[19])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[19]), .Novice_Mobs[19], 1, "#HH_CONTROLLER01::OnNovice19"; end;
OnNovice20:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Novice_Mobs[20])*4)+strmobinfo(7,.Novice_Mobs[20])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 1, .@m$, .@x, .@y);}
    areamonster "018-2-2@No", 20, 20, 220, 220, strmobinfo(1, .Novice_Mobs[20]), .Novice_Mobs[20], 1, "#HH_CONTROLLER01::OnNovice20"; end;
OnInterm0:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[0])*4)+strmobinfo(7,.Interm_Mobs[0])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[0]), .Interm_Mobs[0], 1, "#HH_CONTROLLER01::OnInterm0"; end;
OnInterm1:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[1])*4)+strmobinfo(7,.Interm_Mobs[1])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[1]), .Interm_Mobs[1], 1, "#HH_CONTROLLER01::OnInterm1"; end;
OnInterm2:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[2])*4)+strmobinfo(7,.Interm_Mobs[2])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[2]), .Interm_Mobs[2], 1, "#HH_CONTROLLER01::OnInterm2"; end;
OnInterm3:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[3])*4)+strmobinfo(7,.Interm_Mobs[3])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[3]), .Interm_Mobs[3], 1, "#HH_CONTROLLER01::OnInterm3"; end;
OnInterm4:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[4])*4)+strmobinfo(7,.Interm_Mobs[4])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[4]), .Interm_Mobs[4], 1, "#HH_CONTROLLER01::OnInterm4"; end;
OnInterm5:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[5])*4)+strmobinfo(7,.Interm_Mobs[5])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[5]), .Interm_Mobs[5], 1, "#HH_CONTROLLER01::OnInterm5"; end;
OnInterm6:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[6])*4)+strmobinfo(7,.Interm_Mobs[6])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[6]), .Interm_Mobs[6], 1, "#HH_CONTROLLER01::OnInterm6"; end;
OnInterm7:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[7])*4)+strmobinfo(7,.Interm_Mobs[7])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[7]), .Interm_Mobs[7], 1, "#HH_CONTROLLER01::OnInterm7"; end;
OnInterm8:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[8])*4)+strmobinfo(7,.Interm_Mobs[8])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[8]), .Interm_Mobs[8], 1, "#HH_CONTROLLER01::OnInterm8"; end;
OnInterm9:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[9])*4)+strmobinfo(7,.Interm_Mobs[9])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[9]), .Interm_Mobs[9], 1, "#HH_CONTROLLER01::OnInterm9"; end;
OnInterm10:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[10])*4)+strmobinfo(7,.Interm_Mobs[10])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[10]), .Interm_Mobs[10], 1, "#HH_CONTROLLER01::OnInterm10"; end;
OnInterm11:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[11])*4)+strmobinfo(7,.Interm_Mobs[11])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[11]), .Interm_Mobs[11], 1, "#HH_CONTROLLER01::OnInterm11"; end;
OnInterm12:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[12])*4)+strmobinfo(7,.Interm_Mobs[12])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[12]), .Interm_Mobs[12], 1, "#HH_CONTROLLER01::OnInterm12"; end;
OnInterm13:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[13])*4)+strmobinfo(7,.Interm_Mobs[13])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[13]), .Interm_Mobs[13], 1, "#HH_CONTROLLER01::OnInterm13"; end;
OnInterm14:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[14])*4)+strmobinfo(7,.Interm_Mobs[14])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[14]), .Interm_Mobs[14], 1, "#HH_CONTROLLER01::OnInterm14"; end;
OnInterm15:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[15])*4)+strmobinfo(7,.Interm_Mobs[15])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[15]), .Interm_Mobs[15], 1, "#HH_CONTROLLER01::OnInterm15"; end;
OnInterm16:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[16])*4)+strmobinfo(7,.Interm_Mobs[16])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[16]), .Interm_Mobs[16], 1, "#HH_CONTROLLER01::OnInterm16"; end;
OnInterm17:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[17])*4)+strmobinfo(7,.Interm_Mobs[17])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[17]), .Interm_Mobs[17], 1, "#HH_CONTROLLER01::OnInterm17"; end;
OnInterm18:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[18])*4)+strmobinfo(7,.Interm_Mobs[18])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[18]), .Interm_Mobs[18], 1, "#HH_CONTROLLER01::OnInterm18"; end;
OnInterm19:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Interm_Mobs[19])*4)+strmobinfo(7,.Interm_Mobs[19])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 2, .@m$, .@x, .@y);}
    areamonster "018-2-2@In", 20, 20, 220, 220, strmobinfo(1, .Interm_Mobs[19]), .Interm_Mobs[19], 1, "#HH_CONTROLLER01::OnInterm19"; end;
OnAdvanc0:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[0])*4)+strmobinfo(7,.Advanc_Mobs[0])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[0]), .Advanc_Mobs[0], 1, "#HH_CONTROLLER01::OnAdvanc0"; end;
OnAdvanc1:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[1])*4)+strmobinfo(7,.Advanc_Mobs[1])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[1]), .Advanc_Mobs[1], 1, "#HH_CONTROLLER01::OnAdvanc1"; end;
OnAdvanc2:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[2])*4)+strmobinfo(7,.Advanc_Mobs[2])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[2]), .Advanc_Mobs[2], 1, "#HH_CONTROLLER01::OnAdvanc2"; end;
OnAdvanc3:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[3])*4)+strmobinfo(7,.Advanc_Mobs[3])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[3]), .Advanc_Mobs[3], 1, "#HH_CONTROLLER01::OnAdvanc3"; end;
OnAdvanc4:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[4])*4)+strmobinfo(7,.Advanc_Mobs[4])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[4]), .Advanc_Mobs[4], 1, "#HH_CONTROLLER01::OnAdvanc4"; end;
OnAdvanc5:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[5])*4)+strmobinfo(7,.Advanc_Mobs[5])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[5]), .Advanc_Mobs[5], 1, "#HH_CONTROLLER01::OnAdvanc5"; end;
OnAdvanc6:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[6])*4)+strmobinfo(7,.Advanc_Mobs[6])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[6]), .Advanc_Mobs[6], 1, "#HH_CONTROLLER01::OnAdvanc6"; end;
OnAdvanc7:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[7])*4)+strmobinfo(7,.Advanc_Mobs[7])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[7]), .Advanc_Mobs[7], 1, "#HH_CONTROLLER01::OnAdvanc7"; end;
OnAdvanc8:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[8])*4)+strmobinfo(7,.Advanc_Mobs[8])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[8]), .Advanc_Mobs[8], 1, "#HH_CONTROLLER01::OnAdvanc8"; end;
OnAdvanc9:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[9])*4)+strmobinfo(7,.Advanc_Mobs[9])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[9]), .Advanc_Mobs[9], 1, "#HH_CONTROLLER01::OnAdvanc9"; end;
OnAdvanc10:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[10])*4)+strmobinfo(7,.Advanc_Mobs[10])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[10]), .Advanc_Mobs[10], 1, "#HH_CONTROLLER01::OnAdvanc10"; end;
OnAdvanc11:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[11])*4)+strmobinfo(7,.Advanc_Mobs[11])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[11]), .Advanc_Mobs[11], 1, "#HH_CONTROLLER01::OnAdvanc11"; end;
OnAdvanc12:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[12])*4)+strmobinfo(7,.Advanc_Mobs[12])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[12]), .Advanc_Mobs[12], 1, "#HH_CONTROLLER01::OnAdvanc12"; end;
OnAdvanc13:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[13])*4)+strmobinfo(7,.Advanc_Mobs[13])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[13]), .Advanc_Mobs[13], 1, "#HH_CONTROLLER01::OnAdvanc13"; end;
OnAdvanc14:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[14])*4)+strmobinfo(7,.Advanc_Mobs[14])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[14]), .Advanc_Mobs[14], 1, "#HH_CONTROLLER01::OnAdvanc14"; end;
OnAdvanc15:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[15])*4)+strmobinfo(7,.Advanc_Mobs[15])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[15]), .Advanc_Mobs[15], 1, "#HH_CONTROLLER01::OnAdvanc15"; end;
OnAdvanc16:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[16])*4)+strmobinfo(7,.Advanc_Mobs[16])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[16]), .Advanc_Mobs[16], 1, "#HH_CONTROLLER01::OnAdvanc16"; end;
OnAdvanc17:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Advanc_Mobs[17])*4)+strmobinfo(7,.Advanc_Mobs[17])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 4, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ad", 20, 20, 220, 220, strmobinfo(1, .Advanc_Mobs[17]), .Advanc_Mobs[17], 1, "#HH_CONTROLLER01::OnAdvanc17"; end;
OnExpert0:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[0])*4)+strmobinfo(7,.Expert_Mobs[0])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[0]), .Expert_Mobs[0], 1, "#HH_CONTROLLER01::OnExpert0"; end;
OnExpert1:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[1])*4)+strmobinfo(7,.Expert_Mobs[1])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[1]), .Expert_Mobs[1], 1, "#HH_CONTROLLER01::OnExpert1"; end;
OnExpert2:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[2])*4)+strmobinfo(7,.Expert_Mobs[2])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[2]), .Expert_Mobs[2], 1, "#HH_CONTROLLER01::OnExpert2"; end;
OnExpert3:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[3])*4)+strmobinfo(7,.Expert_Mobs[3])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[3]), .Expert_Mobs[3], 1, "#HH_CONTROLLER01::OnExpert3"; end;
OnExpert4:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[4])*4)+strmobinfo(7,.Expert_Mobs[4])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[4]), .Expert_Mobs[4], 1, "#HH_CONTROLLER01::OnExpert4"; end;
OnExpert5:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[5])*4)+strmobinfo(7,.Expert_Mobs[5])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[5]), .Expert_Mobs[5], 1, "#HH_CONTROLLER01::OnExpert5"; end;
OnExpert6:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[6])*4)+strmobinfo(7,.Expert_Mobs[6])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[6]), .Expert_Mobs[6], 1, "#HH_CONTROLLER01::OnExpert6"; end;
OnExpert7:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[7])*4)+strmobinfo(7,.Expert_Mobs[7])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[7]), .Expert_Mobs[7], 1, "#HH_CONTROLLER01::OnExpert7"; end;
OnExpert8:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[8])*4)+strmobinfo(7,.Expert_Mobs[8])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[8]), .Expert_Mobs[8], 1, "#HH_CONTROLLER01::OnExpert8"; end;
OnExpert9:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[9])*4)+strmobinfo(7,.Expert_Mobs[9])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[9]), .Expert_Mobs[9], 1, "#HH_CONTROLLER01::OnExpert9"; end;
OnExpert10:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[10])*4)+strmobinfo(7,.Expert_Mobs[10])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[10]), .Expert_Mobs[10], 1, "#HH_CONTROLLER01::OnExpert10"; end;
OnExpert11:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[11])*4)+strmobinfo(7,.Expert_Mobs[11])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[11]), .Expert_Mobs[11], 1, "#HH_CONTROLLER01::OnExpert11"; end;
OnExpert12:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[12])*4)+strmobinfo(7,.Expert_Mobs[12])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[12]), .Expert_Mobs[12], 1, "#HH_CONTROLLER01::OnExpert12"; end;
OnExpert13:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[13])*4)+strmobinfo(7,.Expert_Mobs[13])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[13]), .Expert_Mobs[13], 1, "#HH_CONTROLLER01::OnExpert13"; end;
OnExpert14:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Expert_Mobs[14])*4)+strmobinfo(7,.Expert_Mobs[14])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 8, .@m$, .@x, .@y);}
    areamonster "018-2-2@Ex", 20, 20, 220, 220, strmobinfo(1, .Expert_Mobs[14]), .Expert_Mobs[14], 1, "#HH_CONTROLLER01::OnExpert14"; end;
OnMaster0:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[0])*4)+strmobinfo(7,.Master_Mobs[0])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[0]), .Master_Mobs[0], 1, "#HH_CONTROLLER01::OnMaster0"; end;
OnMaster1:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[1])*4)+strmobinfo(7,.Master_Mobs[1])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[1]), .Master_Mobs[1], 1, "#HH_CONTROLLER01::OnMaster1"; end;
OnMaster2:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[2])*4)+strmobinfo(7,.Master_Mobs[2])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[2]), .Master_Mobs[2], 1, "#HH_CONTROLLER01::OnMaster2"; end;
OnMaster3:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[3])*4)+strmobinfo(7,.Master_Mobs[3])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[3]), .Master_Mobs[3], 1, "#HH_CONTROLLER01::OnMaster3"; end;
OnMaster4:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[4])*4)+strmobinfo(7,.Master_Mobs[4])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[4]), .Master_Mobs[4], 1, "#HH_CONTROLLER01::OnMaster4"; end;
OnMaster5:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[5])*4)+strmobinfo(7,.Master_Mobs[5])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[5]), .Master_Mobs[5], 1, "#HH_CONTROLLER01::OnMaster5"; end;
OnMaster6:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[6])*4)+strmobinfo(7,.Master_Mobs[6])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[6]), .Master_Mobs[6], 1, "#HH_CONTROLLER01::OnMaster6"; end;
OnMaster7:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[7])*4)+strmobinfo(7,.Master_Mobs[7])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[7]), .Master_Mobs[7], 1, "#HH_CONTROLLER01::OnMaster7"; end;
OnMaster8:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[8])*4)+strmobinfo(7,.Master_Mobs[8])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[8]), .Master_Mobs[8], 1, "#HH_CONTROLLER01::OnMaster8"; end;
OnMaster9:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[9])*4)+strmobinfo(7,.Master_Mobs[9])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[9]), .Master_Mobs[9], 1, "#HH_CONTROLLER01::OnMaster9"; end;
OnMaster10:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[10])*4)+strmobinfo(7,.Master_Mobs[10])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[10]), .Master_Mobs[10], 1, "#HH_CONTROLLER01::OnMaster10"; end;
OnMaster11:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[11])*4)+strmobinfo(7,.Master_Mobs[11])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[11]), .Master_Mobs[11], 1, "#HH_CONTROLLER01::OnMaster11"; end;
OnMaster12:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[12])*4)+strmobinfo(7,.Master_Mobs[12])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[12]), .Master_Mobs[12], 1, "#HH_CONTROLLER01::OnMaster12"; end;
OnMaster13:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[13])*4)+strmobinfo(7,.Master_Mobs[13])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[13]), .Master_Mobs[13], 1, "#HH_CONTROLLER01::OnMaster13"; end;
OnMaster14:
    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.Master_Mobs[14])*4)+strmobinfo(7,.Master_Mobs[14])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, 16, .@m$, .@x, .@y);}
    areamonster "018-2-5@Ma", 20, 20, 220, 220, strmobinfo(1, .Master_Mobs[14]), .Master_Mobs[14], 1, "#HH_CONTROLLER01::OnMaster14"; end;

}

/*python
df=[('Novice',   21, 1),
    ('Interm', 20, 2),
    ('Advanc',   18, 4),
    ('Expert',   15, 8),
    ('Master',   15, 16)]

for a in df:
    i=0
    while (i < a[1]):
        print('On%s%d:' % (a[0], i))
        print('    if (rand(0, 2000) <= readparam(Luk)+(strmobinfo(3,.%s_Mobs[%d])*4)+strmobinfo(7,.%s_Mobs[%d])) {getmapxy(.@m$, .@x, .@y, 0); makeitem(HeroCoin, %s, .@m$, .@x, .@y);}' % (a[0], i, a[0], i, a[2]))
        print('    areamonster "018-2-2@%s", 20, 20, 220, 220, strmobinfo(1, .%s_Mobs[%d]), .%s_Mobs[%d], 1, "#HH_CONTROLLER01::On%s%d"; end;' % (a[0][:2],a[0],i,a[0],i,a[0],i))
        i+=1
*/
