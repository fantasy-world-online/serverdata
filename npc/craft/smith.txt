// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Smith System (Player, Guild, NPC)
// Notes:
//  Base for Evol MR
// This one is more crazy. Cannot be equipping target craft.
// After successful craft, we use CraftDB return code to equip() the
// new item and apply a random option bonus based on crafter skills
// eg. setequipoption(EQI_HAND_R, 1, VAR_STRAMOUNT, 5)
// We should be able to apply several bonuses for the nicest experience :3
// We should also add a movespeed bonus... So demure can specialize herself in
// crafting really fast weapons in every aspects, and this system would allow her
// to do so :> But then, maybe we should have a crafting skill where players can
// allocate status points?

// Usage: SmithSystem ({scope})
// Scopes: CRAFT_NPC, CRAFT_PLAYER, CRAFT_GUILD
// If an invalid scope is passed, .knowledge won't be set but will be required
// Returns true on success, false on failure
function	script	SmithSystem	{
    // Set .scope, .knowledge and .success
    .scope=getarg(0, CRAFT_PLAYER);
    if (.scope == CRAFT_PLAYER)
        copyarray(.knowledge,RECIPES_EQUIPMENT,getarraysize(RECIPES_EQUIPMENT));
    else if (.scope == CRAFT_GUILD)
        copyarray(.knowledge,$@RECIPES_EQUIPMENT[getcharid(2)],getarraysize($@RECIPES_EQUIPMENT[getcharid(2)]));
    .success=false;

    setskin "craft4";
    .@var$ = requestcraft(4);
    .@craft = initcraft(.@var$);
    .@entry = findcraftentry(.@craft, CRAFT_EQUIPMENT);
    if (debug || $@GM_OVERRIDE) mes "found craft entry: " + .@entry;
    if (debug || $@GM_OVERRIDE) mes "knowledge value: " + .knowledge[.@entry];
    if (.@entry < 0) {
            .success=false;
    } else {
        if (.scope == CRAFT_NPC) {
            usecraft .@craft;
            .@it=getcraftcode(.@entry);
            getitem(.@it, 1);
            .success=true;
        } else if (.knowledge[.@entry] || $@GM_OVERRIDE) {
            // Player craft item
            // Mark the crafting in your score book
            CRAFTING_SCORE+=1;
            usecraft .@craft;
            .@it=getcraftcode(.@entry);
            getnameditem(.@it, strcharinfo(0));
            if (getskilllv(TMW2_CRAFT)) {
                delinventorylist(); // Needed, because we'll rely on rfind()
                getinventorylist();
                .@index=array_rfind(@inventorylist_id, .@it);
                if (csys_Check(.@index, 75000)) {
                    csys_Apply(.@index);
                }
            }

            // Get experience for the craft
            .@xp=getiteminfo(.@it, ITEMINFO_SELLPRICE);
            getexp .@xp+BaseLevel, (.@xp/3)+BaseLevel+JobLevel;

            .success=true;
        } else {
            .success=false;
        }
    }
    deletecraft .@craft;
    setskin "";
    return .success;
}
