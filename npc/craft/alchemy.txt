// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Alchemy System (Player, Guild, NPC)
// Notes:
//  Base for Evol MR

// Usage: AlchemySystem ({scope})
// Scopes: CRAFT_NPC, CRAFT_PLAYER, CRAFT_GUILD
// If an invalid scope is passed, .knowledge won't be set but will be required
// Returns true on success, false on failure
function	script	AlchemySystem	{
    // Set .scope, .knowledge and .success
    .scope=getarg(0, CRAFT_PLAYER);
    if (.scope == CRAFT_PLAYER)
    {
        copyarray(.knowledge,RECIPES_ALCHEMY,getarraysize(RECIPES_ALCHEMY));
    }
    else if (.scope == CRAFT_GUILD)
    {
       copyarray( .knowledge,getd("$RECIPES_ALCHEMY_"+getcharid(2)),getarraysize(getd("$RECIPES_ALCHEMY_"+getcharid(2))) );
    }
    .success=false;

    setskin "craft2";
    .@var$ = requestcraft(2);
    .@craft = initcraft(.@var$);
    .@entry = findcraftentry(.@craft, CRAFT_ALCHEMY);
    if (debug || $@GM_OVERRIDE) mes "found craft entry: " + .@entry;
    if (debug || $@GM_OVERRIDE) mes "knowledge value: " + .knowledge[.@entry];
    if (.@entry < 0) {
            .success=false;
    } else {
        if (.scope == CRAFT_NPC || .knowledge[.@entry]) {
            usecraft .@craft;
            .success=true;
        } else {
            .success=false;
        }
    }
    deletecraft .@craft;
    setskin "";
    return .success;
}
