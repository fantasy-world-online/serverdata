// TMW2 scripts.
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Eistein rewards players for getting level landmarks. Controls level up.

003-0-1,23,25,0	script	Eistein	NPC_UKAR,{

    // Level, Reward
    function is_level {
        if (BaseLevel >= getarg(0)) {
            getitem getarg(1),1;
            setq TulimsharQuest_Eistein, getq(TulimsharQuest_Eistein)+1;
            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                l("Congrats you passed the level cap of @@! Here is a(n) @@, you deserve it.",getarg(0), getitemlink(getarg(1)));
        } else {
            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                l("You are level @@/@@. Keep building levels, you need them!", BaseLevel, getarg(0));
        }
    }


    function quest_open {
        .@q = getq(TulimsharQuest_Eistein);
        switch (.@q) {
            case 0:
                is_level(25, BronzeGift); break;
            case 1:
                is_level(50, GraduationCap); break;
            case 2:
                is_level(75, SilverGift); break;
            case 3:
                is_level(100, GoldenGift); break;
            case 4:
                is_level(125, PrismGift); break;
            case 5:
                is_level(150, SupremeGift); break;
            default:
                mesn;
                mesq l("Waw, you are level @@! Many congratulations. If there were people like you, ukarania wouldn't have been destroyed...", BaseLevel);
                break;
        }
    }

    speech S_LAST_NEXT,
        l("Ah, welcome. Please, don't be afraid of my look, Saulc GM assigned me to here."),
        l("I'm Eistein, survivor from Ukarania. I reward brave adventurers who kill monsters, which plague our lands."),
        l("I'll give you a reward, in the name of Saulc, once you reach the following levels: 25, 50, 75, 100, 125 and 150.");
        do
        {
            select
                l("Interesting! can I be rewarded for my help?"),
                l("What about job levels and job experience?"),
                l("Quit");

            switch (@menu) {
                case 1:
                    quest_open;
                    break;
                case 2:
                    mesn; mesq l("Job levels already boosts all your status. It gives +1 on each status every 10 job levels."); next;
                    break;
            }
        } while (@menu != 3);

    closedialog;
    goodbye;
    close;

// Give skills
OnPCBaseLvUpEvent:
    // Isso é um desperdício, é mais fácil conferir a classe antes
    // Vai gastar CPU a toa

    // Mago Dano
    mskill(1,  CL_MAGODANO, MG_FIREBALL);
	mskill(5,  CL_MAGODANO, MG_NAPALMBEAT);
    mskill(10, CL_MAGODANO, TF_BACKSLIDING);
    mskill(15, CL_MAGODANO, SC_INVISIBILITY);
    mskill(20, CL_MAGODANO, SO_FIREWALK);
    mskill(25, CL_MAGODANO, WL_CRIMSONROCK);
    mskill(30, CL_MAGODANO, HW_MAGICPOWER);

    // Mago Suporte
    mskill(1, CL_MAGOSUPORTE, MG_COLDBOLT);
	mskill(5,  CL_MAGOSUPORTE, MG_NAPALMBEAT);
	mskill(10,  CL_MAGOSUPORTE, AB_HIGHNESSHEAL);
    mskill(15,  CL_MAGOSUPORTE, ALL_RESURRECTION);
    mskill(20, CL_MAGOSUPORTE, SC_ENERVATION);
    mskill(25, CL_MAGOSUPORTE, SC_UNLUCKY);
    mskill(30, CL_MAGOSUPORTE, SO_STRIKING);


    // Guerreiro Dano
    mskill(1,  CL_GUERREIRODANO, SM_BASH);
	mskill(5, CL_GUERREIRODANO, ASC_METEORASSAULT);
    mskill(10,  CL_GUERREIRODANO, GC_DARKILLUSION);
    mskill(15, CL_GUERREIRODANO, RG_INTIMIDATE);
	mskill(20, CL_GUERREIRODANO, KN_AUTOCOUNTER);
    mskill(25, CL_GUERREIRODANO, WL_SIENNAEXECRATE);
    mskill(30, CL_GUERREIRODANO, GC_DARKCROW);

    // Guerreiro Tanker
	mskill(1,  CL_GUERREIROTANKER, SM_BASH);
	mskill(5, CL_GUERREIROTANKER, ASC_METEORASSAULT);
    mskill(10,  CL_GUERREIROTANKER, EVOL_MASS_PROVOKE);
    mskill(15, CL_GUERREIROTANKER, ALL_FULL_THROTTLE);
	mskill(20,  CL_GUERREIROTANKER, AL_ANGELUS);
	mskill(25, CL_GUERREIROTANKER, SC_SHADOWFORM);
	mskill(30, CL_GUERREIROTANKER, RG_CLOSECONFINE);

    // Arqueiro
    mskill(1, CL_ARQUEIRO, AC_CHARGEARROW);
    mskill(5, CL_ARQUEIRO, SN_SHARPSHOOTING);
	mskill(10,  CL_ARQUEIRO, SN_SIGHT);
	mskill(15,  CL_ARQUEIRO, SN_WINDWALK);
    mskill(20, CL_ARQUEIRO, WL_CHAINLIGHTNING);
	mskill(25, CL_ARQUEIRO, AC_CONCENTRATION);
	mskill(30, CL_ARQUEIRO, SC_GROOMY);

    // General Updater
    switch (BaseLevel) {
    case 15:
        sc_end SC_OVERLAPEXPUP;
        break;
    case 25:
    case 50:
    case 75:
    case 100:
    case 125:
    case 150:
        dispbottom l("Milestone levelup: A reward can now be claimed in Tulimshar.");
        break;
    }
    end;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, GraduationCap);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SilkRobe);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 26);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 0);
    npcsit;

    .sex = G_MALE;
    .distance = 4;
    end;
}
