// TMW-2 Script.
// Author:
//    Saulc
//    Jesusalva
// Notes:
//    Bakes Tonori Delight

003-0-1,77,40,0	script	Maxime	NPC_PLAYER,{
    mesn;
    mesq l("Hello. I know the secrets of the legendary @@.", getitemlink(TonoriDelight));
    next;
    mesn;
    mesq l("I could easily bake one for you, provided you bring me the following:");
    mesc l("@@/12 @@", countitem(MaggotSlime), getitemlink(MaggotSlime));
    mesc l("@@/8 @@", countitem(Plushroom), getitemlink(Plushroom));
    mesc l("@@/4 @@", countitem(MushroomSpores), getitemlink(MushroomSpores));
    mesc l("@@/3 @@", countitem(ScorpionStinger), getitemlink(ScorpionStinger));
    mesc l("@@/2 @@", countitem(CactusDrink), getitemlink(CactusDrink));
    mesc l("@@/1 @@", countitem(RoastedMaggot), getitemlink(RoastedMaggot));
    mesc l("@@/120 GP", format_number(Zeny));
    next;
    select
        l("I have the items, please bake for me"),
        l("Ah, nice to know.");

    mes "";

    if (@menu == 2)
        goto L_Close;

    if (
        countitem(MaggotSlime) < 12 ||
        countitem(Plushroom) < 8 ||
        countitem(MushroomSpores) < 4 ||
        countitem(ScorpionStinger) < 3 ||
        countitem(CactusDrink) < 2 ||
        countitem(RoastedMaggot) < 1 ||
        Zeny < 120) goto L_Missing;

    // 4~7 normaly, 5~10 during Summer
    // Xanthem Patch: +20% craft and -20% effect
    // New values: 5~9 normal or 6~12 during Summer
    inventoryplace TonoriDelight, 12;
    delitem MaggotSlime, 12;
    delitem Plushroom, 8;
    delitem MushroomSpores, 4;
    delitem ScorpionStinger, 3;
    delitem CactusDrink, 2;
    delitem RoastedMaggot, 1;
    getitem TonoriDelight, rand(5,9);
    if (season() == SUMMER) {
        getitem TonoriDelight, rand(1,3);
        mesc l("During summer, more Tonori Delight can be produced.");
    }
    Zeny=Zeny-120;
    getexp rand(30, 60), 0;

    mesn;
    mesq l("Here you go, fresh from the oven!");
    next;

L_Close:
    closedialog;
    goodbye;
    close;

L_Missing:
    mesn;
    mesq l("You don't have everything I asked you for.");
    next;
    mesn;
    mesq l("I always wonder if I should raise my price to teach bad kids to don't lie.");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, ChefHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SilkRobe);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonTrousers);
    setunitdata(.@npcId, UDT_WEAPON, AssassinBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 26);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 2);

    .sex = G_MALE;
    .distance = 4;
    npcsit;
    end;
}

