// TMW2 Scripts
// Author:
//    Jesusalva
// Description:
//    Gambler: Will next card be better or worse?
//    $XAN_BETS: How many victories all players already had (for propaganda :3)

020-4,69,35,0	script	Gambler#020-4	NPC_FLOPPED_NOBLEMAN,{
    function cardname{
        switch (getarg(0)) {
        case 0:
            return "A"; break;
        case 10:
            return "J"; break;
        case 11:
            return "Q"; break;
        case 12:
            return "K"; break;
        case 13:
            return l("Joker"); break;
        default:
            return getarg(0)+1;
        }
    }

    goto L_Menu;

L_Menu:
    showavatar;
    mesn;
    mesc l("Hey, I am flopped. Do you want to gamble?");
    mesc l("You need a @@. I'll flip one card, and you'll need to decide if next draw will be HIGHER or LOWER.", getitemlink(CasinoCoins));
    mesc l("If a tie happens, I'll give your coin back.");
    next;
    menu
        rif(countitem(CasinoCoins) >= 1, l("Let's play!")), L_Spin,
        l("Information"), L_Info,
        l("Leave"), L_Quit;

L_Info:
    mes "";
    mesc l("Rules:");
    mesc l("A card will be flipped, you'll need to decide if next flip will be HIGHER or LOWER.");
    mesc l("Cards are ranked on this priority: A - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 9 - 10 - J - Q - K - Joker");
    next;
    mesc l("Prizes:");
    mesc l("If you're right, you'll get at least 80 GP!");
    mesc l("If a tie happens, you'll get your coin back.");
    mesc l("If you're wrong, your winning streak is reset.");
    mesc l("Winning Streak is also reset on logout or when you leave the Inn.");
    next;
    mesc l("Winning Strike Prizes:");
    mesc l("Every seven successive right guesses, you'll get 2x @@!", getitemlink(BronzeGift));
    mesc l("Every fifteen successive right guesses, you'll get a @@!", getitemlink(BronzeGift));
    mesc l("Every fifty successive right guesses, you'll get a @@!", getitemlink(SilverGift));
    mesc l("Every 101 successive right guesses, you'll get a @@!", getitemlink(GoldenGift));
    next;
    goto L_Menu;


L_Spin:
    showavatar AVATAR_CARD_COVER;
    mesc l("I'll draw a card now!");
    next;
    delitem CasinoCoins, 1;
    // First card will never be an edge card (Ace or Joker), so you can ALWAYS guess wrong.
    .@card1=rand(1, 12);
    showavatar 1000+.@card1;
    mesn;
    mesc l("It's a @@!", cardname(.@card1));
    mesc l("Will next draw be HIGHER or LOWER?!");
    next;
    mesc l("Cards are ranked on this priority: A - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 9 - 10 - J - Q - K - Joker");
    select
        l("HIGHER!"),
        l("LOWER!");
    mes "";

    // Flip Flop!
    .@card2=rand(0, 13);
    showavatar 1000+.@card2;
    mesn;
    mesc l("It's a @@!", cardname(.@card2));

    if (.@card1 == .@card2) {
        mesc l("It's a tie!");
        getitem CasinoCoins, 1;
        .@bypass=1;
    } else if (.@card2 < .@card1 && @menu == 2) {
        mesc l("It's lower! That's right!");
        Zeny=Zeny+80;
        @gambler_winstreak=@gambler_winstreak+1;
    } else if (.@card2 > .@card1 && @menu == 1) {
        mesc l("It's higher! That's right!");
        Zeny=Zeny+80;
        @gambler_winstreak=@gambler_winstreak+1;
    } else {
        mesc l("You were wrong!");
        @gambler_winstreak=0;
    }

    // Winning Streak
    if (!.@bypass && @gambler_winstreak) {
        if (@gambler_winstreak % 7 == 0)
            getitem BronzeGift, 1;
        if (@gambler_winstreak % 15 == 0)
            getitem BronzeGift, 1;
        if (@gambler_winstreak % 50 == 0)
            getitem SilverGift, 1;
        if (@gambler_winstreak % 101 == 0)
            getitem GoldenGift, 1;
        mesc l("Your current win streak is @@!", @gambler_winstreak);
        $XAN_BETS=$XAN_BETS+1;
        Zeny+=min((@gambler_winstreak*2), 40);
    } else {
        .@bypass=0;
    }
    next;
    goto L_Menu;

L_Quit:
    close;

OnInit:
    .sex = G_MALE;
    .distance = 4;
    end;

}


// Random NPC
020-4,70,36,4	script	Gambling Xan	NPC_PLAYER,{
    mesn;
    mesq l("Argh... I can never get it right! If only he drew an Ace or a Joker on the first draw... But he never does that!");
    next;
    mesn;
    mesq l("That rat... I already spent @@ @@ with him!!", $XAN_BETS, getitemlink(CasinoCoins));
    mes l("If I weren't tempted to try again everytime someone gets it right...");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CreasedShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, JeansShorts);

    .sex = G_MALE;
    .distance = 4;
    npcsit;
    end;
}
