// TMW2 Script.
// Author:
//    Jesusalva
// Description:
//    Baktar
// Quests:
//  NivalisQuest_Baktar
//    .@q1 = Controls Braknar Shield
//          (0= Not met, 1= Khafar, 2= Baktar, 3= Complete)
//    .@q2 = bitmask with Tulimshar items given

020-4,64,39,6	script	Baktar	NPC_RAIJIN,{
    .@q=getq(NivalisQuest_Baktar);
    .@q2=getq2(NivalisQuest_Baktar);
    if (!.@q)
        goto L_Start;
    if (.@q == 2)
        goto L_Braknar;
    goto L_Collector;

L_Start:
    mesn;
    if (rand(1,5) == 4) mesc l("*cough cough*");
    mesq l("Hello, did you come from Tulimshar? Because you have a nice tan.");
    next;
    select
        l("Yes, I'm coming from Tulimshar."),
        l("Well, I just like sunbathing."),
        l("Good bye.");
    mes "";
    switch (@menu) {
        case 2:
            mesn;
            if (rand(1,5) == 4) mesc l("*cough cough*");
            mesq l("Bah! Don't try to fool me! The sun can't burn in Nivalis.");
            // Don't stop now
        case 3:
            if (rand(1,5) == 4)
                mesc l("*cough cough*");
            close;
            break;
    }

    setq NivalisQuest_Baktar, 1, 0;
    mesn;
    if (rand(1,5) == 4) mesc l("*cough cough*");
    mesq l("All my parents, grandparents, until the world was born, are from Tulimshar.");
    next;
    mesn;
    mesc l("*cough cough*");
    mesq l("Thus, I like to collect small memetos from Tulimshar.");
    next;
    mesn;
    mesq l("The doctor told me to move here after I got tuberculosis... But I like Tulimshar so much! Please bring me Tulimshar souvenirs, I'll pay you well!");
    close;


L_Collector:
    mesn;
    if (rand(1,5) == 4) mesc l("*cough cough*");
    mesq l("Did you brought me an souvenir from Tulimshar?");
    mes "";
    mes "##B" + l("Drag and drop an item from your inventory.") + "##b";

    .@id = requestitem();

    if (.@id < 1) close;
    if (countitem(.@id) < 1 || checkbound(.@id))
        close;

    // No item
    .@m = htget(.TULIMITEM, str(.@id), 0);
    if (!.@m) {
        mesn;
        if (rand(1,5) == 4) mesc l("*cough cough*");
        mesq l("What crap is that?! This is not from Tulimshar!");
        next;
        mesn;
        if (rand(1,5) == 4) mesc l("*cough cough*");
        mesq l("I won't accept stuff from Halinarzo!");
        mesc l("Baktar can be picky with Tulimshar stuff, too.");
        close;
    }
    // Already given
    if (.@q2 & .@m) {
        mesn;
        if (rand(1,5) == 4) mesc l("*cough cough*");
        mesq l("I already have that...");
        close;
    }

    // Gives 2.5× more
    .@sp=getiteminfo(.@id, ITEMINFO_SELLPRICE);
    mesc l("Really give your @@ to Baktar?", getitemlink(.@id)), 1;
    mesc l("The item will be lost forever.");
    next;
    if (askyesno() == ASK_YES) {
        delitem .@id, 1;
        Zeny+=(.@sp*25/10);
        getexp BaseLevel*.@sp, .@sp;
        setq2 NivalisQuest_Baktar, .@q2|.@m;
        mesn;
        if (rand(1,5) == 4) mesc l("*cough cough*");
        mesq l("Thanks.");
    }

    close;

L_Braknar:
    mesn;
    if (rand(1,5) == 4)
        mesc l("*cough cough*");
    mesq l("Welcome back.");
    select
        l("I brought a souvenir for you."),
        l("Do you know someone called Braknar?"),
        l("Er, uhm, hi!");
    mes "";
    switch (@menu) {
        case 1: goto L_Collector;
        case 2:
            mesn;
            if (rand(1,5) == 4) mesc l("*cough cough*");
            mesq l("Yeah, it was my grand-grand-grandfather.");
            next;
            mesn strcharinfo(0);
            mes l("'-' \"Could you give me his shield? Pretty please? I need it to survive and bring Tulimshar goodies!\"");
            next;
            mesn;
            if (rand(1,5) == 4) mesc l("*cough cough*");
            mesq l("No, it is a family heirloom. But I do have the shield blueprints. A skilled craftsman could forge one.");
            next;
            mesn strcharinfo(0);
            mes l("*-* \"Could you share those blueprints with me? Please?\"");
            next;
            mesn;
            if (rand(1,5) == 4) mesc l("*cough cough*");
            if (!.@q2) {
                mesq l("Why should I? Go away. %%n");
                close;
            }
            mesq l("Sure. Here, take it.");
            setq1 NivalisQuest_Baktar, 3;
            break;
        case 3: close;
    }
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, KnitHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, LeatherShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonShorts);
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 24);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 1);

    .sex = G_MALE;
    .distance = 5;
    npcsit;

    // Constants
    .TULIMITEM = htnew;
    htput(.TULIMITEM, str(DesertHat), 1);
    htput(.TULIMITEM, str(SerfHat), 2);
    htput(.TULIMITEM, str(Dagger), 4);
    end;
}

