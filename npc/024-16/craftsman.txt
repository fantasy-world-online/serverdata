// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Craftmaster, teaches player TMW2_CRAFT

024-16,27,42,0	script	Dwarf Craftsmaster	NPC_DWARF_CRAFTMASTER,{
    function calcRequisites;
    function calcPrices;
    function calcUpgrade;
    .@q=getq(General_Narrator);
    if (.@q < 13) {
        hello;
        end;
    }
    mesn;
    mesq lg("Look what we have here, it is a girl!", "Look what we have here, it is a boy!");
    next;
    mesn;
    mesq l("I'm Thurgar the mighty craftsman. I can make anything reality! But I only work to the king!");
    next;
    mesn;
    mesq l("...Unless, of course, if you're interested in learning this art. You'll not regret it, I assure you.");
    next;
    // Main Loop
    mesc l("Crafting Skill Level: @@", getskilllv(TMW2_CRAFT));
    msObjective(CRAFTING_SCORE>=calcRequisites(),
                l("Completed Crafts: @@/@@", CRAFTING_SCORE, calcRequisites()) );
    msObjective(Zeny >= calcPrices(),
                l("Money: @@ GP", format_number(Zeny)) );
    mes "";
    select
        rif(!CRAFTING_SCORE, l("How can I complete a craft?")),
        rif(CRAFTING_SCORE >= calcRequisites(), l("Learn crafting for @@ GP", format_number(calcPrices())) ),
        rif(CRAFTING_SCORE, l("How can I complete a craft?")),
        l("Nothing for now, thanks.");
    mes "";
    switch (@menu) {
    case 1:
    case 3:
        mesn;
        mesq l("Well, first of, you'll need an @@ and an Equipment Recipe.", getitemlink(RecipeBook));
        next;
        if (!countitem(RecipeBook)) {
            mesn;
            mesq l("If you don't have the recipe book, you'll need to find one... Maybe someone in a household you've already helped is willing to give you one.");
            next;
        }
        mesn;
        mesq l("Anyway, once you have the recipe book and learned a recipe, you can craft items in forges. I think you can buy it in your apartment.");
        next;
        mesn;
        mesq l("That will help you to make your very first first craft! Remember to use @@ to change which bonuses can be applied to your craft items.", b("@ucp"));
        next;
        mesn;
        mesq l("I think someone on Tulimshar is capable to teach you these bonuses. Eh, I don't know. Haven't been there for a while.");
        break;
    case 2:
        if (calcUpgrade()) {
            mesn;
            mesq l("There you go. Craft hard, mwhahahahaha!");
        } else {
            mesn;
            mesq l("You don't have met all requisites, like money and successful crafts, or you already reached the maximum level for this skill.");
        }
        break;
    }
    close;

// Calc successful crafts required to learn crafting
// Returns amount of crafts needed
function calcRequisites {
    switch (getskilllv(TMW2_CRAFT)) {
    case 0:
        return 0;
    case 1:
        return 3;
    case 2:
        return 7;
    case 3:
        return 12;
    case 4:
        return 18;
    case 5:
        return 24;
    case 6:
        return 32;
    }
    return -1;
}

// Calc how much GP the skill will cost you
// Returns amount of GP
function calcPrices {
    switch (getskilllv(TMW2_CRAFT)) {
    case 0:
        return 1000;
    case 1:
        return 5000;
    case 2:
        return 9000;
    case 3:
        return 15000;
    case 4:
        return 27000;
    case 5:
        return 36000;
    case 6:
        return 50000;
    }
    return false;
}

// calcUpgrade() returns true if skill
// can be leveled up. And levels it up.
function calcUpgrade {
    .@gp=calcPrices();
    .@cf=calcRequisites();
    if (Zeny < .@gp)
        return false;
    if (CRAFTING_SCORE < .@cf)
        return false;
    if (.@cf < 0)
        return false;

    // You may get a free blueprint
    switch (getskilllv(TMW2_CRAFT)) {
        case 0:
            inventoryplace EquipmentBlueprintA, 1;
            getitem EquipmentBlueprintA, 1;

            // We should aid you getting basic skills - for free!
            if (!CRAFTSYS[CRGROUP_BASE])
                CRAFTSYS[CRGROUP_BASE]+=1;
            if (!CRAFTSYS_CURRENT)
                CRAFTSYS_CURRENT=CRAFTSYS_CURRENT|CRGROUP_BASE;

            break;
        case 1:
            inventoryplace EquipmentBlueprintB, 1;
            getitem EquipmentBlueprintB, 1;
            break;
        case 2:
            inventoryplace EquipmentBlueprintC, 1;
            getitem EquipmentBlueprintC, 1;
            break;
        case 3:
            inventoryplace EquipmentBlueprintD, 1;
            getitem EquipmentBlueprintD, 1;
            break;
        case 4:
            inventoryplace EquipmentBlueprintE, 1;
            getitem EquipmentBlueprintE, 1;
            break;
        default:
            getexp 1700, 200;
            break;
    }

    Zeny-=.@gp;
    skill TMW2_CRAFT, getskilllv(TMW2_CRAFT)+1, 0;
    return true;
}

OnInit:
    .distance=5;
    end;
}

