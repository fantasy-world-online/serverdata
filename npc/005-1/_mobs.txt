// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 005-1: Candor Island mobs
005-1,43,50,2,1	monster	Clover Field	1028,1,35000,300000
005-1,30,66,1,0	monster	Diamond Bif	1108,2,35000,35000,Rosen::OnKillMBif
005-1,70,94,7,6	monster	Maggot	1030,12,15000,15000,Trainer::OnKillMaggot
005-1,89,97,8,9	monster	Candor Scorpion	1073,7,35000,35000,Trainer::OnKillCandorScorpion
005-1,43,115,6,3	monster	Piou	1002,1,35000,300000
005-1,87,43,16,15	monster	Candor Scorpion	1073,12,35000,35000,Trainer::OnKillCandorScorpion
005-1,78,49,28,10	monster	Mana Bug	1075,5,35000,35000,Trainer::OnKillManaBug
005-1,37,49,12,14	monster	Scorpion	1071,7,35000,35000,Trainer::OnKillScorpion
005-1,89,97,8,9	monster	Jack	1120,1,3600000,3600000
