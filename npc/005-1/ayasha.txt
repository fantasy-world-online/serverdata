// Author:
//    Crazyfefe
//    Jesusalva
// Description:
//  Ayasha takes care of the children of Candor Village, and is in charge of protecting them.
//  The kids sometimes go play outside of city walls, and this makes her upset.
//  Even if no major attack happened in Candor for a while.
// Variables:
//  CandorQuest_HAS
//      0: Not met
//      1: Already met
//      2: Quest ongoing
//      3: Found all kids
//      4: Reward given

005-1,59,91,0	script	Ayasha	NPC_HUMAN_FEMALE_NOOB,{
    showavatar NPC_HUMAN_FEMALE_NOOB;  // this is handled by avatars.xml
    if (strcharinfo(2) == "Monster King" && is_admin()) goto L_MKControl;

    function quest_findAllKids {
        setq CandorQuest_HAS, 2;
        mesn;
        mesq l("Good luck!");
        close;
    }

    .@has = getq(CandorQuest_HAS);
    if (.@has == 0) {
        speech S_LAST_NEXT,
            l("Ah, the kids are playing hide and seek, but I am afraid they went too far. A monster attack could start anytime, after all."),
            l("I am currently very worried with them. They're just children! They don't know how to fight!"),
            l("Could you perhaps help me to find all kids?");
        do
        {
            select
                l("Yes!"),
                l("I can't, sorry.");

            setq CandorQuest_HAS, 1;
            switch (@menu) {
                case 1:
                    quest_findAllKids;
                    break;
            }
        } while (@menu != 2);
    } else if (.@has == 1) {
        mesn;
        mesq l("Even if the city has not been attacked on the last few years, I can't help but be concerned while the kids are playing hide and seek.");
        next;
        mesq l("Could you perhaps help me to find all kids?");
        next;

        do
        {
            select
                l("Yes!"),
                l("I can't, sorry.");

            switch (@menu) {
                case 1:
                    quest_findAllKids;
                    break;
            }
        } while (@menu != 2);
    } else if (.@has == 2) {
        mesn;
        mesq l("You still haven't found all of them yet.");
        close;
    } else if (.@has == 3) {
        speech S_LAST_NEXT,
            l("Thank you, here is your reward and... some pocket money.");
            narrator("You receive 96 exp and 200 GP.");
        getexp 150, 0;
        Zeny = (Zeny + 200);
        setq CandorQuest_HAS, 4;
        close;
    } else {
        mesn;
        mesq l("Thank you for your help.");
        if (BaseLevel >= 24 && gettimetick(2) > .RENT_TIME) goto L_Sword;
        close;
    }

    //closedialog;
    //goodbye;
    close;

L_Sword:
    menu
        l("The children are safe, could you rent me your sword?"), L_Rent,
        l("You're welcome!"), -;
    close;

L_Rent:
    // This code is equivalent to: speech S_FIRST_BLANK_LINE | S_LAST_NEXT
    mes "";
    mesn;
    mesq l("Ah, I guess you want to fight at the cave north of me...");
    next;
    mesq l("The children are safe, aren't they...?");
    next;
    mesq l("I can rent it to you for 300 GP, during 15 minutes. Deal?");
    next;
    menu
        rif(Zeny >= 300 && gettimetick(2) > .RENT_TIME, l("Deal!")), L_DoRent,
        l("Maybe later."), -;
    close;

L_DoRent:
    if (gettimetick(2) <= .RENT_TIME) mesq l("Sorry, you was taking too long to decide and I've rented my sword to somebody else.");
    if (gettimetick(2) <= .RENT_TIME) close;
    .RENT_TIME=gettimetick(2)+(15*60);
    set Zeny, Zeny - 300;
    rentitem Judgement, (15*60);
    mes "";
    mesn;
    mesq l("Here it is. Take care with it!");
    close;




L_MKControl:
    mesn;
    mes l("Oh noes! You've found the Candor control panel!");
    menu
        l("Initiate small siege (lv. 5)"), L_MKSmall,
        l("Abort"), -;
    close;

L_MKSmall:
    addmapmask "005-1", MASK_MATTACK;
    changemusic "005-1", "mythica.ogg";
    disablenpc("Mana Stone");
    pvpon("005-1");
    announce(l("##1WARNING! WARNING! Siege starting at Candor!!"), bc_all);
    areamonster("005-1", 0, 0, 125, 125, l("Black Scorpion"), BlackScorpion, 1, "Ayasha::OnBlackScorpionDeath");
    areamonster("005-1", 0, 0, 125, 125, ("House Maggot"), HouseMaggot, 3, "Ayasha::OnHouseMaggotDeath");
    initnpctimer;
    close;


OnBlackScorpionDeath:
    dispbottom "BSD";
    areamonster("005-1", 0, 0, 125, 125, l("Black Scorpion"), BlackScorpion, 1, "Ayasha::OnBlackScorpionDeath");
    debugmes "BSD OK";
    end;

OnManaGhostDeath:
    dispbottom "MGD";
    areamonster("005-1", 0, 0, 125, 125, l("Mana Ghost"), ManaGhost, 1, "Ayasha::OnManaGhostDeath");
    debugmes "MGD OK";
    end;

OnHouseMaggotDeath:
    dispbottom "HMD";
    areamonster("005-1", 0, 0, 125, 125, l("House Maggot"), ManaGhost, 1, "Ayasha::OnHouseMaggotDeath");
    debugmes "HMD OK";
    end;


OnTimer5000:
    areamonster("005-1", 0, 0, 125, 125, "Black Scorpion", BlackScorpion, 1, "Ayasha::OnBlackScorpionDeath");
    areamonster("005-1", 0, 0, 125, 125, ("House Maggot"), HouseMaggot, 10, "Ayasha::OnHouseMaggotDeath");
    mapannounce("005-1", "##2Message to all Candor NPCs: Take shelter!", bc_map);
    disablenpc "Ayasha";
    disablenpc "Charda";
    disablenpc "Faris";
    disablenpc "Ghada";
    disablenpc "Latif";
    disablenpc "Rasin";
    disablenpc "Lilly";
    disablenpc "Zegas";
    disablenpc "Vincent";
    disablenpc "Liana";
    disablenpc "Maya";
    disablenpc "Sailors#005-1";
    disablenpc "Magic Barrier";
    disablenpc "Tolchi";
    disablenpc "Rosen";
    disablenpc "Nylo";
    disablenpc "Cynric";
    disablenpc "Morgan";
    disablenpc "Zitoni";
    disablenpc "Trainer";
    end;

OnTimer60000:
    areamonster("005-1", 0, 0, 125, 125, "Black Scorpion", BlackScorpion, 1, "Ayasha::OnBlackScorpionDeath");
    areamonster("005-1", 0, 0, 125, 125, ("Mana Ghost"), ManaGhost, 3, "Ayasha::OnManaGhostDeath");
    areamonster("005-1", 0, 0, 125, 125, ("House Maggot"), HouseMaggot, 5, "Ayasha::OnHouseMaggotDeath");
    end;

OnTimer120000:
OnTimer180000:
OnTimer240000:
OnTimer300000:
OnTimer360000:
OnTimer420000:
OnTimer480000:
    areamonster("005-1", 0, 0, 125, 125, ("Slime Blast"), 1090, 1);
    areamonster("005-1", 0, 0, 125, 125, ("Mana Ghost"), ManaGhost, 1, "Ayasha::OnManaGhostDeath");
    areamonster("005-1", 0, 0, 125, 125, ("House Maggot"), HouseMaggot, 1, "Ayasha::OnHouseMaggotDeath");
    end;

OnTimer540000:
    mapannounce("005-1", "##1The Monster Army is getting tired of resistance!", bc_map);
    areamonster("005-1", 0, 0, 125, 125, ("Slime Blast"), 1090, 5);
    areamonster("005-1", 0, 0, 125, 125, ("Mana Ghost"), ManaGhost, 1, "Ayasha::OnManaGhostDeath");
    areamonster("005-1", 0, 0, 125, 125, ("House Maggot"), HouseMaggot, 1, "Ayasha::OnHouseMaggotDeath");
    end;

OnTimer600000:
    mapannounce("005-1", "##1The Monster King army is preparing to withdraw!", bc_map);
    end;

OnTimer630000:
    removemapmask "005-1", MASK_MATTACK;
    changemusic "005-1", "bartk_adventure.ogg";
    enablenpc("Mana Stone");
    killmonsterall("005-1", 0);
    pvpoff("005-1");
    announce(("Candor siege is over!"), bc_all);
    enablenpc "Ayasha";
    enablenpc "Charda";
    enablenpc "Faris";
    enablenpc "Ghada";
    enablenpc "Latif";
    enablenpc "Rasin";
    enablenpc "Lilly";
    enablenpc "Zegas";
    enablenpc "Vincent";
    enablenpc "Liana";
    enablenpc "Maya";
    enablenpc "Sailors#005-1";
    enablenpc "Magic Barrier";
    enablenpc "Tolchi";
    enablenpc "Rosen";
    enablenpc "Nylo";
    enablenpc "Cynric";
    enablenpc "Morgan";
    enablenpc "Zitoni";
    enablenpc "Trainer";
    stopnpctimer;
    end;

OnInit:
    .sex = G_FEMALE;
    .distance = 5;
    .RENT_TIME=0; // TODO: This could easily be a $GLOBAL_VARIABLE
    end;
}


function	script	CheckEnfant	{
    @kids_count = 0;
    @count_tmp = 0;
    if (debug)
        npctalk3 l("Hello, I am K-@@, of the @@ order.", @kids, $@KidsBits[@kids]);
    if (STARTAREA & $@KidsBits[@kids])
        goto L_Already;
    STARTAREA = STARTAREA | $@KidsBits[@kids];

    goto L_Loop;

L_KidsTally:
    if (debug)
        npctalk3 l("You found @@ out of @@ kids.", @kids_count, @count_tmp);
    if (@kids_count == 6)
    {
        message strcharinfo(0), "That must have been the last kid.";
        setq CandorQuest_HAS, 3;
    }
    speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
        l("Oh! You found me. Good job!");
        narrator("You receive 100 exp.");
    getexp 100,0;
    close;
    return;

L_Loop:
    while (@count_tmp < 6) {
        if (STARTAREA & $@KidsBits[@count_tmp])
            @kids_count = (@kids_count + 1);
        @count_tmp = (@count_tmp + 1);
    }
    goto L_KidsTally;

L_Already:
    mesn;
    mesq l("Thanks for playing with us! Can you find my friends?");
    close;

    return;
}
005-1,76,100,0	script	Charda	NPC_CHILD8,{
    @kids = 0;
    if (getq(CandorQuest_HAS) == 2)
    {
        CheckEnfant();
    } else {
        mes l("The kid is not paying attention to you.");
        close;
    }
    end;
OnInit:
    // This works at same var from KidsBits. We start counting from 10, then.
    setarray $@KidsBits, (1 << 10), (1 << 11), (1 << 12), (1 << 13), (1 << 14), (1 << 15);
    .sex = G_OTHER;
    .distance = 1;
    end;
}
005-1,28,100,0	script	Faris	NPC_CHILD7,{
    @kids = 1;
    if (getq(CandorQuest_HAS) == 2)
    {
        CheckEnfant();
    } else {
        mes l("The kid is not paying attention to you.");
        close;
    }
    end;
OnInit:
    .sex = G_MALE;
    .distance = 1;
    end;
}
005-1,91,31,0	script	Ghada	NPC_CHILD5,{
    @kids = 2;
    if (getq(CandorQuest_HAS) == 2)
    {
        CheckEnfant();
    } else {
        mes l("The kid is not paying attention to you.");
        close;
    }
    end;
OnInit:
    .sex = G_MALE;
    .distance = 1;
    end;
}
005-1,45,68,0	script	Latif	NPC_HUMAN_M_ARTIS,{
    @kids = 3;
    if (getq(CandorQuest_HAS) == 2)
    {
        CheckEnfant();
    } else {
        mes l("The kid is not paying attention to you.");
        close;
    }
    end;
OnInit:
    .sex = G_MALE;
    .distance = 1;
    end;
}
005-1,25,53,0	script	Rasin	NPC_CHILD3,{
    @kids = 4;
    if (getq(CandorQuest_HAS) == 2)
    {
        CheckEnfant();
    } else {
        mes l("The kid is not paying attention to you.");
        close;
    }
    end;
OnInit:
    .sex = G_MALE;
    .distance = 1;
    end;
}
005-1,44,74,0	script	Lilly	NPC_CHILD6,{
    @kids = 5;
    if (getq(CandorQuest_HAS) == 2)
    {
        CheckEnfant();
    } else {
        mes l("The kid is not paying attention to you.");
        close;
    }
    end;
OnInit:
    .sex = G_MALE;
    .distance = 1;
    end;
}

005-1,60,91,0	script	AyashaDebug	NPC_MONA,{
    showavatar NPC_MONA;  // this is handled by avatars.xml
    mesn;
    mesq l("Reset?");
    next;
    menu
        l("Yes."),  L_Reset,
        l("Test"),  L_Charda,

        l("No."),   L_Close;

L_Reset:
    setq CandorQuest_HAS, 0;
    STARTAREA = STARTAREA &~ (1 << 10);
    STARTAREA = STARTAREA &~ (1 << 11);
    STARTAREA = STARTAREA &~ (1 << 12);
    STARTAREA = STARTAREA &~ (1 << 13);
    STARTAREA = STARTAREA &~ (1 << 14);
    STARTAREA = STARTAREA &~ (1 << 15);
    mes l("Reset!");
    goto L_Close;

L_Charda:
    STARTAREA = STARTAREA &~ (1 << 10);
    mes l("Charda clean!");
    goto L_Close;

L_Close:
    //showavatar;  // Use this to hide the showavatar
    close;

// Use @shownpc to enable AyashaDebug
OnInit:
    if (!debug)
        disablenpc .name$;
    end;
}
