// TMW2 scripts.
// Author:
//    Saulc
//    GonzoDark
//    Jesusalva
// Variables:
//    0    CandorQuest_Maya
// Values:
//    00   Default, no quest selected.
//    01   First quest accepted: Need 3 cotton cloth and 3 maggot slime
//    02   First quest completed: Reward Candor shirt
//    03   Second quest accepted: Need 3 ScorpionStinger and 10 Piou Feathers
//    04   Second quest completed: Reward 700 GP (precise calculation)
//    05   wolvern tooth +20k + 2000monster point ->claw pendant

005-1,49,47,0	script	Maya	NPC_RAIJIN_FEMALE_LEGION_ARTIS,{
    .@maya = getq(CandorQuest_Maya);

    if (.@maya == 0)
        goto L_QuestNotStarted;
    if (.@maya == 1)
        goto L_QuestAccepted;
    if (.@maya == 2)
        goto L_Quest2;
    if (.@maya == 3)
        goto L_Quest2Accepted;
    if (.@maya == 4)
        goto L_NextQuestPending;

L_QuestNotStarted:
    mesn;
    mesq l("Hi there, I can always use a helping hand around here, are you the one for the job?");
    mes "";
    menu
        l("Sure"),L_Next,
        l("No, thanks."),L_Close;

L_Next:
    mes "";
    mesn;
    mesq l("Good! First, let us test if you are resourceful. Bring me 3 @@ and 3 @@. That should be enough!", getitemlink(CottonCloth), getitemlink(MaggotSlime));
    setq CandorQuest_Maya, 1;
    close;

L_QuestAccepted:
    mesn;
    mesq l("I see you have brought @@/3 @@ and @@/3 @@ for me",countitem(CottonCloth),getitemlink(CottonCloth),countitem(MaggotSlime),getitemlink(MaggotSlime));
    mes "";
    menu
        rif(countitem(CottonCloth) >= 3 && countitem(MaggotSlime) >= 3, l("Here they are!")), L_QuestCompleted,
        rif(countitem(CottonCloth) < 3 || countitem(MaggotSlime) < 3, l("Oh, then I don't have enough! I'll bring more later!")), L_GetHelp1,
        l("Can we get back to that later?"), -;
    close;

L_GetHelp1:
    mes "";
    mesc l("Protip: You can get @@ from shops. Cotton is rumored to be magical, keep this is mind.", getitemlink(CottonCloth));
    mesc l("Protip 2: If you fell stuck, ask at #world, even if nobody is online. Who knows, someone on Discord or IRC might reply!");
    next;
    goto L_Close;

L_QuestCompleted:
    delitem CottonCloth, 3;
    delitem MaggotSlime, 3;
    set Zeny, Zeny + 725;
    getitem CandorShirt, 1;
    getexp BaseLevel*8, 5;
    setq CandorQuest_Maya, 2;

    mes "";
    mesn;
    mesq l("Thanks for the help. Here, take this shirt and some money.");
    close;




L_Quest2:
    mesn;
    mesq l("Thanks again for the help. You have proven that you are resourceful.");
    next;
    if (BaseLevel < 7) mesn;
    if (BaseLevel < 7) mesq l("But maybe you should help other people and get some levels before returning to me.");
    if (BaseLevel < 7) close;
    mesn;
    mesq l("As always, I can use a helping hand around here. Interested?");
    mes "";
    menu
        l("Sure"),-,
        l("No, thanks."),L_Close;

    mes "";
    mesn;
    mesq l("Good! I want 3 @@ and 10 @@. I have a contract to transform that in good money.",
           getitemlink(ScorpionStinger), getitemlink(PiouFeathers));
    setq CandorQuest_Maya, 3;
    close;

L_Quest2Accepted:
    mesn;
    mesq l("I see you have brought @@/3 @@ and @@/10 @@ for me.",
            countitem(ScorpionStinger),getitemlink(ScorpionStinger),
            countitem(PiouFeathers),getitemlink(PiouFeathers));
    mes "";
    menu
        rif(countitem(ScorpionStinger) >= 3 && countitem(PiouFeathers) >= 10, l("Here they are!")), L_Quest2Completed,
        rif(countitem(ScorpionStinger) < 3 || countitem(PiouFeathers) < 10, l("Oh, then I don't have enough! I'll bring more later!")), L_Close,
        l("Can we get back to that later?"), -;
    close;

L_Quest2Completed:
    delitem ScorpionStinger, 3;
    delitem PiouFeathers, 10;
    set Zeny, Zeny + 925;
    getexp BaseLevel*10, 10;
    setq CandorQuest_Maya, 4;

    // Reward Calculation: Piou base is 3 and Stinger base is 25.
    // Maya will pay in a 1.5x factor + 300 GP she owed you + a small bonus to round things
    // (3*3*1.5)+(25*10*1.5) = roughly 389 GP + 300 + bonus(11) = 700

    mes "";
    mesn;
    mesq l("Thanks for the help! If you help people, they'll start trusting you. Once they trust you, they'll give you quests which are very important to them;");
    next;
    mesn;
    mesq l("And once they entrust you with what is important for them, they'll pay better. Here is 700 GP. Come back later.");
    close;


L_NextQuestPending:
    mesn;
    mesq l("Thanks again for the help. You have proven that you are resourceful. Come back again later.");
    close;

L_Close:
    closedialog;
    goodbye;
    close;


OnInit:
    .sex = G_FEMALE;
    .distance = 4;
    end;
}
