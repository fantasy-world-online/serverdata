// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 004-2-1: Canyon Caves mobs
004-2-1,55,58,29,22	monster	Cave Snake	1035,12,35000,150000
004-2-1,70,40,3,1	monster	Small Topaz Bif	1101,1,35000,150000
004-2-1,54,54,54,54	monster	Cave Maggot	1027,20,40000,200000
004-2-1,43,66,9,6	monster	Snake	1122,3,35000,150000
004-2-1,46,40,4,4	monster	Snake	1122,3,35000,150000
004-2-1,84,56,9,9	monster	Snake	1122,3,35000,150000
004-2-1,60,46,29,22	monster	Cave Maggot	1027,6,35000,150000
004-2-1,55,48,29,22	monster	Small Topaz Bif	1101,3,35000,150000
004-2-1,80,53,12,25	monster	Chagashroom Field	1128,3,35000,150000
