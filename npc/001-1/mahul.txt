// TMW-2 Script
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Mahul is the Aeros Well Master

001-1,299,45,0	script	Mahul	NPC_PLAYER,{
    mesn;
    mesq l("Uhul! My name is Mahul!");
    mes l("I can fill your bottle with water for only @@ gp the bottle.", .COST_PER_BOTTLE);
    mes l("After all, I am the Well Master!");
    input .@count;

    if (.@count == 0)
        close;
    .@Cost = .@count * .COST_PER_BOTTLE;
    .@empty = countitem(EmptyBottle);

    if (.@empty < .@count)
        goto L_NotEnoughBottles;
    if (Zeny < .@Cost)
        goto L_NotEnoughMoney;
    getinventorylist;
    inventoryplace BottleOfDivineWater, .@count;

    Zeny=Zeny-.@Cost;
    delitem EmptyBottle, .@count;
    getitem BottleOfDivineWater, .@count;
    close;

L_NotEnoughBottles:
    mes "";
    mesn;
    mes l("You don't have that many empty bottles!");
    close;

L_NotEnoughMoney:
    mes "";
    mesn;
    mes l("You don't have enough gold! You need @@ gp.", .@Cost);
    close;

OnInit:
    .COST_PER_BOTTLE = 100;
    .sex = G_MALE;
    .distance = 7;

    .@npcId = getnpcid(.name$);
    // Check items.xml for info about this
    setunitdata(.@npcId, UDT_HEADTOP, InfantryHelmet);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SailorShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, RaidTrousers);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 7);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 17);

    end;
}
