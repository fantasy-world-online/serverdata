// TMW2 scripts.
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Water animations, splash, fishes, etc...

001-1,254,70,0	script	#water_animation_aeros0	NPC_WATER_SPLASH,{

    fishing(3, CommonCarp, Roach, Tench,
               GrassCarp); // begin or continue fishing
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}

001-1,250,72,0	duplicate(#water_animation_aeros0)	#water_animation_aeros1	NPC_WATER_SPLASH
001-1,254,76,0	duplicate(#water_animation_aeros0)	#water_animation_aeros2	NPC_WATER_SPLASH
001-1,247,77,0	duplicate(#water_animation_aeros0)	#water_animation_aeros3	NPC_WATER_SPLASH

001-1,105,112,0	duplicate(#water_animation_aeros0)	#water_animation_aeros4	NPC_WATER_SPLASH
001-1,99,112,0	duplicate(#water_animation_aeros0)	#water_animation_aeros5	NPC_WATER_SPLASH
001-1,94,110,0	duplicate(#water_animation_aeros0)	#water_animation_aeros6	NPC_WATER_SPLASH
001-1,90,112,0	duplicate(#water_animation_aeros0)	#water_animation_aeros7	NPC_WATER_SPLASH


