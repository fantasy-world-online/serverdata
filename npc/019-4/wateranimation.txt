// TMW2 scripts.
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Water animations, splash, fishes, etc...

019-4,144,53,0	script	#fishing_winterlands0	NPC_WATER_SPLASH,{

    fishing(1, CommonCarp,
               FrozenYetiTear, IceCube, GrassCarp); // begin or continue fishing
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}

019-4,146,32,0	duplicate(#fishing_winterlands0)	#fishing_winterlands1	NPC_WATER_SPLASH
019-4,115,71,0	duplicate(#fishing_winterlands0)	#fishing_winterlands2	NPC_WATER_SPLASH

019-1,114,104,0	duplicate(#fishing_winterlands0)	#fishing_winterlands3	NPC_WATER_SPLASH
019-1,121,78,0	duplicate(#fishing_winterlands0)	#fishing_winterlands4	NPC_WATER_SPLASH

019-2,103,119,0	script	#fishing_winterlandsb	NPC_WATER_SPLASH,{

    fishing; // begin or continue fishing
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}

019-2,103,103,0	duplicate(#fishing_winterlandsb)	#fishing_winterlands5	NPC_WATER_SPLASH
019-2,109,74,0	duplicate(#fishing_winterlandsb)	#fishing_winterlands6	NPC_WATER_SPLASH
019-2,109,44,0	duplicate(#fishing_winterlandsb)	#fishing_winterlands7	NPC_WATER_SPLASH


