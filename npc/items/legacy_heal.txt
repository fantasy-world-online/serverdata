// TMW-2 Script.
// Evol scripts.
// Author:
//    Reid
//    Jesusalva
// Description:
//    Legacy Healing System
//
// Variables:
//    @delay    Second of healing
//    @min      Min amount of healing
//    @max      Max amount of healing
//
// *getequipoption(EQI_HEAD_TOP,1,168); → Heal Bonus (should be first bonus on Chef Hat)


-	script	legacy_heal	-1,{

OnUse:
    if (@delay <= 0) {
        Exception("Invalid legacy healing item, deleting without healing effect.");
        end;
    }

    // minimum between @min and bVit / 2 * BaseLevel / 10
    @min = min(@max, readparam(bVit) * BaseLevel / 20);
    @val1 = rand(@min, @max);

    @delay *= 1000; // Put the delay in ms

    // We now have @val1 (new effect), @delay (new delay)
    // But do we have .@v and .@d (old effect and delay)?
    .@v=getstatus(getarg(0), 1);
    .@d=getstatus(getarg(0), 4) * 1000;

    // If there WAS an effect previously, get ponderate average
    if (.@v > 0) {
        @val1=ponderate_avg(@val1, @delay, .@v, .@d);
        @delay=ponderate_avg(@delay, @val1, .@d, .@v);
    }

    // Decide the healing bonus type. We have four types: S, L, G and M
    // By default, we use 'S'
    .@skill = SC_S_LIFEPOTION;

    // Apply the effect and finish
    sc_end .@skill;
    sc_start2 .@skill, @delay, @val1, 1;

    @delay=0;
    @min=0;
    @max=0;
    @val1=0;
    end;
}
