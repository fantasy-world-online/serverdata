// TMW2 scripts.
// Authors:
//    Pyndragon
//    Jesusalva
// Description:
//    Hand Teleporter (also saves coordinates - @memo)

-	script	LoF Teleporter	NPC_HIDDEN,{
    close;

    // Checks if you can warp
    function loftel_check {
        getmapxy(.@m$, .@x, .@y, 0);
        .@is_hurt=(readparam(Hp) < readparam(MaxHp)*9/10); // <90% hp
        .@is_town=(getmapflag(.@m$, mf_town));
        return (.@is_hurt && !.@is_town);
    }

    // Calculate time remaining
    // (time, .@x)
    function loftel_time {
        return gettimetick(2)+max((60*getarg(0))-(getarg(1)*60), 30);
    }

L_Cooldown:
    mesn;
    mesc l("This teleporter is currently recharging.");
    mesc l("You can use it again in @@.", FuzzyTime(TELEPORTER_TIME));
    close;

OnUse:
    if (TELEPORTER_TIME > gettimetick(2))
        goto L_Cooldown;
    if (readparam(Hp) < readparam(MaxHp)) {
        dispbottom l("You are hurt, and cannot use this.");
        end;
    }
    if (BaseLevel < 20) {
        dispbottom l("This is too powerful to you. Get level 20 before attempting to use.");
        end;
    }

    mesn;
    mesc l("Ozthokk, a great sage from the Land Of Fire, holds secrets of time and space travel.");
    mesc l("This is not magic, it is science!");
    mes "";
    mesc l("PS. Additional reagents may be required for warps.");
    next;

    .@x=(reputation("LoF")/10)+min(10, countitem(TimeFlask)-1); // up to 10 minutes reduction from quests, and 10 from time flasks

    select
        l("Don't warp"),
        rif(TELEPORTERS & TP_FROST, l("Frostia (@@m)", 120-.@x)),
        rif(TELEPORTERS & TP_HALIN, l("Halinarzo (@@m)", 120-.@x)),
        rif(GSET_SOULMENHIR_MANUAL, l("Save Point (@@m)", 20-.@x));

    if (@menu == 1)
        close;

    switch (@menu) {
        case 1:
    }
    if (loftel_check()) {
        dispbottom l("You are hurt, and cannot use this.");
    }
	    doevent "shake::OnGM";
    switch (@menu) {
        case 2:
            warp "024-1", 155, 82;
            TELEPORTER_TIME=loftel_time(120, .@x);
            LOCATION$="Frostia";
            break;
        case 3:
            warp "009-1", 113, 91;
            TELEPORTER_TIME=loftel_time(120, .@x);
            LOCATION$="Halin";
            break;
        case 4:
            warp "Save", 0, 0;
            TELEPORTER_TIME=loftel_time(20, .@x);
            //LOCATION$="Save";
            break;
    }
    closedialog;
    end;
}
