// TMW-2 Script.
// Author:
//    Crush
//    Jesusalva
// Description:
//    Alcohol effects
//    TODO: Retroactive, weakens every hour...
//
// Variables:
//    @taste    Alcohol taste (0~100) - influences exp up
//    @Alcohol   Alcoholic rating (0~100) - influences Attack Speed Malus, Min. Vit and duration
//    ALC_DELAYTIME For how long you are drunk (the delay) - gettimetick(2)
//    ALC_THRESHOLD How drunk you are (the bonus)
//
//    When drunk, attack speed is lowered but exp gain is increased.
//    Attack Speed Reductor: SC_ATTHASTE_INFINITY
//    Max HP Reductor: SC_INCMHPRATE
//    EXP Increaser: SC_OVERLAPEXPUP

-	script	alcohol_sc	-1,{

    // Stack remaning bonuses if the last one hasn't finished
    // remaining_bonuses(sc, type)
    // type 0: delay
    // type 1: value
    function remaining_bonus
    {
        if (getstatus(getarg(0)))
        {
            if (getarg(1))
                return getstatus(getarg(0), 1);
            else
                return getstatus(getarg(0), 5); // Shouldn't it be 5?
        }
        return 0;
    }

OnUse:
    if (@Alcohol <= 0) close;
    // Do you have enough vitality to hold your beer?
    .@vit=readparam(bVit);
    if (@Alcohol+ALC_THRESHOLD > .@vit) {
        dispbottom l("You vomit, you are too drunk for this to have effect anymore.");
        dispbottom l("Raise vitality to be able to drink even more.");
		sc_start SC_CONFUSION, 5000, 0, 10000, SCFLAG_NOAVOID; // Warning, forces user to use @resync!
        end;
    }
    .@deltatime=60*1000; // How long (in ms) each Alcohol point works? (max. 100 points)
    // Default value is 1 minute per alcohol point - you'll be somber after at most two hours.

    // Taste is affected by users near you.
    // Each user raises exp bonus in 1%, capped to twice the beverage taste
    // If you are with many people, drink a better beverage! ;-)
    getmapxy(.@m$, .@x, .@y, 0);
    .@bonus=getareausers(.@m$, .@x-10, .@y-10, .@x+10, .@y+10)-1;
    @taste+=min(@taste*2, .@bonus);

    // Alcohol EXP Bonus - ponderate average, so having more VIT doesn't means
    // more experience - only more time (be careful when mixing alcohol!)
    .@v=remaining_bonus(SC_OVERLAPEXPUP, true);
    .@t=remaining_bonus(SC_OVERLAPEXPUP, false)/1000;

    if (.@t) .@val1 = ponderate_avg(@taste, @Alcohol, .@v, .@t);
    else .@val1 = @taste;

    // Put the delay in ms.
    .@delay = remaining_bonus(SC_OVERLAPEXPUP, false);
    .@delay = .@t + @Alcohol*.@deltatime;

    // Reset EXP Bonus based on the new cumulative delay and average exp bonus
    sc_end SC_OVERLAPEXPUP;
    sc_start SC_OVERLAPEXPUP, .@delay, .@val1;

    // Recalculate Alcohol Threshold and time
    ALC_THRESHOLD+=@Alcohol;
    if (ALC_DELAYTIME < gettimetick(2))
        ALC_DELAYTIME=gettimetick(2);
    ALC_DELAYTIME+=@Alcohol*.@deltatime;

    // Debug comment if you need to check stuff
    //debugmes "%d %d | %d %d | f t ", remaining_bonus(SC_OVERLAPEXPUP, false), remaining_bonus(SC_OVERLAPEXPUP, true), remaining_bonus(SC_ATTHASTE_INFINITY, false), remaining_bonus(SC_ATTHASTE_INFINITY, true);

    // For debuff I'll use inc_sc_bonus utilities (exp gain = atk speed loss)
    @min=-(remaining_bonus(SC_OVERLAPEXPUP, true)*2);
    @max=-(remaining_bonus(SC_OVERLAPEXPUP, true)*2);
    @type=SC_ATTHASTE_INFINITY;
    @delay=@Alcohol*(.@deltatime/1000);
	doevent "inc_sc_bonus::OnUse";
    close;
}

