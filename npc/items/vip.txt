// FWO Script
// ---------------------------------------
// Author: Jesusalva
// ---------------------------------------
// Title: Premium/ViP Services System

// An imprecise/unreliable way to check if you are still a Very Important Player
// It also corrects GM group, granting access to sponsor-only area in Hurnscald Inn
// and Tulimshar Magic Council.
function	script	checkPremium	{
	if (#PREMIUM) {
		if (#PREMIUM >= gettimeparam(GETTIME_DAYOFMONTH)) {
			// Report how much time left
			if (#PREMIUM == gettimeparam(GETTIME_DAYOFMONTH))
				dispbottom l("Your VIP ticket will expire today.");
			else
				dispbottom l("Your VIP ticket will expire in @@ day(s).", #PREMIUM-gettimeparam(GETTIME_DAYOFMONTH));

			// Adjust GM group
			if (!getgmlevel())
				atcommand "@adjgroup 1 "+strcharinfo(0);

			// Check if your premium is still running every hour
			addtimer2(3600000, "Malindou::OnCheckPremium");
		} else {
			// Premium expired, clear variables
			dispbottom l("Your VIP ticket expired.");
			#PREMIUM=false;
			if (getgmlevel() == 1)
				atcommand "@adjgroup 0 "+strcharinfo(0);
		}
	}
	return;
}

// getPremium(Boost, Days)
function	script	getPremium	{
        if ((!getarg(0,0)) || !getarg(1,0)) {
                debugmes "getPremium - bad setting";
                end;
        }

        #PREMIUM=gettimeparam(GETTIME_DAYOFMONTH)+getarg(1);
		SC_Bonus(getarg(1)*86400, SC_CASH_PLUSEXP, getarg(0));
		SC_Bonus(getarg(1)*86400, SC_CASH_RECEIVEITEM, getarg(0));
		//SC_Bonus(getarg(1)*86400, SC_CASH_PLUSONLYJOBEXP, getarg(0));
		checkPremium();
        return;
}

