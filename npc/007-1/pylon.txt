// TMW2 scripts.
// TMW2 Authors:
//      Saulc
//      Jesusalva
// Description:
//      Miner Pylon transforme gems into gem powder, he ask for 100gp + 1 gem and give 100 EXp (each time) + 1 to 3 gem powder's.
// Variable:
//      dont need variable
// Reference:
//     https://fr.wikipedia.org/wiki/Pilon#Objets not Epiphysis !

007-1,47,44,0	script	Pylon	NPC_PLAYER,{

    mesn;
    mesq l("Hello, I'm Pylon!");
    next;
    mesq l("I work in this mine since 3 years, I'm an expert on gems: Ruby, Topaz, Sapphire, You know!");
    next;
    mesq l("If you have some gems, I can transform them into powder.");
    next;
    mesq l("I only take a tax of 100 gp per gem.");
    if (Zeny < 100)
        close;
    next;
    mesq lg("Then, would you like me to transform one of your gems?");
    next;
    goto L_Menu;

L_Menu:
    mesn strcharinfo(0);
    // We could add a drag zone, and allow players to drag their gems, but... Meh.
    select
        l("Hum, how many gem powders can I get from one gem?"),
        rif(countitem(Diamond) >= 1 && Zeny >= 100, l("Yeah sure, take my Diamond!")),
        rif(countitem(Ruby) >= 1 && Zeny >= 100, l("Yeah sure, take my Ruby!")),
        rif(countitem(Emerald) >= 1 && Zeny >= 100, l("Yeah sure, take my Emerald!")),
        rif(countitem(Sapphire) >= 1 && Zeny >= 100, l("Yeah sure, take my Sapphire!")),
        rif(countitem(Topaz) >= 1 && Zeny >= 100, l("Yeah sure, take my Topaz!")),
        rif(countitem(Amethyst) >= 1 && Zeny >= 100, l("Yeah sure, take my Amethyst!")),
        rif(countitem(IronOre) >= 1 && Zeny >= 100, l("And what's about Iron Ore?")),
        l("No, thanks, I will keep my gems.");

    mes "";

    switch (@menu) {
    case 1:
        goto L_Question;
        break;
    case 8:
        goto L_Ore;
        break;
    case 9:
        close;
        break;
    default:
        goto L_Powder;
        break;
    }

L_Question:
    mesn;
    mesq lg("That depends on your luck!");
    next;
    mesq l("With one gem you can expect to get 1 to 3 powders!");
    next;
    mesc l("We must blame Saulc!");
    next;
    mesq lg("By the way would you like to transform your gem?");
    next;
    goto L_Menu;

L_Ore:
    mesn;
    mesq l("Hum... I'm not really good at it.");
    next;
    mesn;
    mesq l("But I can try my best for you!");
    next;
    mesn;
    mesq l("If you want it?");
    next;
    goto L_MenuOre;

L_MenuOre:
    menu
        rif(countitem(IronOre) >= 1 && Zeny >= 100, l("Make me an Iron Powder.")), L_OreOk,
        l("Nah, thank you."), L_Close;

L_OreOk:
    // Amount iron ore
    .@amo=rand(1,2);

    delitem IronOre, 1;
    Zeny = Zeny - 100;
    getexp 10, 0;
    inventoryplace IronPowder, .@amo;
    getitem IronPowder, .@amo;
    mes "";
    mesn;
    mesq l("Here you go, I tried my best! Do you want another?");
    next;
    goto L_MenuOre;


// Must rework IDs
L_Powder:
    // Magic
    .@id=Diamond+@menu-2;
    .@pw=DiamondPowder+@menu-2;

    // Amount
    .@am=rand(1,3);

    delitem .@id, 1;
    Zeny = Zeny - 100;
    getexp 60, 0;


    inventoryplace .@pw, .@am;
    getitem .@pw, .@am;

    mes "";
    mesn;
    mesq l("Here is your powder! I hope it will be useful.");
    next;
    mesq l("Would you like to transform one more?");
    next;
    goto L_Menu;


L_Close:
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, MinerHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, ArtisTankTop);
    setunitdata(.@npcId, UDT_HEADBOTTOM, BromenalPants);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 15);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);
    .sex = G_MALE;
    .distance = 4;
    end;
}
