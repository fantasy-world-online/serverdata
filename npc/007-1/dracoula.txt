// TMW2 Script
// Author:
//  Saulc
//  Jesusalva
// Description:
//      Dracoula is daily npc, ask for bat teeth every 23 h
// Variable:
//      MineQuest_Dracoula

007-1,170,99,0	script	Dracoula	NPC_UKAR_F,{
    mesn;
    mesq lg("Hello adventurer! Are you lost?");
    next;
    mesq l("This is not a place for non-experimented people!");
    next;
    mesq l("Mine exit is top left!");
    mes "";
    if (BaseLevel >= 15) goto L_Menu;
    close;

L_Menu:
    mesn;
    mesq l("But as you're here now, could you do me a favour?");
    mes "";
    menu
        l("Hum, Which type of favor?"),L_Quest,
        l("Can I find a mana source here?"),L_Mana,
        l("Can I become a miner?"),L_Miner,
        l("No, thanks. I gonna leave this place."),L_Close;

L_Quest:
    mes "";
    .@q=getq(MineQuest_Dracoula);
    if (!.@q) {
        mesn;
        mesq l("Nice! First let me introduce myself. I am Dracoula, a miner!");
        next;
        mesn;
        mesq l("I mine here since a while. My favorite activity it's to scare others miners!");
        next;
    }
    if (.@q == 0) goto L_Continue;
    .@q2=getq2(MineQuest_Dracoula) + 60 * 60 * 23;
    if (santime() >= .@q2) goto L_Repeat;
    mesn;
    mesq l("But come back in a few hours, I didn't lost all @@!", getitemlink(BatTeeth));
    close;

L_Continue:
    mesq l("I love seeing their terrorize face.");
    mes "";
    menu
        l("Ok. Cool life!"), L_Close,
        l("Haha, Nice, but how do you do that?"), L_Next;

L_Next:
    mes "";
    mesq l("I disguise myself into a giant mutated bat, but every time I break or lose my fake teeth.");
    next;
    mesq l("I usually ask adventurers for 1 @@, but new miners should be arriving soon. I need to make them cry!", getitemlink(BatTeeth));
    next;
    mesq l("So it could be nice, if you could bring me 1 @@,", getitemlink(BatTeeth));
    mes "";
    menu
        rif(countitem(BatTeeth) >= 1, l("Hey! I already got them!")), L_Finish,
        l("I'll get to it."), L_Close;
    close; // double sure

L_Repeat:
    mesn;
    if (getequipid(EQI_HEAD_TOP) > 0)
        mesq l("Oh it's you @@, I did not recognize you with your hat!", strcharinfo(0));
    else
        mesq l("Oh it's you @@, I did not recognize you without a hat!", strcharinfo(0));
    next;
    mesq l("Do you have an extra of 1 @@ for me?", getitemlink(BatTeeth));
    mes "";
    menu
        rif(countitem(BatTeeth) >= 1, l("Yep, I bring them for you!")), L_Finish2,
        l("Actually not."), L_Close;
    close;

// First Time Only
L_Finish:
    delitem BatTeeth, 1;
    getexp 5666, 5; // 20 / 18% = 111 kills * 15 xp = 1665 xp gained from killing. (40% bonus)
    Zeny = (Zeny + 540); // 3*20 = 60 base (400% bonus)
    setq MineQuest_Dracoula, 1, santime();
    mes "";
    mesn;
    mesq l("WAW thank you! Come back later to bring me extra @@!", getitemlink(BatTeeth));
    close;

// Repeat
L_Finish2:
    delitem BatTeeth, 1;
    getexp 275, 1; // 11 / 18% = 61 kills * 15 xp = 915 xp gained from killing. (30% bonus)
    Zeny = (Zeny + 165); // 3*11 = 33 base (x% bonus)
    setq MineQuest_Dracoula, 1, santime();
    mes "";
    mesn;
    mesq l("So COOL, thanks! Come back later to bring me extra @@!", getitemlink(BatTeeth));
    close;

L_Mana:
    mes "";
    mesn;
    mesq l("Ah! Actually nobody found one.");
    next;
    mesn;
    mesq l("But it's ultimate goal of miners there.");
    next;
    mesn;
    mesq l("If one of us found a Mana stone. They would become rich!");
    next;
    mesn;
    mesq l("Twelve times more if it is an elusive Mana Fragment no one knows where they are!");
    next;
    mesn;
    mesq l("That is.");
    next;
    goto L_Menu;

L_Miner:
    mes "";
    mesn;
    mesq l("You should ask Tycoon.");
    next;
    mesn;
    mesq l("He is the Miners leader.");
    next;
    goto L_Menu;

L_Close:
    closedialog;
    goodbye;
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, MinerHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, ArtisTankTop);
    setunitdata(.@npcId, UDT_HEADBOTTOM, JeansShorts);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 7);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 8);

    .sex = G_FEMALE;
    .distance = 4;

    end;
}

