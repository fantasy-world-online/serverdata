// TMW2 Script
// Author:
//    Ernando <ernando.quirino@hotmail.com> (Creator)
//    Jesusalva <admin@tmw2.org>
// Description:
//    Hunger Games™ version for The Mana World Brazil v2, created by Ernando Quirino.
//    Entrance to Hungry Quirin Arena
// Variables:
//      EQ - Ernando Quirino
//      $@EQ_TIMER
//          < gettimetick(2): Can be open!
//          > gettimetick(2): Registration open!
//          = gettimetick(2): Event start!
//      $@EQ_STATUS
//          0: Register open
//          1: Fight happening
//          2: Event finished

012-1,70,63,0	script	Quirin	NPC_ERNANDO,{
    if (is_admin())
            goto L_Hub;

    if ($@EQ_STATUS) goto L_NoSeason;
    if ($@EQ_STATUS == 0) {
        if ($@EQ_TIMER < gettimetick(2))
            goto L_Hub;
        else
            goto L_Register;
    }

    npctalk3 l("Bug, report me!");
    end;

L_NoSeason:
    // If it finished, it can be done again in one hour
    if ($@EQ_STATUS == 2) {
        if ($@EQ_TIMER+3600 < gettimetick(2)) {
            $@EQ_STATUS=0;
            npctalkonce l("A sec... And... Done! I just finished cleaning it up!");
        } else {
            npctalkonce l("I'm currently cleaning the arena, wait just @@ more.", FuzzyTime($@EQ_TIMER+3600));
        }
    } else {
        npctalkonce l("I'm currently hosting a fight.");
    }
    end;

L_Register:
    mesn;
    mesq l("Hello player, do you want to participate on HUNGRY QUIRIN event?!");
    if ($@EQ_TIMER+180 < gettimetick(2))
        mesc l("Event will start in @@", FuzzyTime($@EQ_TIMER));
    else
        mesc l("Event will start in @@", FuzzyTime($@EQ_TIMER)), 1;
    next;
    select
        l("Yeah, sign me up!"),
        l("No, not at the moment."),
        l("Information");

    switch (@menu) {
    case 1:
        goto L_SignUp;
    case 2:
        close;
    case 3:
        goto L_Info;
    }

L_Info:
    mesn col(l("Hungry Quirin Arena Rules"), 3);
    mesc l("1- You must not be carrying anything with you.");
    mesc l("2- You must not use a cart. If you do, YOU WILL BE SEVERELY PENALIZED.");
    mesc l("3- All items from the Arena are from the Arena. You won't carry any of them back with you.");
    mesc l("4- Experience and Gold earned during this event can be kept.");
    next;
    mesn col(l("Hungry Quirin Arena Information"), 3);
    mesc l("1- Survive. If you die, you will gain nothing. And people want to kill you.");
    mesc l("2- Take everything you can find. You'll be warped without equip or healing items! Kill monsters to get some stuff too!");
    mesc l("3- Trust nobody. There can be only one winner, and it must be you.");
    mesc l("4- Take Care. Wildlife can kill you too. There can be traps.");
    mesc l("5- Trust yourself. You will lose the moment you enter in panic. This arena is not for the weak-willed!");
    next;
    goto L_Register;

L_SignUp:
    // Player cannot be carrying anything
	getinventorylist;
	if(@inventorylist_count>=1) goto L_Full;
    getcartinventorylist();
	if(@cartinventorylist_count>=1) goto L_Full;

    // Warp player
    if (rand2(1,2) == 1)
        warp "001-8", rand(42, 57), 42;
    else
        warp "001-8", rand(42, 57), 57;

    // Prevent further movements!
    setpcblock(PCBLOCK_ATTACK|PCBLOCK_SKILL|PCBLOCK_USEITEM|PCBLOCK_MOVE|PCBLOCK_COMMANDS, true);
    //dispbottom l("Stay ready!");
    dispbottom l("##1DON'T MOVE until the signal. Stay ready! If you move, you will desync client!");
    close;

L_Full:
    mesn;
    mesq l("You cannot bring anything to the arena. Please put everything on the storage.");
    close;

L_Hub:
    mesn;
    mesc l("Welcome to HUNGRY QUIRIN ARENA mangment panel.");
    next;
    mesn;
    mesq l("I am Quirino Voraz, and my arena is the coolest PVP Arena on all Mana Worlds.");
    mesq l("However, I need a tax to start, and you need to arrange players. There are no refunds.");
    next;
    mes l("Current player count: @@/5 must be online.", getusers(1));
    mes l("Current arena player count: @@ on map. (Min. 3 to begin event)", getmapusers("001-8"));

    // Open event? Minimum 5 connections or GM_OVERRIDE flag.
    if ($@EQ_TIMER < gettimetick(2) && !$@EQ_STATUS && (getusers(1) >= 5 || $@GM_OVERRIDE)) {
        next;
        mesc l("Activate event?"), 1;
        mesc l("It'll cost @@ GP", .price);
        if (Zeny < .price)
            close;
        menuint
            l("NO"), -1,
            l("Give players 5 minutes"), 300,
            l("Give players 10 minutes"), 600,
            l("Give players 15 minutes"), 900,
            l("Give players 20 minutes"), 1200,
            l("Give players 25 minutes"), 1500,
            l("Give players 30 minutes"), 1800,
            l("NO"), -1;
        mes "";
        if (@menuret > 0) {
            Zeny=Zeny-.price;
            $@EQ_TIMER=gettimetick(2)+@menuret;
            initnpctimer;
            setcells "001-8", 41, 58, 41, 43, 6, "qhubN";
            setcells "001-8", 41, 58, 56, 58, 6, "qhubS";
            announce("##1HUNGRY QUIRIN EVENT: ##3##BRegister is now open! Talk to Quirin, on Tulimshar Councilroom!", bc_all|bc_npc);
            channelmes("#world", strcharinfo(0)+" invites everyone to HUNGER QUIRIN PVP ARENA in Tulimshar. It'll start in "+FuzzyTime($@EQ_TIMER));
        }
        close;
    }

    // Main Control menu. Not using l() on purpose.

    select
        rif($@EQ_STATUS == 0 && (getmapusers("001-8") >= 3 || $@GM_OVERRIDE) && is_admin(), "Start Event at once!"),
        rif($@EQ_STATUS == 1 && is_admin(), "Send wave of items and monsters!"),
        rif($@EQ_STATUS == 0 && $@GM_OVERRIDE && is_admin(), "[DEBUG] Join Event"),
        rif($@EQ_STATUS == 0 && $@GM_OVERRIDE && is_admin(), "[DEBUG] Join & Start Event Now"),
        "I'm done.";

    if (@menu == 1)
        donpcevent("#QuirinoHUB::OnStart");
    if (@menu == 2)
        donpcevent("#QuirinoHUB::OnSendWave");
    if (@menu == 3)
        goto L_Register;
    if (@menu == 4) {
        addtimer 1000, "Quirin::OnDebugReg";
        goto L_SignUp;
    }

    close;

OnDebugReg:
    donpcevent("#QuirinoHUB::OnStart");
    end;

// Each minute
OnTimer60000:
    // We must autostart event now
    if ($@EQ_TIMER <= gettimetick(2)) {
        if (getmapusers("001-8") >= 3 || $@GM_OVERRIDE) {
            donpcevent("#QuirinoHUB::OnStart");
        } else {
            delcells "qhubN";
            delcells "qhubS";
            maptimer("001-8", 1000, "#QuirinoHUB::OnCancel");
            announce("##1HUNGRY QUIRIN EVENT: ##3##BCancelled due lack of players!", bc_all|bc_npc);
        }
        end;
    }
    mapannounce "003-1", "Hungry Quirin starts in " + FuzzyTime($@EQ_TIMER) + " and there are " + getmapusers("001-8") + "/3 player(s) standing by." , 0;
    mapannounce "001-8", "Hungry Quirin starts in " + FuzzyTime($@EQ_TIMER), 0;
    initnpctimer;
    end;

OnInit:
    .price=570;
    .sex=G_MALE;
    .distance=5;
    end;
}


/*
Blockwalls TileCondition and masks for setcells
manaplus/src/enums/resources/map/blockmask.h

        WALL        = 0x80,  // 1000 0000 = 128
        AIR         = 0x04,  // 0000 0100 = 4
        WATER       = 0x08,  // 0000 1000 = 8
        GROUND      = 0x10,  // 0001 0000 = 16
        GROUNDTOP   = 0x20,  // 0010 0000 = 32
        PLAYERWALL  = 0x40,  // 0100 0000 = 64
        MONSTERWALL = 0x02   // 0000 0010 = 2
*/
