// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    This script controls access to Ships, fixing variables.

012-1,157,65,0	script	HurnsShip#M	NPC_HIDDEN,0,0,{

OnTouch:
    LOCATION$="Hurns";
    warp "002-3@"+LOCATION$, 31, 28;
    closedialog;
    close;
}
