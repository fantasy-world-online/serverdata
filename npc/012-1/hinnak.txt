// TMW2 Script
// Author:
//    Jesusalva
// Description:
//    Hurnscald farmer.

012-1,105,30,0	script	Hinnak	NPC_ORC,{
    .@q1=getq(HurnscaldQuest_Farmers);
    .@q2=getq2(HurnscaldQuest_Farmers);
    mesn;
    if (strcharinfo(0) == $MOST_HEROIC$) mesq l("Ah, if it isn't @@? You're the talk of the town!", $MOST_HEROIC$);
    if (strcharinfo(0) == $MOST_HEROIC$) next;
    if (.@q1 == 0) goto L_Start;
    if (.@q1 == 1) goto L_Check;
    if (.@q1 == 4) goto L_Letter;
    if (.@q1 == 5) goto L_Thanks;
    mesq l("Maybe you could now help my friend Oscar. His farm is west of here.");
    close;

L_Start:
    mesq l("I hate you Pinkies, and I hate the Monster King too, for bringing forth these plagues!!");
    next;
    mesn strcharinfo(0);
    mesc l("The farmer seems mad and in need of help. Will you help him?");
    if (askyesno() != ASK_YES) {
        mesq l("Sorry, I have to go.");
        close;
    }
    next;
    mesn;
    mesq l("Ah, so you're willing to help? Great! Because I HATE THEM ALL!");
    next;
    if (readparam(bAgi) < 20) goto L_Slow;
    if (BaseLevel < 20) goto L_Weak;
    mesn;
    mesq l("They jump left and right and left and right again, and I can't catch them.");
    next;
    mesn;
    mesq l("Please kill some of them, and bring me 10 @@!", getitemlink(PinkAntenna));
    setq HurnscaldQuest_Farmers, 1, 0;
    close;

OnKillPinkie:
    .@q1=getq(HurnscaldQuest_Farmers);
    .@q2=getq2(HurnscaldQuest_Farmers);
    if (.@q1 == 1) {
        setq2 HurnscaldQuest_Farmers, .@q2+1;
        if (! (.@q2+1) % 10)
            dispbottom l("@@ pinkies killed on @@'s field.", .@q2+1, l("Oscar"));
    }
    fix_mobkill(Pinkie);
    end;

L_Slow:
    mesn;
    mesq l("But you're too slow to catch any of them. Sorry.");
    next;
    mesc l("Raise agility to do this quest.");
    close;

L_Weak:
    mesn;
    mesq l("But you're too weak to beat any of them. Sorry.");
    close;

L_Check:
    mesq l("Hey, how is the monster extermination going?");
    next;
    mesn;
    // You in average need to kill 244 Pinkies. You get this bonus for not leaving the fields.
    if (.@q2 >= 210)
        mesq l("You killed so many Pinkies, I don't care with the Antennas anymore! They are almost gone!");
    else if (.@q2 >= 150)
        mesq l("I see you are doing good. Keep slaying them, hahaah!");
    else if (.@q2 >= 100)
        mesq l("Good job, you already killed over 100! Hahah, that sure teach them a lesson!");
    else if (.@q2 >= 50)
        mesq l("Yeah, teach them a lesson! Keep going!");
    else
        mesq l("Go kill them!!");
    next;
    menu
        rif (.@q2 >= 210 && countitem(PinkAntenna) >= 10, l("No, I insist, keep the Antennas as a trophy.")), L_RealHelp,
        rif (.@q2 >= 210 && countitem(PinkAntenna) < 10, l("Thanks, they don't like to drop their Antennas.")), L_TrueHelp,
        rif (.@q2 >= 100 && .@q2 < 210 && countitem(PinkAntenna) >= 10, l("Here are the Antennas.")), L_Help,
        rif (.@q2 < 100 && countitem(PinkAntenna) >= 10, l("Here are the Antennas.")), L_NoKill,
        l("I'm not done yet. I'll be back."), -;
    close;

L_RealHelp:
    inventoryplace Scythe, 1;
    delitem PinkAntenna, 10;
    getexp 1600, 60;
    Zeny=Zeny+2500;
    setq HurnscaldQuest_Farmers, 2, 0;
    getitem Scythe, 1;
    mes "";
    mesn;
    mesq l("Waw! You really did it, you're the savior of my farm!! You have my eternal gratitute. Here is 2500 GP for your troubles!");
    close;

L_TrueHelp:
    mes "";
    mesn;
    mesq l("I know how hard it is! I hate them with all my heart, only seeing you killing them left and right was GREAT!");
    next;
    inventoryplace Scythe, 1;
    getitem Scythe, 1;
    getexp 1000, 0;
    Zeny=Zeny+500;
    setq HurnscaldQuest_Farmers, 2, 0;
    mesn;
    mesq l("You have my eternal gratitute. Here is 500 GP for your troubles.");
    close;

L_Help:
    inventoryplace Scythe, 1;
    delitem PinkAntenna, 10;
    getexp 1000, 0;
    Zeny=Zeny+1000;
    setq HurnscaldQuest_Farmers, 2, 0;
    getitem Scythe, 1;
    mes "";
    mesn;
    mesq l("Many thanks. You have my eternal gratitute. Here is 1000 GP for your troubles.");
    close;

L_NoKill:
    mesn;
    mesq l("That won't do it. I want to see PINK BLOOD! No... not so cluttered. Just kill the Pinkies on my farm.");
    next;
    mesn;
    mesq l("The antennas by themselves mean nothing, I want to see you killing my ENEMIES, the pinkies!");
    close;

L_Letter:
    mesq l("You're the savior of Hurnscald crops. Half from the world would die from famine, weren't for you.");
    next;
    mesn;
    mesq l("@@ and I signed this letter. Deliver it to Airlia on the Town Hall, and she'll reward you correctly.", l("Oscar"));
    close;

L_Thanks:
    mesq l("Many thanks for all the help!");
    next;
    GHQ_Assign(Pinkie, "Hurnscald", getitemlink(PinkHelmet) + col(l(" (Hinnak's and Oscar's fields Pinkies may not count)"), 1));
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, FarmerHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, LeatherShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, TulimsharGuardBoots);
    setunitdata(.@npcId, UDT_WEAPON, CottonTrousers);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 0);

    .sex = G_MALE;
    .distance = 5;
    end;
}

