// TMW-2 Script
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Mahad is the Hurnscald Well Master
//    ...Well, he takes the water from the pond, but you shouldn't mind this.
//    It's still safe to drink, 100% warranted!
// Note: You may get a Grass Carp (worth 100 GP) instead. Bottle is lost? O.o

012-1,117,54,0	script	Mahad	NPC_PLAYER,{
    mesn;
    mes l("I can fill your bottle with water for only @@ gp the bottle.", .price);
    mes l("After all, I am the Well Master!");
    input .@count;
    mes "";

    if (!.@count)
        close;

    .@gp = .@count * .price;

    // Not enough cash
    if (Zeny < .@gp) {
        mesn;
        mes l("You don't have enough gold! You need @@ gp.", .@gp);
        close;
    }

    // Not enough bottles
    if (countitem(EmptyBottle) < .@count) {
        mesn;
        mes l("You don't have that many empty bottles!");
        close;
    }

    inventoryplace GrassCarp, .@count, BottleOfWoodlandWater, .@count;

    // Calculate how many grass carp you'll get
    .@iced=0;
    for (.@i=0; .@i < .@count; .@i++) {
        if (rand(1,1000) < 11)
            .@iced++;
    }

    // Apply the results and have a happy day!
    Zeny-=.@gp;
    if (.@iced)
        getitem GrassCarp, .@iced;
    getitem BottleOfWoodlandWater, .@count-.@iced;
    close;

OnInit:
    .price = 60;
    .sex = G_MALE;
    .distance = 7;

    .@npcId = getnpcid(.name$);
    // Check items.xml for info about this
    setunitdata(.@npcId, UDT_HEADTOP, InfantryHelmet);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SailorShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, RaidTrousers);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 7);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 17);

    end;
}
