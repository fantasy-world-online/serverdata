// TMW2 Script
// Author:
//  Jesusalva
//  Saulc
// Description:
//      Guard ask you to clean the cave of red scorpion. bring her some red scorpion stingers to prove you do it well.
// Variable:
//      TulimsharQuest_Devoir

003-10,38,60,0	script	Guard Devoir	NPC_GUARD2,{

    mesn;
    mesq l("That's terrible! Monsters are piling up near city gates! We need to stop their progression.");
    if (BaseLevel >= 25) goto L_Menu;
    close;

L_Menu:
    mesn;
    mesq l("Would you like to help us to protect the town from a red scorpion invasion??");
    mes "";
    menu
        l("Yeah, sure! I'm brave as Simon."),L_Quest, // famous player name
        l("Where can I find them?"),L_Where,
        l("No, thanks."),L_Close;

L_Quest:
    mes "";
    .@q=getq(TulimsharQuest_Devoir);
    mesq l("Nice! I want you to kill some red scorpions, as I said!");
    next;
    if (.@q == 0) goto L_Continue;
    .@q2=getq2(TulimsharQuest_Devoir) + 60 * 60 * 36;
    if (santime() >= .@q2) goto L_Repeat; // Repats every 36 hours
    mesn;
    mesq l("But, it wouldn't look nice if I let you do all the killing! The other guards are working right now!");
    next;
    mesn;
    mesq l("Come back in a few hours, and we can fix that!");
    close;

L_Continue:
    mesq l("Red Scorpion breed as fast as the Ratto! With our current numbers, it's nearly impossible to take over.");
    next;
    mesq l("I usually ask for 7, but to prove you are going to help us in this purge, I will ask you for 14 @@! I will reward you for your bravure.", getitemlink(RedScorpionStinger));
    mes "";
    menu
        rif(countitem(RedScorpionStinger) >= 14, l("Here they are, miss!")), L_Finish,
        l("I'll get to it."), L_Close;
    close; // double sure

L_Repeat:
    mesq l("How is your purge going? I hope you had success at it!");
    next;
    mesq l("Did you brought me 7 @@?", getitemlink(RedScorpionStinger));
    mes "";
    menu
        rif(countitem(RedScorpionStinger) >= 7, l("Here they are miss!")), L_Finish2,
        l("I'll get to it."), L_Close;
    close;

// First Time Only
L_Finish:
    delitem RedScorpionStinger, 14;
    getexp 2203, 11; // r7.5 1703 → 2203
    Zeny = (Zeny + 1372); // 70*14 = 980 base (40% bonus)
    setq TulimsharQuest_Devoir, 1, santime();
    mes "";
    mesn;
    mesq l("Many thanks! Come back later to bring me extra @@!", getitemlink(RedScorpionStinger));
    close;

// Repeat
L_Finish2:
    delitem RedScorpionStinger, 7;
    getexp 1193, 0; // r7.5 993 → 1193
    Zeny = (Zeny + 686); // 70*7 = 490 base (40% bonus)
    setq TulimsharQuest_Devoir, 1, santime();
    mes "";
    mesn;
    mesq l("Many thanks! Come back later to bring me extra @@!", getitemlink(RedScorpionStinger));
    close;

L_Where:
    mes "";
    mesq l("Ah, there are lots on the miners cave.");
    next;
    mes l("Take care though, don't attack them when they are in group!");
    next;
    mes l("Good luck.");
    goto L_Close;

L_Close:
    closedialog;
    goodbye;
    close;

OnInit:
    .sex = G_FEMALE;
    .distance = 5;
    end;
}
