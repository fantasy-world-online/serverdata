// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 010-1: Desert Mountains mobs
010-1,105,89,9,4	monster	Desert Maggot	1083,12,35000,150000
010-1,65,97,4,4	monster	Scorpion	1071,8,35000,150000
010-1,48,82,4,4	monster	Scorpion	1071,6,35000,150000
010-1,60,57,9,4	monster	Sarracenus	1125,2,35000,300000
010-1,88,76,3,8	monster	Mountain Snake	1123,3,35000,300000
010-1,103,29,3,8	monster	Mountain Snake	1123,3,35000,300000
010-1,120,32,3,2	monster	Desert Bandit	1124,1,35000,300000
010-1,115,47,3,2	monster	Desert Bandit	1124,2,35000,300000
010-1,53,87,3,2	monster	Desert Bandit	1124,2,35000,300000
010-1,93,79,6,5	monster	Sarracenus	1125,2,35000,300000
010-1,78,97,6,5	monster	Sarracenus	1125,1,35000,300000
010-1,75,45,6,5	monster	Sarracenus	1125,1,35000,300000
010-1,100,69,8,2	monster	Snake	1122,12,35000,300000
010-1,59,95,8,2	monster	Snake	1122,2,35000,300000
010-1,74,45,17,3	monster	Snake	1122,4,35000,300000
010-1,110,36,8,2	monster	Desert Maggot	1083,10,35000,300000
010-1,56,100,9,4	monster	Desert Maggot	1083,12,35000,150000
010-1,81,57,39,39	monster	Desert Maggot	1083,60,35000,150000
