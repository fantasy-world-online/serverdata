// TMW2 Script.
// Author:
//    Jesusalva
// Description:
//    Kfahr - Placeholder
// Quests:
//  HurnscaldQuest_Khafar
//    .@q1 = when even, must buy a beer
//           when odd, can ask another story
//    .@q2 = Temporary variable, holds last story
// Secrets Unlocked:
//  NivalisQuest_Baktar (status 1→2)
//    Win arm wrestling to unlock Braknar Shield Secrets

012-2,54,46,2	script	Khafar	NPC_ORC,{
    function MustDrink;
    mesc l("You stand before a battle-scarred, darkly tanned warrior, brimming with muscles.");
    mesc l("Just looking at him you smell danger, adventure, excitement...");
    next;
    mesc l("On second thought, he really could use a bath.");
    next;
    mesc l("The warrior turns towards you, grinning broadly.");
    next;
    mesn;
    mesq l("Why, hello there!  Come to visit me to hear Kfahr the Warrior or Andrei Sakar's exploits, have you?");
    mesc l("He laughs heartily and gives you a slap on the back.");
    next;
    mesn;
    mesq l("Can't blame you, can't blame you at all! Here, take a seat!");
    next;
    goto L_Menu;

L_Menu:
    do
    {
        .@q=getq(HurnscaldQuest_Khafar);
        if (.@q % 2 == 0)
            MustDrink();
        select
            l("Don't you have any exploit of your own?"),
            l("Who is Kfahr the Warrior?"),
            l("Who is Andrei Sakar?"),
            l("Nah, see you later.");
        mes "";
        switch (@menu) {
        case 1:
            mesn;
            mesq l("I once killed a Giant Maggot alone!");
            next;
            break;
        case 2:
            mesn;
            mesq l("He is my grand-grand-grandfather! The might Kfahr, with his friend Braknar, and heroes from Tulimshar legends.");
            next;
            mesn;
            mesq l("Togheter, they wrote legends. They slayed Toby Rick the Desert Worm, the mightiest desert beast.");
            next;
            mesn;
            mesq l("Braknar also used a pseudonym - Arvek. If you hear that name in some tale, it is the same person.");
            next;
            mesn;
            mesq l("Well, that was a good fight. My grand-grand-grandfeather gave his shield to Arvek -- err, Braknar I mean.");
            next;
            mesn;
            mesq l("The last time they were seen... They went to explore the Desert Temple. None returned alive.");
            next;
            select
                l("Cool story, bro!"),
                l("What about the Desert Temple?"),
                l("What about the Shield?"),
                l("What about the worm?");
            mes "";
            switch (@menu) {
                case 2:
                    mesn;
                    mesq l("Lost forever on the Desert Canyon, I guess.");
                    next;
                    break;
                case 3:
                    mesn;
                    mesq l("Oh, the @@ might be a heirloom or something from Braknar family. I dunno who could have it.", getitemlink(BraknarShield));
                    if (getq(NivalisQuest_Baktar) == 1)
                        setq1 NivalisQuest_Baktar, 2;
                    next;
                    break;
                case 4:
                    mesn;
                    mesq l("It is a boss. I don't know how to summon one, and to be honest - You won't want to see one either.");
                    next;
                    break;
            }
            break;
        default:
            mesn;
            mesq lg("See you later, my friend! Thanks for the drink!");
            close;
            break;
        }
        setq1 HurnscaldQuest_Khafar, .@q+1;
        getexp 50, 0; // Beer is now lost - get 50xp
    } while (true);
    close;

function MustDrink {
    mesn;
    mesq l("But first... My throat is dry. Can you bring me a beer?");
    if (!countitem(Beer))
        close;
    next;
    select
        l("Here, you can have a glass."),
        l("Sorry, I have nothing.");
    mes "";
    if (@menu == 1) {
        delitem Beer, 1;
        .@q=getq(HurnscaldQuest_Khafar);
        setq1 HurnscaldQuest_Khafar, .@q+1;
        return;
    }
    close;
}

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, FairyHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, ForestArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonShorts);
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 24);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 7);

    .sex = G_MALE;
    .distance = 5;
    npcsit;
    end;
}

