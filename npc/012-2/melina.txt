// TMW-2 Script
// Author:
//    Jesusalva
// Description:
//    Sells beer
// Variables:
//    $HELENA_ST
//      Controls stock and beer prices

012-2,54,43,0	script	Melina	NPC_FEMALE,{

hello;
shop .name$;
close;

OnTimer1000:
    domovestep;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, CottonGloves);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CottonShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, AssassinBoots);
    setunitdata(.@npcId, UDT_WEAPON, MiniSkirt);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 20);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    tradertype(NST_MARKET);
    if ($HELENA_ST < 700)
        sellitem Beer, 500-($HELENA_ST/2), ($HELENA_ST/3)+1;
    else
        sellitem Beer, 150, ($HELENA_ST/3)+1;

    initpath "move", 54, 43,
             "dir", DOWN, 0,
             "wait", 27, 0,
             "move", 43, 43,
             "dir", DOWN, 0,
             "wait", 27, 0,
             "move", 48, 45,
             "dir", DOWN, 0,
             "wait", 27, 0,
             "move", 58, 42,
             "dir", DOWN, 0,
             "wait", 27, 0,
             "move", 35, 43,
             "dir", DOWN, 0,
             "wait", 20, 0;
    initialmove;
    initnpctimer;

    .sex = G_FEMALE;
    .distance = 5;
    end;

OnClock2358:
    if ($HELENA_ST < 700)
        restoreshopitem Beer, 500-($HELENA_ST/2), ($HELENA_ST/3)+1;
    else
        restoreshopitem Beer, 150, ($HELENA_ST/3)+1;
    end;

}
