// TMW-2 Script.
// Evol scripts.
// Authors:
//    gumi
//    Qwerty Dragon
//    Reid
//    WildX
//    Jesusalva
// Description:
//    A small note presenting the rules and game-world release notes of TMW-2.

002-3,40,25,0	script	Note#johanne	NPC_PAPER_NOTE,{
    narrator S_LAST_NEXT,
            l("The La Johanne always have interesting notes.");

    do {
        select
            l("Read the Rules."),
            l("Read the News."),
            l("Leave.");

        switch (@menu) {
            case 1:
                GameRules 8 | 4;
                break;
            case 2:
                GameNews;
                break;

        }
    } while (@menu != 3);


    narrator S_NO_NPC_NAME,
        l("Following these lines are some other writings on this paper."),
        l("Do not give the password of your room to anybody! Keep it secret and try not to use the same one in any other room in the future. - Juliet"),
        l("I love you, Silvia! - Swezanne"),
        l("Other things are written but are not legible anymore.");

    close;

OnInit:
    .distance = 2;
    end;
}
