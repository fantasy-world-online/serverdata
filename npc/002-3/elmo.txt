// TMW2 Script.
// Authors:
//    Jesusalva
// Description:
//    Elmo's second dialog. He is Nard's deputy and second-in-command.
//    Elmo was created in Evol by Qwerty Dragon and Reid
// TODO: allows smart noobs (<15) get EXP Bonus (20%)

002-3,32,24,0	script	Elmo	NPC_ELMO,{
    function ExpBoost;
    showavatar NPC_ELMO;  // this is handled by avatars.xml

    // Core functions
    if (BaseLevel < 15)
        ExpBoost();

    if (getq(ShipQuests_Julia) < 3) goto L_NotYet;
    if (getq(CandorQuest_Sailors) == 2) goto L_Party;
    if (LOCATION$ == "Candor" && rand(1,7) != 5) goto L_Candor;

    // TODO: NPC looking at task schedule and stuff
    mesn;
    if (rand(1,2) == 1)
        mesq l("Ah, @@ seems to be behind the @@ schedule again...", any("Juliet", "Billy Bons", "Chef Gado", "Dan", "Devis", "Peter"), any(l("cleaning"), l("stocking")));
    else
        mesq l("Uhm, @@ seems to have finished their scheduled tasks again... If they were so diligent in cleaning and stocking, though...", any("Nard", "Elmo", "Juliet", "Chef Gado", "Dan", "Peter"));

    close;

L_NotYet:
    mesn;
    mes l("I'm not the Captain, Nard is.");
    mes l("You should talk to him instead.");
    close;

L_Candor:
    mesn;
    if (getq(General_Narrator) < 1) mes l("\"Hey, have you already got the money necessary for the travel?");
    if (getq(General_Narrator) < 1) mes l("If you haven't, maybe there are a few things you can do besides selling items.\"");
    if (getq(General_Narrator) >= 1) mesq l("Maybe there are things in Candor which still require your attention? I overheard some of them.");
    next;

    mes "";

    // Valon Quest
    .@q=getq(CandorQuest_Trainer);
    if (.@q < 1) {
        mesc l("##BFirst and foremost, you should talk to Trainer, inside the big house.##b"), 1;
        mes l("Besides being able to train you, he is a walking encyclopedia - Ask him anything you are unsure about!");
        next;
        mes l("To find him, just leave the ship and turn left. You should also touch the Soul Menhir when you leave this ship.");
        mes l("The Soul Menhir will attach your soul, so when you die, you'll appear where you last touched it.");
        close;
    } else if (.@q < 12) {
        mes l("- Inside the big house is someone who can train you. All experience is handy!");
    }

    // Barrel Quest
    .@q=getq(CandorQuest_Barrel);
    if (.@q < 4)
        mes l("- I think you can help the storehouse for some quick cash.");

    // Kids Quest
    .@q=getq(CandorQuest_HAS);
    if (.@q < 4)
        mes l("- You can always play with kids. Not very profitable, though.");

    // Sailors Quest
    .@q=getq(CandorQuest_Sailors);
    if (.@q < 3)
        mes l("- Some of our crew are missing. They're probably wasting their time at beach.");

    // Vincent Quest
    .@q=getq(CandorQuest_Vincent);
    if (.@q < 2)
        mes l("- I overheard rumors about a festival. Maybe someone needs help with their figurine?");

    // Tolchi Quest
    .@q=getq(CandorQuest_Tolchi);
    if (.@q < 2)
        mes l("- The weapon master, Tolchi, could use your help. But she will most likely force you to visit Tulimshar in the end.");

    // Maya Quest
    .@q=getq(CandorQuest_Maya);
    if (.@q < 4)
        mes l("- There is a woman walking on the island, called Maya. Once she realises you're willing to help, she'll start paying well.");

    // Rosen Quest
    .@q=getq(CandorQuest_Rosen);
    if (.@q < 3)
        mes l("- The weapon seller, Rosen, wanted to help new players to improve their equipment.");

    // Nylo Quest
    .@q=getq(CandorQuest_Marggo);
    if (.@q < 1)
        mes l("- The farmer Nylo, who loves beer and money, seems to be having troubles with his crops.");

    // Ship Quests
    .@q1=getq(ShipQuests_Dan);
    .@q2=getq(ShipQuests_ChefGado);
    .@q3=getq(ShipQuests_Peter);
    if (.@q1 < 3 || .@q2 < 2 || .@q3 < 7)
        mes l("- Some sailors within this ship may need your help: Chef Gado, Dan, Peter... help them all and collect rewards!");

    // Report in an abstract way to the player how good they are at getting travel
    // discounts, and how much work is left to do. Some points are easy/required to get (eg. Dan, Peter, HAS, etc.)
    next;
    closeclientdialog;
    .@n=nard_reputation();
    if (.@n >= 15)
            npctalk3 l("Nard is truly amazed at you. I am impressed, too.");
    else if (.@n >= 13)
            npctalk3 l("Nard is amazed at you.");
    else if (.@n >= 11)
            npctalk3 l("Nard is very impressed, you're really a hard worker. Congrats!");
    else if (.@n >= 9)
            npctalk3 l("Nard is impressed, you're a hard worker.");
    else if (.@n >= 7)
            npctalk3 l("Nard noticed your hard work.");
    else if (.@n >= 5)
            npctalk3 l("Nard likes people who work hard. Work harder!");
    else if (.@n >= 3)
            npctalk3 l("You really should do some tasks to impress our captain.");
    else
            npctalk3 l("Nard doesn't like people who gets money without working for it.");

    end;

L_Party:
    mesn;
    mesq l("What? A party?");
    next;
    setq CandorQuest_Sailors, 3;
    getexp 25, 0;
    Zeny = (Zeny + 1000);
    mesq l("Alright, I'll show up later. Thanks for calling me. Here's 1000 GP for your efforts."); // With this, the final cost is 50 GP
    close;

function ExpBoost {
    mesn;
    mesq l("Hey there, @@! I see you are still a noob!", strcharinfo(0));
    next;
    mesn;
    mesq l("Well, I'll give you a hour of EXP RATE UP! How cool is that? Enjoy!");
    mesc l("This boost can be used until level 15."), 9;
    next;
    // Get the average level of top players to calculate EXP Boost
    // Level 100 ("max") = 25% EXP BOOST (max)
    // Current (2019-04-27) top is 80/80/75, meaning a 19% EXP Boost.
    .@AVG_LEVEL=($@hoblvl_value[0]+$@hoblvl_value[1]+$@hoblvl_value[2])/3;
    .@BONUS=.@AVG_LEVEL/4;
    sc_end SC_OVERLAPEXPUP;
    sc_start SC_OVERLAPEXPUP, 3600000, min(25, .@BONUS);
    mesc l("EXP Gain raised in @@% for one hour!", min(25, .@BONUS)), 2;
    return;
}

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}
