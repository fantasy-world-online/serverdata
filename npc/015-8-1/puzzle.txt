// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    PUZZLES and TRAPS submodule - Sagratha's Cave Boss Room - 015-8-1

// Exit area
015-8-1,50,79,0	script	#Exit01581	NPC_HIDDEN,1,0,{
    end;
OnTouch:
    .@q=getq(HurnscaldQuest_Sagratha);
    // Cheater Detected
    if (!MAGIC_LVL || .@q < 5) { 
        setq HurnscaldQuest_Sagratha, 0, 0, 0;
        sc_end SC_CASH_PLUSEXP;
        sc_end SC_OVERLAPEXPUP;
        sc_start SC_OVERLAPEXPUP, 300000, -20;
        warp "Save", 0, 0;
        end;
    }
    if (.@q == 5) {
        dispbottom l("You are NOT allowed to leave here!");
    } else {
        warp "015-8", 94, 21;
    }
    end;
}


015-8-1,0,0,0	script	#SaggyBossTrap01	NPC_TRAP,0,0,{
    end;
OnTouchNPC:
OnTouch:
    // instance_id()
    if (instance_id() >= 0)
        SteelTrap(rand2(10, 40), 5, any(0,1,1,2), instance_npcname(.name$));
    else
        SteelTrap(rand2(10, 40), 5, any(0,1,1,2));
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay instance_npcname(.name$), NPC_TRAP;
    end;


OnInstanceInit:
    .@x=rand(20,80);
    .@y=rand(20,80);
    movenpc instance_npcname(.name$), .@x, .@y;
    // It's on a wall, let's remove it
    if (!checkcell(instance_mapname("015-8-1"), .@x, .@y, cell_chkpass)) {
        disablenpc instance_npcname(.name$);
    }
    end;

OnInit:
    disablenpc .name$;
    end;
}

// Create more traps. (They can be on walls so amount is random >.<)
015-8-1,0,0,0	duplicate(#SaggyBossTrap01)	#SaggyBossTrap02	NPC_TRAP,0,0
015-8-1,0,0,0	duplicate(#SaggyBossTrap01)	#SaggyBossTrap03	NPC_TRAP,0,0
015-8-1,0,0,0	duplicate(#SaggyBossTrap01)	#SaggyBossTrap04	NPC_TRAP,0,0
015-8-1,0,0,0	duplicate(#SaggyBossTrap01)	#SaggyBossTrap05	NPC_TRAP,0,0
015-8-1,0,0,0	duplicate(#SaggyBossTrap01)	#SaggyBossTrap06	NPC_TRAP,0,0
015-8-1,0,0,0	duplicate(#SaggyBossTrap01)	#SaggyBossTrap07	NPC_TRAP,0,0
015-8-1,0,0,0	duplicate(#SaggyBossTrap01)	#SaggyBossTrap08	NPC_TRAP,0,0
015-8-1,0,0,0	duplicate(#SaggyBossTrap01)	#SaggyBossTrap09	NPC_TRAP,0,0
015-8-1,0,0,0	duplicate(#SaggyBossTrap01)	#SaggyBossTrap10	NPC_TRAP,0,0
015-8-1,0,0,0	duplicate(#SaggyBossTrap01)	#SaggyBossTrap11	NPC_TRAP,0,0
015-8-1,0,0,0	duplicate(#SaggyBossTrap01)	#SaggyBossTrap12	NPC_TRAP,0,0
// twelve traps should be enough

// Transitional, dummy NPCs
015-8-1,25,34,0	script	#LockedDoor01581A	NPC_NO_SPRITE,0,0,{
    end;
OnTouch:
    npctalkonce l("This door is locked.");
    end;
}

015-8-1,26,68,0	duplicate(#LockedDoor01581A)	#LockedDoor01581B	NPC_NO_SPRITE,0,0
015-8-1,37,68,0	duplicate(#LockedDoor01581A)	#LockedDoor01581C	NPC_NO_SPRITE,0,0

015-8-1,53,31,0	script	#PaperNote01581A	NPC_NO_SPRITE,{
    npctalkonce any(l("You cannot decipher what's written in there."), l("It's written in Mananese, you cannot read."), l("It's full of Mouboo drawings."), l("This note is too old and difficult to read."), l("The only readable thing is an old stain of blood."), l("This note is not interesting, maybe the obelisk is more."));
    end;
OnInit:
    .distance=2;
    end;
}

015-8-1,56,31,0	duplicate(#PaperNote01581A)	#PaperNote01581B	NPC_NO_SPRITE
015-8-1,59,31,0	duplicate(#PaperNote01581A)	#PaperNote01581C	NPC_NO_SPRITE
015-8-1,62,31,0	duplicate(#PaperNote01581A)	#PaperNote01581D	NPC_NO_SPRITE
015-8-1,65,31,0	duplicate(#PaperNote01581A)	#PaperNote01581E	NPC_NO_SPRITE
015-8-1,68,31,0	duplicate(#PaperNote01581A)	#PaperNote01581F	NPC_NO_SPRITE
015-8-1,71,31,0	duplicate(#PaperNote01581A)	#PaperNote01581G	NPC_NO_SPRITE

015-8-1,42,51,0	script	#Sign01581A	NPC_NO_SPRITE,{
    npctalkonce l("It's a strange drawing of a Mouboo.");
    dispbottom l("Where is here? What was this place used for?!");
    end;
OnInit:
    .distance=2;
    end;
}

