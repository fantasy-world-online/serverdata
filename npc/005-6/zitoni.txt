// TMW2 scripts.
// Author:
//    Vasily_Makarov (original from Evol)
//    Jesusalva
// Description:
//    Stat resetter. He is an alchemist, married with Morgan.
// Others:
// .@wasSP - free status points before reset

005-6,43,39,0	script	Zitoni	NPC_RUMLY,{
    mesn;
    mesq l("Ah, hello there! I am @@, a Redy alchemist.", .name$);
    mes "";
    mesq l("I have developed a special formula, which resets your stats!");
    next;
    mes "";

L_Menu:
    .@q1=getq(TulimsharQuest_DarkInvocator);
    if (BaseLevel < 10)
        .@price = 1;
    else
        .@price = (BaseLevel*200-(9*200))/(BaseLevel/10);
    // Lv 10: 1 GP
    // Lv 90: 1.800 GP

    mesn strcharinfo(0);
    select
        l("Status reset? Sounds illegal!"),
        l("Can you reset my stats please?"),
        l("Do you make any other kind of potions?"),
        rif(.@q1 == 1 || .@q1 == 2, l("Can you help me with Everburn Powder? I need 5.")),
        rif(.@q1 == 3 && countitem(DarkDesertMushroom) >= 1,l("Zarkor sent you this gift. He needs Everburn Powder.")),
        rif(.@q1 >= 4,l("I need some Everburn Powder.")),
        lg("You are weird, I have to go sorry.");

    mes "";
    switch (@menu) {
        case 1:
            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                lg("Wait, are you with the police? I didn't do anything wrong, I promise!"),
                l("My formula is not a drug, nor magic. It is an ancient technology of our people!"),
                l("You can use it to clear your stats, to start freshly if you know what I mean..."),
                l("For only a small amount of Gold Pieces, I will show you how it works!"),
                l("Although the more powerful you are, the more money you will need."),
                l("I will let you test it for a peny until level 10!");

            select
                l("Sounds good!"),
                rif(Zeny >= .@price, lg("I think I have enough gold with me.")),
                l("We will talk about it later."),
                l("My stats are too good, I won't need it.");

            switch (@menu) {
                case 1:
                    speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                        l("Yes, it is a really sweet deal, believe me!");

                    goto L_Menu;
                case 2:
                    goto L_ResetStats;
                case 3:
                    goto L_Quit;
                case 4:
                    goto L_Quit;
            }

        case 2:
            goto L_ResetStats;
        case 3:
            goto L_OtherPotion;
        case 4:
            goto L_DarkInv_Ever;
        case 5:
            goto L_DarkInv_Mush;
        case 6:
            goto L_DarkInv_Powder;
        default:
            goto L_Quit;
    }

L_ResetStats:
    mesn;
    mesq l("Status point reset can't be undone. Do you really want this?");

L_ConfirmReset:
    if (BaseLevel <= 10)
        ConfirmStatusReset(0);
    else
        ConfirmStatusReset();
    goto L_Quit;

L_OtherPotion:
    mesn;
    mesq l("I make both @@ and @@, if you give me the shrooms associated to them, and money.", getitemlink(HastePotion), getitemlink(StrengthPotion));
    next;
    mesn;
    mesq l("For you, it will be only 50 GP for potion! But I need the base ingredients, four @@ - or @@, depends on which one.", getitemlink(Plushroom), getitemlink(Chagashroom));
    next;
    menu
        l("Nothing at the moment."), L_Quit,
        l("4 Plushrooms for a Haste Potion!"), L_HastePotion,
        l("4 Chagashrooms for a Strength Potion!"), L_StrengthPotion;

L_HastePotion:
    mes "";
    mesn;
    if (Zeny < 50) {
        mesq l("You don't have enough money. Sorry.");
        next;
        goto L_Quit;
    }
    if (countitem(Plushroom) < 4) {
        mesq l("I need @@ to work...", getitemlink(Plushroom));
        next;
        goto L_Quit;
    }
    inventoryplace HastePotion, 1;
    Zeny=Zeny-50;
    delitem Plushroom, 4;
    getitem HastePotion, 1;
    mesq l("Here you go!");
    goto L_OtherPotion;

L_StrengthPotion:
    mes "";
    mesn;
    if (Zeny < 50) {
        mesq l("You don't have enough money. Sorry.");
        next;
        goto L_Quit;
    }
    if (countitem(Chagashroom) < 4) {
        mesq l("I need @@ to work...", getitemlink(Chagashroom));
        next;
        goto L_Quit;
    }
    inventoryplace StrengthPotion, 1;
    Zeny=Zeny-50;
    delitem Chagashroom, 4;
    getitem StrengthPotion, 1;
    mesq l("Here you go!");
    goto L_OtherPotion;


L_DarkInv_Ever:
    mesn;
    mesq l("...What? You want @@? Five of them?!", getitemlink(EverburnPowder));
    next;
    mesn;
    mesq l("You have no idea of how dangerous that item is! I can't simply do it!");
    next;
    mesn;
    mesq l("I don't know who sent you to me, but this is a flat and big NO!");
    next;
    mesc l("Zitoni won't cooperate with you. Perhaps you should ask to Zarkor about that.");
    setq TulimsharQuest_DarkInvocator, 2;
    close;

L_DarkInv_Mush:
    mesn;
    mesq l("Ah... A @@. The sturdiest from all mushroom, and very, very rare.", getitemlink(DarkDesertMushroom));
    next;
    mesn;
    mesc l("Zitoni seems to be lost on deep thought.");
    next;
    mesn;
    mesq l("Ah... Well, ok. I'll do the powder for you, but you still need to bring me the material.");
    next;
    mesn;
    mesq l("Just... Don't accidentaly invoke a Legendary Guardian or something, these thingies could destroy a whole city.");
    delitem DarkDesertMushroom, 1;
    setq TulimsharQuest_DarkInvocator, 4;
    close;

L_DarkInv_Powder:
    mesn;
    mesq l("@@ is an expensive, rare, and dangerous item. Do not shake it too much, or it will catch fire.", getitemlink(EverburnPowder));
    next;
    mesn;
    mesq l("...And trust me, it'll take way more than just water to put the fire down.");
    next;
    mesn;
    mesq l("I will require 2500 GP, 1 @@, 1 @@ and 1 @@, for a small handful of it.", getitemlink(IronPowder), getitemlink(SulfurPowder), getitemlink(PileOfAsh));
    mes "";
    select
        l("I still don't have everything, but don't worry, I'll be back."),
        l("I have everything here with me.");
    mes "";
    if (@menu == 1)
        goto L_Quit;
    if (countitem(IronPowder) < 1 ||
        countitem(SulfurPowder) < 1 ||
        countitem(PileOfAsh) < 1 ||
        Zeny < 2500) goto L_Liar;
    Zeny=Zeny-2500;
    delitem IronPowder, 1;
    delitem SulfurPowder, 1;
    delitem PileOfAsh, 1;
    getitem EverburnPowder, 1;
    getexp 300, 0;
    mesn;
    mesq l("Here you go. Handle it with caution.");
    close;

L_Liar:
    mesn;
    mesq l("Liar. You can't fool me. That's the price. Get that or get out!");
    next;
    mesn;
    mesq l("We're talking about @@. It's not something for kids or pranksters!", getitemlink(EverburnPowder));
    close;

L_Quit:
    closedialog;
    goodbye;
    close; // double sure

OnInit:
    .sex = G_MALE;
    end;
}
