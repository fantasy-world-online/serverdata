// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 008-2: 2nd Floor - Party Dungeon mobs
008-2,79,244,15,12	monster	Slime Blast	1090,2,15000,15000
008-2,47,200,21,39	monster	Bandit	1024,4,25000,25000
008-2,106,203,33,26	monster	Mouboo	1023,4,25000,25000
008-2,84,35,61,19	monster	Cave Snake	1035,7,25000,25000
008-2,60,74,35,18	monster	Giant Maggot	1031,4,25000,25000
008-2,86,107,8,14	monster	Moggun	1070,2,25000,25000
008-2,48,126,28,33	monster	Mana Bug	1075,6,25000,25000
008-2,119,244,23,13	monster	Copper Slime	1088,1,25000,25000
008-2,117,84,21,27	monster	Red Slime	1092,6,25000,25000
008-2,122,157,21,17	monster	Yellow Slime	1091,3,25000,25000
008-2,110,125,21,14	monster	Lava Slime	1097,2,25000,25000
008-2,89,158,9,17	monster	Snake	1122,2,25000,25000
008-2,37,63,21,39	monster	Bandit	1024,4,25000,25000
008-2,119,93,21,77	monster	Desert Log Head	1127,6,25000,25000
008-2,117,213,21,39	monster	Desert Bandit	1124,3,25000,25000
008-2,30,197,17,24	monster	Sarracenus	1125,2,25000,25000
008-2,88,70,10,48	monster	Angry Red Scorpion	1130,6,25000,25000
008-2,67,87,10,60	monster	Sea Slime	1093,7,25000,25000
008-2,33,138,21,32	monster	Robin Bandit	1153,1,25000,25000
008-2,59,204,11,34	monster	Candied Slime	1089,4,25000,25000
008-2,86,172,10,54	monster	Green Slime	1085,6,25000,25000
008-2,81,61,73,39	monster	Plushroom Field	1011,4,25000,45000
008-2,81,242,21,15	monster	Piou	1002,3,25000,15000
008-2,81,117,73,39	monster	Chagashroom Field	1128,4,25000,45000
008-2,40,200,32,39	monster	Big Ruby Bif	1100,1,35000,45000
008-2,103,200,29,39	monster	Sapphire Bif	1114,2,25000,45000
