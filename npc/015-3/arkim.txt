// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Heremit
// Variables:
//    $ARKIM_ST
//      How many Bat Teeth/Wings were given
//    q1
//      Controls your own progress helping Arkim - Unused, might give place to some real quest later.
//    q2
//      Controls your own progress helping Arkim - Items today
//    q3
//      Controls your own progress helping Arkim - Your timer

015-3,170,169,0	script	Arkim	NPC_TERRY,{
    mesn;
    mesq lg("Hello, young girl...", "Hello, young boy...");
    next;

L_Loop:
    .@q1=getq(HurnscaldQuest_Arkim);
    .@q3=getq3(HurnscaldQuest_Arkim);
    mesn;
    mesq l("I am doing a great research with Bats, and thus far I collected @@ Bat Wings and Teeths.", $ARKIM_ST);
    next;
    // It was yesterday
    if (.@q3 < $@ARKIM_QTIMER) {
        setq2 HurnscaldQuest_Arkim, 0;
        setq3 HurnscaldQuest_Arkim, $@ARKIM_QTIMER;
    }

    // Daily limit reached
    .@q2=getq2(HurnscaldQuest_Arkim);
    if (.@q2 >= (BaseLevel-17)/3) goto L_Timer;
    select
        rif(countitem(BatWing) >= 1, l("Donate a Bat Wing")),
        rif(countitem(BatTeeth) >= 1, l("Donate a Bat Teeth")),
        l("I better leave this crazy man to his ordeals..."),
        l("Had you any breakthrough?");
    mes "";

    switch (@menu) {
        case 1:
            delitem BatWing, 1;
            getexp 25, 0;
            Zeny=Zeny+50;
            break;
        case 2:
            delitem BatTeeth, 1;
            getexp 36, 0;
            Zeny=Zeny+70;
            break;
        case 3:
            close;
            break;
        case 4:
            goto L_Research;
            break;
    }
    $ARKIM_ST=$ARKIM_ST+1;
    setq2 HurnscaldQuest_Arkim, .@q2+1;
    goto L_Loop;

L_Timer:
    if (BaseLevel < 20)
        mesc l("You need at least level 20 to help.");
    mesn;
    mesq l("You've helped me plenty. Please come back in @@", FuzzyTime($@ARKIM_TIMER+(60*60*24),2,2));
    close;

// TODO
L_Research:
    mesn;
    mesq l("Let me see... The more Wings and Teethes I collect, the more my research shall advance.");
    next;
    select
        l("Thanks."),
        rif($ARKIM_ST >= 1400, l("Cursed Arrows")),
        rif($ARKIM_ST >= 2800, l("Poison Arrows")),
        rif($ARKIM_ST >= 1200, l("Piberries Infusion")),
        rif($ARKIM_ST >= 2600, l("Fate's Potion")),
        rif($ARKIM_ST >= 4000, l("Clotho Liquor")),
        rif($ARKIM_ST >= 4700, l("Lachesis Brew")),
        rif($ARKIM_ST >= 6600, l("Atropos Mixture")),
        rif($ARKIM_ST >= 7500, l("Dark Desert Mushroom"));
    mes "";
    mesn;

    switch (@menu) {
    case 1:
        mesq l("Good bye!");
        close;
        break;
    case 2:
        mesq l("The @@ are specially dangerous, and archers love them.", getitemlink(CursedArrow));
        next;
        mesn;
        mesq l("These are being crafted by Alan.");
        break;
    case 3:
        mesq l("The @@ are specially dangerous, and archers love them.", getitemlink(PoisonArrow));
        next;
        mesn;
        mesq l("These are being crafted by Alan.");
        break;
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
        mesq l("The @@ is a powerful healing drink.", getitemlink(PiberriesInfusion-4+@menu));
        next;
        mesn;

        switch (@menu) {
            case 4:
               mesq l("@@ is developing these potions.", "Wyara"); break;
            case 5:
               mesq l("@@ is developing these potions.", "Fate, in Nivalis,"); break; // TODO: Missing NPC
            default:
               mesq l("@@ is developing these potions.", "Jesusalva"); // TODO: Clotho, Lachesis and Atropos
               break;
        }

        break;
    case 9:
        mesq l("Ah! @@. A very rare drop!", getitemlink(DarkDesertMushrooom));
        next;
        mesn;
        mesq l("You can find it in the bandit market, but it is EXPENSIVE.");
        break;

    }
    next;
    goto L_Research;

OnInit:
    .sex=G_MALE;
    .distance=5;
    // No end; on purpose

OnHour00:
    $@ARKIM_QTIMER=gettimeparam(GETTIME_DAYOFMONTH);
    $@ARKIM_TIMER=gettimetick(2);
    end;

}
