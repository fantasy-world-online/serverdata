// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Slay the Fafi Dragon quest. Due the OP monster, you better find yourself a good team!

018-4-2,31,26,0	script	Susanne	NPC_FAIRY_A,{
    .@q=getq(LoFQuest_Fairy);

    if(BaseLevel < 53) goto L_Noob;
    if(.@q == 1) goto L_Coward;
    if(.@q == 2) goto L_Complete;
    if(.@q >= 3) goto L_Thanks;

    mesn;
    mesq l("Do you like fairies? Well, you should love them! We play with humans all the time, it is usually pretty fun...");
    next;
    mesc l("(The girl now looks away sadly, thinking about what to say next.)");
    next;
    mesn;
    mesq l("But dragons came and started roaming this island. My friends are now either dead or missing. You look big and strong, do you want to avenge me?");
    goto L_Menu;

L_Menu:
    menu
        l("Sure, I'd love to help! What can I do?"), L_Yes,
        l("Nah, I've got more serious matters to attend to..."), L_No;

L_Yes:
    mesn;
    mesq l("Thank you so much! That might not bring my friends back, but will make this island much safer!");
    next;
    mesn;
    mesq l("The Fafi Dragon are really really bad guys. I don't know how they came here, as they're not from the Lands Of Fire, nor from Mana World.");
    next;
    mesn;
    mesq l("But this is not important. They are dangerous. They must be stop at any cost!");
    next;
    mesn;
    mesq l("So if you can help, go kill at least one on this island. They take a long time to respawn.");
    next;
    mesn;
    mesq l("Thank you...");
    setq LoFQuest_Fairy, 1;
    if (!mobcount("018-4","Susanne::OnKillFafi"))
        goto L_Spawn;
    close;

OnKillFafi:
    message strcharinfo(0), "You killed the Fafi Dragon.";
    .@q=getq(LoFQuest_Fairy);
    if (.@q == 1) {
        setq LoFQuest_Fairy, 2;
        mapannounce "018-4-2", "" +strcharinfo(0)+ " has killed the Fafi Dragon!", 0;
        mapannounce "018-4-1", "" +strcharinfo(0)+ " has killed the Fafi Dragon!", 0;
        mapannounce "018-4", "" +strcharinfo(0)+ " has killed the Fafi Dragon!", 0;
        message strcharinfo(0), "Cheers are being heard throughout the land!";
    }
    fix_mobkill(FafiDragon);
    end;

L_Complete:
    mesn;
    mesq l("Many thanks for killing it. Maybe more humans come here now, and play with me.");
    next;
    inventoryplace FafiMask, 1;
    getitem FafiMask, 1;
    getexp 80000, 200;
    setq LoFQuest_Fairy, 3;
    mesn;
    mesq l("By the way, I found this Mask after you killed the Fafi Dragon, Maybe you can use it some day.");
    next;
    mesn;
    mesq l("And some times, more Fafi dragons come... Don't neglect your aid.");
    close;

L_Thanks:
    mesn;
    mesq l("Sometimes humans come here and play with me. Many thanks for the help!");
    if (!mobcount("018-4","Susanne::OnKillFafi") && .respawnTime <= gettimetick(2)) {
        next;
        mesn;
        mesq l("Actually, about Fafi dragons...");
        next;
        goto L_Check;
    }
    close;

L_No:
    mesn;
    mesq l("Ok...please come back when you aren't busy...");
    next;
    mesc l("The girl turns around and you hear her sniffing, she is probably crying...");
    close;

L_Noob:
    mesn;
    mesq l("Hey, have you come to play with me? I love playing with humans! Usually so few show in, it must be the dragons...");
    //mesq l("I need some help with something, but I don't think you're strong enough. Come back later please when you're stronger.");
    close;

L_Coward:
    if (!mobcount("018-4","Susanne::OnKillFafi"))
        goto L_Check;
    mesn;
    mesq l("What are you doing talking to me? Go fight, you coward!");
    close;

L_Check:
    if (.respawnTime > gettimetick(2) && (!mobcount("018-4","Susanne::OnKillFafi"))){
        mesn;
        mesq l("Just wait. Fafi dragons are slow to show up...");
    } else {
        mesn;
        mesq l("The Fafi knows you are here to kill him. They are smart. But I can make a trap.");
        next;
        mesn;
        mesq l("Bring me this and I'll spawn it for you:");
        // .@q valid values are 1 (Starter) and 3 (Veteran)
        mesc l("@@/@@ @@", countitem(ShadowHerb), .@q*5, getitemlink(ShadowHerb));
        mesc l("@@/@@ @@", countitem(Moss), .@q*4, getitemlink(Moss));
        mesc l("@@/@@ @@", countitem(Root), .@q*3, getitemlink(Root));
        next;
        mesn;
        mesq l("I'll be attracted by the Shadow Herb. The moss will hide the roots, which will entrap it for a while.");
        mesq l("So, do you have the items?");
        if (askyesno() != ASK_YES) {
            close;
        }
        mes "";
        if (countitem(ShadowHerb) < .@q*5 ||
            countitem(Moss)       < .@q*4 ||
            countitem(Root)       < .@q*3) goto L_Fail;
        if (.respawnTime > gettimetick(2)) {
            mesn;
            mesq l("Too slow.");
            close;
        }
        delitem ShadowHerb, .@q*5;
        delitem Moss,       .@q*4;
        delitem Root,       .@q*3;
        goto L_Spawn;
    }
    close;

L_Spawn:
    if (.respawnTime > gettimetick(2)){
        mesn;
        mesq l("Just wait. Fafi dragons are slow to show up...");
    } else {
        mesn;
        mesc l("Screams");
        mesq l("I hear a Fafi Dragon on THIS very island!!");
        .respawnTime=gettimetick(2)+(60*90);
        monster "018-4", any(64,83,100,123,139), any(64,74,89), strmobinfo(1, FafiDragon), FafiDragon, 1, "Susanne::OnKillFafi";
    }
    close;

L_Fail:
    mesn;
    mesq l("You're lucky that this is the Land Of Fire and I'm on a good mood, otherwise, I would have killed you for lying to me.");
    close;

OnInit:
    .respawnTime=0;
    .sex=G_FEMALE;
    .distance=5;
    end;

}
