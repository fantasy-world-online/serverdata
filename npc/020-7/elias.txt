// TMW2 scripts.
// Authors:
//    Jesusalva
//    TMW Org.
// Description:
//    Controls access to Blue Sage Residence
//    Minimum level: 36 (implicit)
//    Minimum jblvl: 16 (implicit)

// NivalisQuest_BlueSage STRUCTURE
//  FIELD 1:
//      INVESTIGATION
//      1 - STBY OUTSIDE
//      2 - ACCESS GRANTED
//      3 - QUEST ASSIGNED BY PEETU - talk to Oskari (and others)
//      4 - Oskari is OK with peetu, but wanna hear from others. He also sends you
//          to ask what Peetu happened
//      5 - Adultered ingredients seems the cause, report to Elias
//      6 - Elias is now worried about a visitor. Ask people about and report.
//      7 - If everyone found the visitor, confirm Elias the worries
//      8 - Elias sent you to Oskari to inform the issue. Blue Sage probably knew all along.
//      will not advance unless everyone thinks Peetu is good.
//      9 - Oskari accepts the cause. Tells to report Peetu that it probably was
//          a saboutage, to check if the Silk Cocoon really was there.
//      10 - Peetu confirmed the saboutage. Report to Blue Sage.
//      11 - Blue Sage accepted the evidence, and explains about other sages issues.
//          It's not known who or what is behind this. He excuses for making you waste
//          your time. He asks you to return to him later, as he needs to write letters.
//      12 - QUEST COMPLETE - You collected your reward
//          Also picked up a letter for Frostia Mayor, about the incident (Main Story).
//  FIELD 2:
//      Bitwise (BS_QVISITOR)
//  FIELD 3:
//      Bitwise (BS_QHELPER)

020-7,44,41,0	script	#BlueSageEntry	NPC_HIDDEN,1,1,{
OnTouch:
    .@q=getq(NivalisQuest_BlueSage);
    if (.@q < 2) {
        slide 42, 43;
        doevent "Elias::OnAccessDenied";
    }
    end;
}

// Here we start
020-7,40,41,0	script	Elias	NPC_BLUESAGEWORKER_MB,{
    function eliasWorry;
    function eliasQuestion;
    function eliasThankyou;
    function eliasConfirmed;
    mesn;
    mesq l("Hello, and welcome to Blue Sage's Residence, Library, and Nivalis Townhall.");
    .@q=getq(NivalisQuest_BlueSage);
    if (.@q >= 2)
        goto L_Main;
    next;
    mesn;
    mesq l("Due to a recent incident involving slimes, the building is closed to public visits.");
    .@qn=getq(General_Narrator);
    mes "";
    select
        l("That's sad to hear."),
        rif(.@qn == 10 && !.@q, l("I have a letter from Rakinorf.")),
        rif(.@q == 1, l("So? How was it?")),
        l("What happened?");
    mes "";
    switch (@menu) {
        case 2:
            mesn;
            mesq l("Oh, you must be from Hurnscald Household, then. Hand me the letter, I'll have it delivered.");
            next;
            mesn;
            mesq l("Please wait here a short while.");
            setq NivalisQuest_BlueSage, 1, 0, 0;
            break;
        case 3:
            mesn;
            mesq l("Yes, the Blue Sage will see you.");
            next;
            mesn;
            mesq l("Please don't mind the mess, there are slimes everywhere, thanks to Peetu.");
            next;
            mesn;
            mesq l("You may pass. The Sage is on the library waiting for you.");
            setq NivalisQuest_BlueSage, 2, 0, 0;
            break;
        case 4:
            mesn;
            mesq l("Slimes are on the loose. They have escaped, ate several books, and some are still in the building.");
            next;
            mesn;
            mesq l("Some are dangerous, too. Even if you look capable of fighting, the Blue Sage instructed me to prevent anyone from visiting until the mess is cleared.");
            break;
    }
    //goodbye;
    close;

L_Main:
    next;
    // .@q is preserved when you use goto
    switch (.@q) {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
            eliasQuestion();
            break;
        case 6:
            eliasWorry();
            break;
        case 7:
        case 8:
            eliasConfirmed();
            break;
        case 9:
        case 10:
        case 11:
            eliasThankyou();
            break;
        default:
            mesn;
            mesq l("I heard you have been assisting the household staff. For that, I am thankful.");
            break;
    }
    close;

// Elias is worried with visitor and asks for insights
function eliasWorry {
    .@q2=getq2(NivalisQuest_BlueSage);
    if (.@q2 == BS_NPCALL) {
        setq1 NivalisQuest_BlueSage, 7;
        eliasConfirmed();
        close;
    }
    mesn;
    mesq l("After your questions I'm really getting worried about this guy with the mask. In retrospect it really seems suspicous.");
    next;
    mesn;
    mesq l("It might be a good idea to ask around if anyone else observed something odd connected to this person.");
    return;
}

// Suspections Confirmed, report back
function eliasConfirmed {
    mesn;
    mesq l("I heard your investigations reminded other people that they observed suspicious behaviour too.");
    next;
    mesn;
    mesq l("It's all my fault, I should've paid more attention! You should talk to Chief Oskari about this.");
    if (getq(NivalisQuest_BlueSage) == 7)
        setq1 NivalisQuest_BlueSage, 8;
    return;
}

// Waiting quest to end
function eliasThankyou {
    mesn;
    mesq l("Now that Chief Oskari knows about the sequence of events, the Sage will take care of it. It's really a shame how malicious people can be.");
    next;
    mesn;
    mesq l("Thanks a lot for your help revealing the truth.");
    return;
}

// Main question to Elias
function eliasQuestion {
    select
        l("Peetu thinks someone put Silk Cocoon along the ingredients."),
        l("Thanks, pal. I still got some matters to discuss with the Blue Sage if you excuse me.");
    mes "";
    if (@menu == 2) {
        mesn;
        mesq l("Yes, of course. We have good relations with Hurnscald Town, so you're welcome.");
        close;
    }
    mesn;
    mesq l("So, Peetu didn't check the ingredients before casting? That's... Well, I don't know.");
    next;
    mesn;
    mesq l("But that is unlikely, we have no reason to sabotage our own work! You saw the mess it caused. Everyone knew it was important.");
    next;
    mesn strcharinfo(0);
    mesq l("Hmm, it might be a hunch but... Can you tell me if there were any unusual visitors before the incident?");
    next;
    mesn;
    mesq l("Mh, let me think. There were quite a few visitors with different concerns, but that's usual at this time of the year.");
    next;
    mesn;
    mesq l("One of them was a bit odd, they were wearing a mask and had a strange way of talking. They said they came from Frostia, which is an elven town in the north. Elves are usually shy, so I thought it would have been rude to ask them to remove the mask.");
    next;
    mesn;
    mesq l("If I remember correctly they wanted to see the library. They looked like a man, but you never know.");
    next;
    mesn;
    mesc l("Elias gets a bit excited.");
    mesq l("Do you think he may have had something to do with the accident? We're a very hospitable house, so I didn't see a reason to deny him the entrance. I mean, his mask might have been a bit unusual, but hey, there could've been many reasons why someone would wear a mask, don't you think?");
    setq1 NivalisQuest_BlueSage, 6;
    next;
    eliasWorry();
    return;
}


OnAccessDenied:
    npctalk3 l("You can't go in there!");
    end;

OnInit:
    .sex=G_MALE;
    .distance=5;
    end;

}

