// TMW-2 Script
// Author:
//    Jesusalva
// Description:
//    Alan is Hurnscald's bowmaster. He may craft the Forest Bow, and sell ammo.

012-3,44,40,0	script	Alan	NPC_BOWMASTER,{
    .@q=getq(HurnscaldQuest_ForestBow);
    .@q2=getq2(HurnscaldQuest_ForestBow);
    .@q3=getq3(HurnscaldQuest_ForestBow);
    switch (.@q) {
        case 1:
            npctalk3 l("Ah, if Jack and that fisherman from Halinarzo still gave me materials...");
            break;
        case 2:
            npctalk3 l("The bow I gave you was a masterpiece.");
            break;
    }

    mesn;
    mesq l("Only the finest bows and arrows, in the land where wood is abundant!");
    mes "";
    menu
        l("Ok, thanks."), L_Close,
        l("I want to trade."), L_Shop,
        rif(.@q == 0, l("Do you accept special requests? Could you make me a really good bow?")), L_ForestBow,
        rif(.@q == 1 && .@q2 == .@q3 && .@q2 == 99, l("I have the sturdy wood and the string.")), L_Craft;

L_Shop:
    openshop;
    closedialog;
    close;

L_ForestBow:
    mesn;
    mesq l("Well, I know how to do a @@, a sturdy bow, but I am not making those anymore, sorry.", getitemlink(ForestBow));
    next;
    menu
        l("Oh, too bad."), L_Close,
        l("What? Why not?"), L_Next;

L_Next:
    mesn;
    mesq l("You see, I cannot use any raw material. It must be sturdier than the usual, and I don't have the materials for it.");
    next;
    mesn;
    mesq l("The wood, Jack the Lumberjack used to deliver me, but he isn't delivering anymore. And the string was imported from Halinarzo.");
    next;
    mesn;
    mesq l("Perhaps, you could convince Jack to give me the wood, and seek for a fisherman on Halinarzo to give you the string? I won't charge anything.");
    next;
    menu
        l("Not really, sorry."), L_Close,
        l("Right'o, I'll arrange the material!"), L_Start;

L_Start:
    // q2 → Wood Part
    // q3 → String Part
    setq HurnscaldQuest_ForestBow, 1, 0, 0;
    goto L_Close;

L_Craft:
    inventoryplace ForestBow, 1;
    getitem ForestBow, 1;
    getexp 900, 0;
    setq HurnscaldQuest_ForestBow, 2, 0, 0;
    mesn;
    mesq l("Here you go - have fun with it.");
    next;
    goto L_Close;


L_Close:
    closedialog;
    goodbye;
    close;

OnInit:
    .sex = G_MALE;
    .distance = 5;
	tradertype(NST_MARKET);

    sellitem ShortBow, 9000, 1;
    sellitem WoodenBow, 4000, 2;

    sellitem TrainingArrow, -1, 2000;
    sellitem Arrow, -1, 1000;
    sellitem IronArrow, -1, 500;

    if ($ARKIM_ST > 1400)
        sellitem CursedArrow, -1, $ARKIM_ST-1400;
    if ($ARKIM_ST > 2800)
        sellitem PoisonArrow, -1, $ARKIM_ST-2800;

	sellitem TrainingAmmoBox, -1, 10;
	sellitem ArrowAmmoBox, -1, 5;
	sellitem IronAmmoBox, -1, 2;

    if ($ARKIM_ST > 2800)
        sellitem CursedAmmoBox, -1, (($ARKIM_ST-1400)/200);
    if ($ARKIM_ST > 5600)
        sellitem PoisonAmmoBox, -1, (($ARKIM_ST-2800)/200);

    end;


OnClock2357:
OnClock1151:
    restoreshopitem ShortBow, 9000, 1;
    restoreshopitem WoodenBow, 4000, 2;
	restoreshopitem TrainingAmmoBox, -1, 10;
	restoreshopitem ArrowAmmoBox, -1, 5;
	restoreshopitem IronAmmoBox, -1, 2;

    if ($ARKIM_ST > 2800)
        restoreshopitem CursedAmmoBox, -1, (($ARKIM_ST-1400)/200);
    if ($ARKIM_ST > 5600)
        restoreshopitem PoisonAmmoBox, -1, (($ARKIM_ST-2800)/200);
OnClock0611:
OnClock1800:
    restoreshopitem TrainingArrow, -1, 2000;
    restoreshopitem Arrow, -1, 1000;
    restoreshopitem IronArrow, -1, 500;
    if ($ARKIM_ST > 2800)
        restoreshopitem CursedArrow, -1, (($ARKIM_ST-1400)/200);
    if ($ARKIM_ST > 5600)
        restoreshopitem PoisonArrow, -1, (($ARKIM_ST-2800)/200);

}
