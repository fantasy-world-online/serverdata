// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Debug functions for Craft Systems


001-3-1,32,89,0	script	GM Alchemy Table	NPC_NO_SPRITE,{
    if (!$@GM_OVERRIDE && $EVENT$ == "" && !is_staff()) goto L_Offline;
    mesc l("Welcome to Saulc's Magic Alchemy Table!");
    mesc l("This table will prepare the potion for you, no skill required!");
    mesc l("What will you brew today?");
    if (AlchemySystem(CRAFT_NPC))
        mesc l("Success!"), 3;
    else
        mesc l("That didn't work!"), 1;
    close;

L_Offline:
    npctalk3 l("Oops! Seems like Saulc doesn't wants you messing on his chemistry set!");
    close;

OnInit:
    .distance=5;
    end;
}

