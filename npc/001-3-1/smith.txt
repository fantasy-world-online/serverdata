// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Debug functions for Craft Systems


001-3-1,52,86,0	script	GM Black Smithy	NPC_NO_SPRITE,{
    if (!$@GM_OVERRIDE && $EVENT$ == "" && !is_staff()) goto L_Offline;
    mesc l("Welcome to Saulc's Magic Smith Table!");
    mesc l("This forge will prepare equipment for you, no skill required!");
    mesc l("What will you craft today?");
    //mesc l("You need to have an @@ equipped.", getitemlink(Knife)), 1;
    if (SmithSystem(CRAFT_NPC))
        mesc l("Success!"), 3;
    else
        mesc l("That didn't work!"), 1;
    close;

L_Offline:
    npctalk3 l("Oops! Seems like Saulc doesn't wants you messing on his raging furnaces!");
    close;

OnInit:
    .distance=5;
    end;
}

